<?php

namespace jobseeker\controllers;

use Yii;
use yii\web\Controller;
use jobseeker\models\SearchForm;
use jobseeker\models\Images;

class MainController extends Controller
{
    /**
     * {@inheritdoc}
     */
    /**
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    */
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest)
        {
//            Yii::$app->response->redirect(Yii::$app->urlManager->createUrl('jobseeker/my'));
        }
        
        $searchFormModel = new SearchForm();
        
        return $this->render('index', compact('searchFormModel'));
    }
}