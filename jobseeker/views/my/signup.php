<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use borales\extensions\phoneInput\PhoneInput;

$this->title = Yii::t('jobseeker', 'Sign Up as Jobseeker');

?>

<section id="jobseeker-signup">
    <div class="container py-5">
        <h1><?= Yii::t('jobseeker', 'Registration') ?></h1>
        <p class="text-secondary"><?= Yii::t('jobseeker', "You will have access to all the site features you need to find a great job.") ?></p>
        <div class="tabs-container row justify-content-start">
            <div class="tab active col-6 col-sm-6 col-md-2 col-lg-2 text-secondary"><?= Yii::t('jobseeker', 'Jobseeker') ?></div>
            <div class="tab col-6 col-sm-6 col-md-2 col-lg-2 text-blue"><?= Html::a(Yii::t('jobseeker', 'Employer'), Url::to('employer/my/signup'))?> <i class="fal fa-angle-right"></i></div>
        </div>
        
        <div class="row">
                <?php 
                    $signupForm = ActiveForm::begin([
                        'id' => 'jobseeker-signup-form',
                        'options' => ['id' => 'signup_block', 'class' => 'col-md-8 col-left px-4 py-4 form-phone-validation'],
                    ]);
                ?>
                    
                <h2 class="mb-4">
                    <?= Yii::t('jobseeker', 'Your credentials') ?>
                    <a class="text-blue h4 unselectable" data-toggle="popover" data-trigger="click hover" data-placement="bottom" data-content="Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.">
                        <i class="fal fa-question-circle "></i> 
                    </a>
                </h2>
    
                <div class="">
                <?= $signupForm->field($signupFormModel, 'email', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control'] ])->label(Yii::t('jobseeker', 'Email'), ['class' => 'form-control-label h5 text-main-2']) ?>
                </div>
        
                <div class="">
                <?= $signupForm->field($signupFormModel, 'password', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}", 'inputOptions' => ['class' => 'form-control', 'type' => 'password'] ])->label(Yii::t('jobseeker', 'Password'), ['class' => 'form-control-label h5 text-main-2']) ?>
                </div>
    
                <div class="">
                <?= $signupForm->field($signupFormModel, 'password_repeat', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}", 'inputOptions' => ['class' => 'form-control', 'type' => 'password'] ])->label(Yii::t('jobseeker', 'Password again'), ['class' => 'form-control-label h5 text-main-2']) ?>
                </div>
                <hr>    
                <h2 class="mb-4">
                    <?= Yii::t('jobseeker', 'Contact information') ?>
                    <a class="text-blue h4 unselectable" data-toggle="popover" data-trigger="click hover" data-placement="bottom" data-content="Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.">
                        <i class="fal fa-question-circle "></i> 
                    </a>
                </h2>
                <p class="text-secondary mb-4"><?= Yii::t('jobseeker', 'We will not share your name and phone number with anyone. They will only appear on the site if you yourself publish them.') ?></p>
                <div class="">
                <?= $signupForm->field($signupFormModel, 'full_name', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}", 'inputOptions' => ['class' => 'form-control'] ])->label(Yii::t('jobseeker', 'Your first and last name'), ['class' => 'form-control-label h5 text-main-2']) ?>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 px-0 mx-0 float-left">
                    <?= $signupForm->field($signupFormModel, 'phone', ['addAriaAttributes' => true, 'validateOnType' => true, 'template' => "{label}\n{input}\n{error}", 'inputOptions' => ['class' => 'form-control', 'data-id' => 'phone-input'] ])->label(Yii::t('jobseeker', 'Phone'), ['class' => 'form-control-label h5 text-main-2'])->widget(PhoneInput::className(), [
                            'jsOptions' => [
                                'nationalMode' => false,
                                'onlyCountries' => ['us', 'ru', 'ua']
                            ],
                    ]); ?>
                    
                </div>
                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 px-0 mx-0 float-right">
                    <div class="form-group field-signupform-phone_verification_code float-left required">
                        <label class="form-control-label h5 float-left px-0 col-12 text-main-2"><?= Yii::t('jobseeker', 'Phone verification') ?></label>
                        <?= Html::button(Yii::t('jobseeker', 'Send'), ['class' => 'col-5 float-left', 'name' => 'phone-submit-button', 'data-disabled' => 'true']) ?>
                        <?= $signupForm->field($signupFormModel, 'phone_verification_code', ['addAriaAttributes' => true, 'enableClientValidation' => true, 'validateOnType' => true, 'template' => "{input}\n{error}", 'selectors' => ['error' => '.help-block.col-6.float-right'], 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control col-6 text-center float-right', 'readonly' => '', 'data-id' => 'verification-code-input', 'placeholder' => Yii::t('jobseeker', 'Code')] ])->error(['class' => 'help-block col-6 float-right']); ?>
                        <div class="phone-verified-sign col-6 float-right d-none"><?= Yii::t('jobseeker', 'Verified') ?> <i class="fad fa-check-double"></i></div>
                    </div>
                </div>
                <hr> 
                <p class="text-secondary mb-4"><?= Yii::t('jobseeker', 'By clicking the "Register" button below, you accept the <a href="{terms_of_use}">site\'s terms of use</a> and <a href="{privacy_policy}">privacy policy.</a>', ['terms_of_use' => '', 'privacy_policy' => '']) ?></p>
                <div class="col-xs-12 col-sm-8 col-md-2 col-lg-2 px-0">
                     <?= Html::submitButton(Yii::t('jobseeker', 'Register'), ['name' => 'signup-button']) ?>
                </div>
                <?php 
                    ActiveForm::end();
                ?>
            <div class="col-md-4 col-right">
                <h5 class="text-main pl-4">
                   <?= Yii::t('jobseeker','Looking for employees?') ?>
                </h5>
                <ul class="text-blue pl-4 list-unstyled">
                    <li>
                        <i class="fad fa-circle mr-2"></i>
                        <?= Html::a(Yii::t('jobseeker','Register as an employer'), Yii::$app->urlManager->createUrl('employer/my/signup')) ?>
                    </li>
                </ul>
                <h5 class="text-main pl-4">
                    <?= Yii::t('jobseeker','Already registered?') ?>
                </h5>
                <ul class="text-blue pl-4 list-unstyled">
                    <li>
                        <i class="fad fa-circle mr-2"></i>
                        <?= Html::a(Yii::t('jobseeker','Sign in'), Yii::$app->urlManager->createUrl('jobseeker/my/login'))  ?></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>