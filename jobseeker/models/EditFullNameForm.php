<?php

namespace jobseeker\models;

use Yii;
use yii\base\Model;
use common\models\Jobseekers;

class EditFullNameForm extends Model 
{
    public $full_name; 
        
    public function rules()
    {
        return [
            ['full_name', 'trim'],
            ['full_name', 'required'],
            ['full_name', 'string', 'min' => 3, 'max' => 255],
        ];
    }
    
    public function editFullName() {
        if(!$this->validate()) {
            return false;
        }
        
        $jobseeker = Jobseekers::findOne(['user_id' => Yii::$app->user->getId()]);
        $jobseeker->full_name = $this->full_name;
        
        return $jobseeker->save();
              
    }
}