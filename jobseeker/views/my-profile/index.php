<?php

use common\assets\CommonAsset;
use jobseeker\assets\AppAsset;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use jobseeker\models\ProfileSettingsVariables;
use yii\widgets\ActiveForm;

$js = <<<JS
    var section_left = $('.profile-info-section-left');
    var section_right = $('.profile-info-section-right');
    $(window).on('scroll', function() {
        if($(window).width() >= window.getComputedStyle(document.body).getPropertyValue('--breakpoint-md').substr(1,3))
        {
            if(window.pageYOffset >= section_right.offset().top)
            {
                section_left.addClass('fixed');
                section_left.css('height', (section_right.offset().top + section_right.outerHeight() - window.pageYOffset) + 'px');
                section_right.addClass('offset-3');
                
            }
            else
            {
                section_left.removeClass('fixed');
                section_left.css('height', 'auto');
                section_right.removeClass('offset-3');
            }
        }
    });
JS;

$this->registerJsFile(
    '@web/js/select2.js',
    [
        'position' => \yii\web\View::POS_HEAD,
        'depends' => [
            AppAsset::class,
            CommonAsset::class
        ]
    ]
);

$this->registerJsFile(
    '@web/js/pjax-form-loading.js',
    [
        'position' => \yii\web\View::POS_HEAD,
        'depends' => [
            AppAsset::class,
            CommonAsset::class
        ]
    ]
);

$this->registerJs($js);

$SettingsSelectData = new ProfileSettingsVariables();

?>

<div class="container my-5 py-0 px-0 bg-white b-1">
    <div class="row mx-0">
        <div class="col-12 col-md-3 py-4 px-4 profile-info-section-left br-1">
            <?php
                
                Pjax::begin([ 'id' => 'Jobseeker', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n4 pt-4 mb-n3 pb-3']]); 
                
            ?>
            <div class="row mx-0 justify-content-center">
                <div id="profile-photo-container" class="mt-2 px-0" style="background-image: url(<?= (empty($Jobseeker->photo->path_cropped)) ?  WImages::widget(['title' => 'profile-photo-empty']) : $Jobseeker->photo->path_cropped ?>);">
                    <span class="profile-control-buttons-container">
                        <div class="profile-control-button-block" data-toggle="modal" data-target="#edit-photo-modal">
                            <button class="profile-control-button position-relative" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit photo" style="top:-3px;">
                                <i class="fas fa-pencil"></i>
                            </button>   
                        </div> 
                    </span>
                </div>
                <h3 class="col-12 text-center text-main mt-2"><?= $Jobseeker->full_name ?></h3>
                <div class="col-12 text-center text-secondary px-0 py-1">
                    <?= $Jobseeker->city . ', ' . $Jobseeker->country ?>
                </div>
                <div class="col-12 text-center text-secondary px-0 py-1">
                    <?= Yii::t('jobseeker', '{age} years old', ['age' => date('Y') - date('Y', strtotime($Jobseeker->birth_date))]) ?>
                </div>
            </div>
            <?php
            
                Pjax::end(); 
                
            ?>
            <hr class="bt-1 mx-n4">
            <?php

            if(!empty($JobseekerCurrentProfile) && !empty($JobseekerProfiles)):

                Pjax::begin([ 'id' => 'JobseekerCurrentProfileNav', 'enablePushState' => true, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n3 pb-3']]);

                $JobseekerProfileTitle = md5($JobseekerCurrentProfile->title);

            ?>

            <div class="row mx-0">
                <div class="dropdown mx-n4 mb-3" style="width: calc(100% + 3rem);">
                    <button type="button" class="btn btn-secondary dropdown-toggle px-4 text-left outline-none" id="profile-specialization-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?= $JobseekerCurrentProfile->title ?>
                        <i class="fad fa-sort-size-down-alt ml-1"></i>
                    </button>
                    <div class="dropdown-menu my-0" aria-labelledby="profile-specialization-dropdown" style="width: 100%; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 0px; box-shadow: rgba(57, 73, 76, 0.35) 0px 5px 10px -1px; border: 0px;">
                    <?php foreach($JobseekerProfiles as $JobseekerProfile): ?>
                        <?= Html::button($JobseekerProfile->title, ['class' => 'dropdown-item',
                            'onclick' =>
                                '
                                $("#JobseekerCurrentProfileNav").toggleClass("loading");
                                $("#JobseekerCurrentProfile").toggleClass("loading");
                                pjaxContainerReload("#JobseekerCurrentProfileNav", "'.Yii::$app->urlManager->createAbsoluteUrl('jobseeker/my/profile/' . $JobseekerProfile->id).'"); 
                                pjaxContainerReload("#JobseekerCurrentProfile", "'.Yii::$app->urlManager->createAbsoluteUrl('jobseeker/my/profile/' . $JobseekerProfile->id).'");
                                ']) ?>

                    <?php endforeach;?>
                    </div>
                </div>
                <div class="col-12 text-main font-weight-bold pl-0 py-1">
                    <?= Yii::t('jobseeker', 'Employment type') ?>
                    <span class="profile-control-buttons-container">
                        <div class="profile-control-button-block d-inline-block" data-toggle="modal" data-target="#edit-profile-modal--<?= $JobseekerProfileTitle ?>">
                            <button class="profile-control-button ml-3 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit employment type">
                                <i class="fas fa-pencil"></i>
                            </button>
                        </div>
                    </span>                
                </div>
                
                <div class="col-12 text-secondary pl-0 py-1">
                <?php
                    foreach (explode(', ', $JobseekerCurrentProfile->employment_type) as $EmploymentType) {
                        echo $SettingsSelectData->employment_types[$EmploymentType] . ',';
                    }
                ?>
                </div>
                
                <div class="col-12 text-main font-weight-bold pl-0 py-1">
                    <?= Yii::t('jobseeker', 'Desired salary') ?>
                    <span class="profile-control-buttons-container">
                        <button class="profile-control-button ml-3 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit employment type">
                            <i class="fas fa-pencil"></i>
                        </button>   
                    </span>
                </div>
                <div class="col-12 text-secondary pl-0 py-1">
                    <?= number_format($JobseekerCurrentProfile->salary, 2, '.', ',') ?> <?= $JobseekerCurrentProfile->salary_currency ?>
                </div>
            </div>

            <?php
                
                Pjax::end();
            endif;

            ?>
            <hr class="bt-1 mx-n4">
            <?php
                
            if(!empty($JobseekerLanguages)):
                    
                Pjax::begin([ 'id' => 'JobseekerLanguages', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n4 pb-4']]); 
                
            ?>
            <div class="row mx-0">
                <h5 class="col-12 text-main font-weight-normal mt-2 px-0">
                    <?= Yii::t('jobseeker', 'Languages') ?>
                    <span class="profile-control-buttons-container">
                        <div class="profile-control-button-block d-inline-block" data-toggle="modal" data-target="#edit-languages-modal">
                            <button class="profile-control-button" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Edit language') ?>">
                                <i class="fas fa-pencil"></i>
                            </button>
                        </div>
                        <button class="profile-control-button ml-1 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit employment type">
                            <i class="fas fa-plus"></i>
                        </button>
                    </span>
                </h5>
                <?php 
                
                foreach($JobseekerLanguages as $JobseekerLanguage): 
                    $JobseekerLanguageTitle = md5($JobseekerLanguage->language);
                
                ?>
                <div class=" text-main font-weight-bold pl-0 pr-2 py-1">
                    <?= common\models\i18nLanguages::findOne(['id' => $JobseekerLanguage->language])->english_name ?>:
                </div>
                <div class=" text-secondary pl-0 py-1">
                    <?= Yii::t('jobseeker', $SettingsSelectData->language_proficiency[$JobseekerLanguage->proficiency]) ?>
                    <?php if(!empty($JobseekerLanguage->certificate_photo)): ?>
                    <i class="fas fa-check-circle" style="color: #4cd964;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Language certificate approved"></i>
                    <?php endif; ?>
                </div>
                <?php endforeach; ?>

                <div class="modal fade" id="edit-languages-modal" tabindex="-1" aria-labelledby="edit-languages-modal-title" aria-hidden="true" role="dialog">
                    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                        <div class="modal-content br-2 b-none">
                            <?php
                            $editLanguagesForm = ActiveForm::begin([
                                'id' => 'edit-languages-form',
                                'options' => [
                                    'onsubmit' => 'return submitFormAjax(this, "#JobseekerLanguages");',
                                    'style' => 'height: 100%; overflow-y: auto;'
                                ]
                            ]);
                            ?>
                            <div class="modal-header">
                                <h5 class="modal-title" id="edit-languages-modal-title"><?= Yii::t('jobseeker', 'Edit languages') ?></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <?php

                                foreach($JobseekerLanguages as $JobseekerLanguage):
                                    $JobseekerLanguageTitle = md5($JobseekerLanguage->language);

                                    $editLanguagesFormModel->language = $JobseekerLanguage->language;
                                    $editLanguagesFormModel->proficiency = $JobseekerLanguage->proficiency;

                                ?>

                                <?= $editLanguagesForm->field($editLanguagesFormModel, 'language', ['addAriaAttributes' => true, 'enableClientValidation' => true, 'template' => "{input}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-language-form-select--'. $JobseekerLanguageTitle] ])
                                    ->hiddenInput(['value' => $JobseekerLanguage->language])
                                ?>

                                <?= $editLanguagesForm->field($editLanguagesFormModel, 'proficiency', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-language-form-proficiency-select--'. $JobseekerLanguageTitle] ])
                                    ->label($JobseekerLanguage->i18nLanguage['english_name'], ['class' => 'control-label font-weight-normal text-main-2'])
                                    ->widget(Select2::className(),
                                        [
                                            'data' => $SettingsSelectData->language_proficiency,
                                            'theme' => 'my-settings','pluginLoading' => false,
                                            'initValueText' => Yii::t('jobseeker', $SettingsSelectData->language_proficiency[$JobseekerLanguage->proficiency]),
                                            'options' => [
                                                'placeholder' => Yii::t('jobseeker', 'Proficiency'),
                                            ],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ]
                                        ])
                                ?>

                                <?php endforeach; ?>

                                <?php var_dump($editLanguagesFormModel->proficiency); ?>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                                <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Save') ?></button>
                            </div>
                            <?php
                            ActiveForm::end();
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php
                
                Pjax::end(); 
            endif;
            
            ?>
        </div>
        
        <hr class="w-100 d-block d-md-none mb-0">
        
        <div class="col-12 col-md-9 py-4 px-4 profile-info-section-right pos">
            <div class="row mx-0 mb-4">
                 <?= Html::a('Public view', Yii::$app->urlManager->createUrl('jobseeker/my/profile'), ['class' => 'btn btn-secondary text-main br-2']) ?>
                 <?= Html::a('Profile settings', Yii::$app->urlManager->createUrl('jobseeker/my/settings/profile'), ['class' => 'btn btn-primary br-2 ml-2']) ?>
            </div>
            
            <hr class="bt-1 mx-n4">
            
            <?php 
            
            if(!empty($JobseekerCurrentProfile)):   
                
                Pjax::begin([ 'id' => 'JobseekerCurrentProfile', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n3 pb-3']]); 
                
            ?>
            <div class="row mx-0">
                <h1 class="mb-0">
                    <?= $JobseekerCurrentProfile->title ?>
                </h1>
                <span class="col-12 text-secondary px-0 mt-1 mb-3">
                    <?= Yii::t('jobseeker', 'Specializes in <b>{specialization_categories}</b>', ['specialization_categories' => $JobseekerCurrentProfile->categories]) ?>
                </span>
                <p><?= $JobseekerCurrentProfile->description ?></p>
            </div>
            <?php
                
                $SpecializationSkillGroups = \yii\helpers\ArrayHelper::map($JobseekerCurrentProfile->profileSpecializationSkills, 'title', 'description', 'group_title');
                                                                        
                foreach ($SpecializationSkillGroups as $SkillGroupTitle => $SkillGroupSkills):
            ?>
            <div class="row mt-3 mx-0">
                <h5 class="font-weight-normal w-100">
                    <?= Yii::t('jobseeker', $SkillGroupTitle) ?>
                    <span class="profile-control-buttons-container">
                        <button class="profile-control-button ml-3 position-relative" style="top: -3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit employment type">
                            <i class="fas fa-pencil"></i>
                        </button>
                    </span>
                </h5>
                <ul class="profile-skills-list">
                    <?php 
                        foreach ($SkillGroupSkills as $SkillTitle => $SkillDescription):
                    ?>
                    
                    <li data-toggle="popover" data-placement="top" data-trigger="click hover" data-content="<?= $SkillDescription ?>"><?= $SkillTitle; ?></li>

                    
                    <?php
                        endforeach;
                    ?>
                </ul>
            </div>
            <?php endforeach; ?>

            <?php if($JobseekerCurrentProfile->soft_skills): ?>
            <div class="row mt-3 mx-0">
                <h5 class="font-weight-normal w-100">
                    <?= Yii::t('jobseeker', 'Soft skills') ?>
                    <span class="profile-control-buttons-container">
                        <button class="profile-control-button ml-3 position-relative" style="top: -3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit employment type">
                            <i class="fas fa-pencil"></i>
                        </button>
                    </span>
                </h5>
                <ul class="profile-skills-list">

                    <?php foreach (explode(', ', $JobseekerCurrentProfile->soft_skills) as $ProfileSoftSkill): ?>
                    <li data-toggle="popover" data-placement="top" data-trigger="click hover" data-content="<?= common\models\SoftSkills::findOne(['title' => $ProfileSoftSkill])->description ?>"><?= $ProfileSoftSkill ?></li>
                    <?php endforeach; ?>

                </ul>
            </div>
            <?php endif; ?>

            <?php
                
                Pjax::end(); 
            endif;
            
            ?>
            <hr class="bt-1 mx-n4">
            <?php
                
            if(!empty($JobseekerEducation) || !empty($JobseekerAdditionalEducation)):
                    
                Pjax::begin([ 'id' => 'JobseekerEducation', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n4 pb-4']]); 
                
            ?>
            <div class="row mx-0">
                <h5 class="font-weight-normal w-100">
                    <?= Yii::t('jobseeker', 'Education') ?>
                    <span class="profile-control-buttons-container">
                        <button class="profile-control-button ml-3 position-relative" style="top: -3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit employment type">
                            <i class="fas fa-plus"></i>
                        </button>
                    </span>
                </h5>
                
                <div class="col-12 px-0 mt-3" id="profile-education-container">
                    <?php

                        foreach ($JobseekerEducation as $JobseekerEducationItem):
                            $JobseekerEducationTitle = md5($JobseekerEducationItem->educational_institution);
                    ?>
                    <div class="row profile-education-institution" >
                        <div class="col-auto col-education-institution-marker mt-0"> 
                            <i class="fad fa-university fa-2x fa-fw"></i>
                        </div>
                       <div class="col col-education-institution-content mt-n2 mb-2" data-toggle="collapse" data-target="#education-institution--<?= $JobseekerEducationTitle ?>">
                            <p class="text-secondary mb-0 mt-2" style="font-size: 13px;"><?= date('d.m.Y', strtotime($JobseekerEducationItem->education_start_date)) ?> - <?= date('d.m.Y', strtotime($JobseekerEducationItem->education_end_date)) ?></p>
                            <h5 class="mb-0">
                                <?= $JobseekerEducationItem->educational_institution ?> 
                                <span class="h6 text-secondary font-weight-normal ml-2">
                                    <i class="fas fa-map-marker-alt mr-1"></i>
                                    <?= $SettingsSelectData->countries[$JobseekerEducationItem->country] ?>  
                                </span>
                            </h5>
                            <p><?= $JobseekerEducationItem->specialization ?></p>
                            <div class="profile-education-institution-info mx-n2 mt-n2 mb-2 collapse <?= (!empty($JobseekerAdditionalEducationItem->description)) ?: 'd-none' ?>" data-parent="#profile-education-container" id="education-institution--<?= $JobseekerEducationTitle ?>">
                                <?= $JobseekerEducationItem->description ?>
                            </div>
                            <span class="profile-control-buttons-container mr-3 mt-3" style="top: 0;right: 0;">
                                <button class="profile-control-button" style="height: 32px;width: 32px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit employment type">
                                    <i class="fas fa-pencil"></i>
                                </button> 
                                
                                <button class="profile-control-button" style="height: 32px;width: 32px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit employment type">
                                    <i class="fas fa-trash"></i>
                                </button> 
                            </span>
                        </div>
                    </div>
                    <?php
                    
                        endforeach;
                        foreach ($JobseekerAdditionalEducation as $JobseekerAdditionalEducationItem):
                            $JobseekerAdditionalEducationItemTitle = md5($JobseekerAdditionalEducationItem->educational_institution);
                        
                    ?>
                    <div class="row profile-education-course" data-toggle="collapse" data-target="#education-course--<?= $JobseekerAdditionalEducationItemTitle ?>" aria-expanded="true">
                        <div class="col-auto col-education-course-marker mt-0"> 
                            <i class="fad fa-users-class fa-2x fa-fw"></i>
                        </div>
                        <div class="col col-education-course-content mt-n2 mb-2">
                            <p class="text-secondary mb-0 mt-2" style="font-size: 13px;"><?= date('d.m.Y', strtotime($JobseekerAdditionalEducationItem->education_start_date)) ?> - <?= date('d.m.Y', strtotime($JobseekerAdditionalEducationItem->education_end_date)) ?></p>
                            <h5 class="mb-0">
                                <?= $JobseekerAdditionalEducationItem->educational_institution ?> 
                                <span class="h6 text-secondary font-weight-normal ml-2">
                                    <i class="fas fa-map-marker-alt mr-1"></i>
                                    <?= $SettingsSelectData->countries[$JobseekerAdditionalEducationItem->country] ?> 
                                </span>
                            </h5>
                            <p><?= $JobseekerEducationItem->specialization ?></p>
                            <div class="profile-education-course-info word-break text-pre-line mt-n2 mb-2 collapse  <?= (!empty($JobseekerAdditionalEducationItem->description)) ?: 'd-none' ?>" data-parent="#profile-education-container" id="education-course--<?= $JobseekerAdditionalEducationItemTitle ?>" style=""><?= $JobseekerAdditionalEducationItem->description ?></div>

                            <span class="profile-control-buttons-container mr-3 mt-3" style="top: 0;right: 0;">
                                <button class="profile-control-button" style="height: 32px;width: 32px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit employment type">
                                    <i class="fas fa-pencil"></i>
                                </button> 
                                
                                <button class="profile-control-button" style="height: 32px;width: 32px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit employment type">
                                    <i class="fas fa-trash"></i>
                                </button> 
                            </span>
                        </div>
                    </div>
                    <?php
                    
                        endforeach;
                    
                    ?>
                </div>
            </div>
            <?php
                
                Pjax::end(); 
            endif;
            
            ?>
            <hr class="bt-1 mx-n4">
            <?php
                
            if(!empty($JobseekerWorkExperience)):
                    
                Pjax::begin([ 'id' => 'JobseekerWorkExperience', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n4 pb-4']]); 
                
            ?>
            <div class="row mx-0">
                <h5 class="font-weight-normal w-100">
                    <?= Yii::t('jobseeker', 'Employment history') ?>
                    <span class="profile-control-buttons-container">
                        <button class="profile-control-button ml-3 position-relative" style="top: -3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit employment type">
                            <i class="fas fa-plus"></i>
                        </button>
                    </span>
                </h5>
                
                <div class="col-12 px-0 mt-3" id="profile-work-experince">
                    <?php

                        foreach ($JobseekerWorkExperience as $JobseekerWorkExperienceItem):
                            $JobseekerWorkExperienceItemTitle = md5($JobseekerWorkExperienceItem->company_name . $JobseekerWorkExperienceItem->id);
                    ?>
                    <div class="row profile-work-experience">
                        <div class="col-auto col-work-experience-marker mt-0"> 
                            <i class="fad fa-circle fa-2x"></i>
                        </div>
                       <div class="col col-work-experience-content mt-n2 mb-2" data-toggle="collapse" data-target="#work-experince--<?= $JobseekerWorkExperienceItemTitle ?>">
                            <p class="text-secondary mb-0 mt-2" style="font-size: 13px;"><?= $JobseekerWorkExperienceItem->work_start_date ?> - <?= $JobseekerWorkExperienceItem->work_end_date ?></p>
                            <h5 class="mb-0">
                                <?= $JobseekerWorkExperienceItem->company_name ?><span class="font-weight-normal text-secondary"> - <?= $SettingsSelectData->specialization[$JobseekerWorkExperienceItem->company_industry] ?></span>
                                <span class="h6 text-secondary font-weight-normal ml-2">
                                    <i class="fas fa-map-marker-alt mr-1"></i>
                                    <?= $SettingsSelectData->countries[$JobseekerWorkExperienceItem->country] ?>
                                </span>
                            </h5>
                            <p><?= $JobseekerWorkExperienceItem->job_title ?></p>
                            <div class="profile-work-experience-info mx-n2 mt-n2 mb-2 collapse <?= (!empty($JobseekerAdditionalEducationItem->description)) ?: 'd-none' ?>" data-parent="#profile-work-experince" id="work-experince--<?= $JobseekerWorkExperienceItemTitle ?>">
                                    <?= $JobseekerWorkExperienceItem->description ?>
                            </div>
                        </div>
                        <span class="profile-control-buttons-container mr-3 mt-3"  style="top: 0;right: 0;">
                            <div class="profile-control-button-block d-inline-block" data-toggle="modal" data-target="#edit-work-experience-modal--<?= $JobseekerWorkExperienceItemTitle ?>">
                                <button class="profile-control-button" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Edit work experience') ?>">
                                    <i class="fas fa-pencil"></i>
                                </button>                                     
                            </div>
                            <div class="profile-control-button-block d-inline-block" >
                                <?= Html::button('<i class="fas fa-trash"></i>', ['class' => 'profile-control-button', 'style' => 'top:-3px;', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-original-title' => Yii::t('jobseeker', 'Remove work experience'), 'onclick' => 
                                   '$("#JobseekerWorkExperience").toggleClass("loading");
                                    $.ajax({
                                        type: "POST",
                                        url: "",
                                        data: {
                                            action: "JobseekerWorkExperienceRemove",
                                            work_experience_id: "'.$JobseekerWorkExperienceItem->id.'"
                                        }, success: function(response) {
                                            if(response) {
                                                pjaxContainerReload("#JobseekerWorkExperience");
                                            }
                                        }
                                    });'
                                ]) ?>
                            </div>
                        </span>
                    </div>
                    <?php
                    
                        endforeach;
                    
                    ?>
                </div>
            </div>
            <?php
                
                Pjax::end(); 
            endif;
            
            ?>
            <hr class="bt-1 mx-n4">
            <?php
                
            if(!empty($JobseekerOtherExperience)):
                    
                Pjax::begin([ 'id' => 'JobseekerOtherExperience', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n4 pb-4']]); 
                
            ?>
            <div class="row mx-0">
                <h5 class="font-weight-normal w-100">
                    Other experience
                    <span class="profile-control-buttons-container">
                        <button class="profile-control-button ml-3 position-relative" style="top: -3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit employment type">
                            <i class="fas fa-plus"></i>
                        </button>
                    </span>
                </h5>
                
                <div class="col-12 px-0 mt-3" id="profile-other-experince">
                <?php

                    foreach ($JobseekerOtherExperience as $JobseekerOtherExperienceItem):
                        $JobseekerOtherExperienceItemTitle = md5($JobseekerOtherExperienceItem->title);
                        
                ?>
                    <div class="row profile-other-experience">
                        <div class="col-auto col-other-experience-marker mt-0"> 
                            <i class="fad fa-circle fa-2x"></i>
                        </div>
                       <div class="col col-other-experience-content mt-n2 mb-2 pb-5">
                            <h5 class="mb-3 mt-2"><?= $JobseekerOtherExperienceItem->title ?></h5>
                            <p class="profile-other-experience-info" onclick="$(this).toggleClass('expanded');">
                                <?= $JobseekerOtherExperienceItem->description ?>
                            </p>
                            <span class="profile-control-buttons-container mr-3 mt-3"  style="top: 0;right: 0;">
                                <div class="profile-control-button-block d-inline-block" data-toggle="modal" data-target="#edit-other-experience-modal--<?= $JobseekerOtherExperienceItemTitle ?>">
                                    <button class="profile-control-button" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Edit other experience') ?>">
                                        <i class="fas fa-pencil"></i>
                                    </button>                                     
                                </div>
                                <div class="profile-control-button-block d-inline-block" >
                                    <?= Html::button('<i class="fas fa-trash"></i>', ['class' => 'profile-control-button', 'style' => 'top:-3px;', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-original-title' => Yii::t('jobseeker', 'Remove other experience'), 'onclick' => 
                                       '$("#JobseekerOtherExperience").toggleClass("loading");
                                        $.ajax({
                                            type: "POST",
                                            url: "",
                                            data: {
                                                action: "JobseekerOtherExperienceRemove",
                                                other_experience_id: "'.$JobseekerOtherExperienceItem->id.'"
                                            }, success: function(response) {
                                                if(response) {
                                                    pjaxContainerReload("#JobseekerOtherExperience");
                                                }
                                            }
                                        });'
                                    ]) ?>
                                </div>
                            </span>
                        </div>
                    </div>
                <?php

                    endforeach;
                    
                ?>
                </div>
            </div>
            <?php
                
                Pjax::end(); 
            endif;
            
            ?>
            <hr class="bt-1 mx-n4">
            
            <div class="row mx-0">
                <h5 class="font-weight-normal w-100">
                    Testimonials
                    <span class="profile-control-buttons-container">
                        <button class="profile-control-button ml-3 position-relative" style="top: -3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit employment type">
                            <i class="fas fa-plus"></i>
                        </button>
                    </span>
                </h5>

                <div class="col-12 px-0 mt-3" id="profile-testimonials">

                    <div class="row profile-testimonial">
                        <div class="col-auto col-testimonial-marker mt-0">
                            <i class="fad fa-user-tie fa-2x"></i>
                        </div>
                       <div class="col col-testimonial-content mt-n2 mb-2 pb-5">
                            <p class="text-secondary mb-0 mt-2" style="font-size: 13px;">OSCHADBANK</p>
                            <h5 class="mb-0">Martin L.</h5>
                            <p>Director of Marketing</p>
                            <p class="profile-other-experience-info" onclick="$(this).toggleClass('expanded');">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                            <span class="profile-control-buttons-container mr-3 mt-3" style="top: 0;right: 0;">
                                <button class="profile-control-button" style="height: 32px;width: 32px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit employment type">
                                    <i class="fas fa-pencil"></i>
                                </button>

                                <button class="profile-control-button" style="height: 32px;width: 32px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit employment type">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
