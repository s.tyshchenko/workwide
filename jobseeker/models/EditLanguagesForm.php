<?php

namespace jobseeker\models;

use common\models\JobseekerLanguages;
use common\models\Jobseekers;
use Yii;
use yii\base\BaseObject;
use yii\base\Model;
use common\models\ProfileProfessionalSkills;

class EditLanguagesForm extends Model
{
    public $language;
    public $proficiency;

    public function rules()
    {
        return [
            [['language', 'proficiency'], 'required'],
            ['language', 'each', 'rule' => ['string', 'max' => '255']],
            ['proficiency', 'each', 'rule' => ['string', 'max' => '255'], 'skipOnEmpty' => false]
        ];
    }

    public function editLanguages() {

        if(!$this->validate()) {
            return false;
        }

        return json_encode($this->language);

        $Jobseeker = Jobseekers::findOne(['user_id' => Yii::$app->user->id]);
        $JobseekerLanguages = JobseekerLanguages::find()->where(['jobseeker_id' => $Jobseeker->id]);

        foreach($JobseekerLanguages as $jobseeker_language)
        {
            if($key = array_search($this->language, $jobseeker_language->language))
            {
                $jobseeker_language->proficiency = $this->proficiency[$key];
                $jobseeker_language->save();
            }
            else
            {
                $jobseeker_language->delete();
            }
        }

        return true;
    }
}