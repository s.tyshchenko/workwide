<?php

namespace jobseeker\models;

use Yii;
use yii\base\Model;
use common\models\Jobseekers;
use common\models\JobseekerProfiles;

class AddProfileForm extends Model 
{
    public $title;
    public $description;
    public $visibility; 
    public $employment_type;
    public $salary;
    public $salary_currency;
    public $specialization;
    public $categories;
        
    public function rules()
    {
        return [
            [['title', 'employment_type', 'specialization', 'categories'], 'required'],
            
            ['description', 'string', 'min' => 100, 'max' => 1000],
            ['employment_type', 'each', 'rule' => ['string']],
            ['salary', 'integer'],
            ['salary_currency', 'string'],
//            ['salary', 'exist', 'targetAttribute' => 'salary_currency'],
            ['specialization', 'string'],
            ['categories', 'each', 'rule' => ['string']],
            
            [['salary_currency'], 'required', 'when' => function($model){
                return $model->salary != null;
            }, 'whenClient' => "function (attribute, value) {
                return $('#add-profile-form-salary').val() != null;
            }"]
        ];
    }
    
    public function addProfile() {
        if (!$this->validate()) {
            return Yii::$app->response->redirect(Yii::$app->urlManager->createUrl('jobseeker/my/settings/profile'));
        }
        
        $jobseeker_profile = new JobseekerProfiles();
        $jobseeker_profile->title = $this->title;
        $jobseeker_profile->jobseeker_id = Jobseekers::find(['id' => Yii::$app->user->id])->one()->id;
        $jobseeker_profile->description = $this->description;
        $jobseeker_profile->employment_type = implode(', ', $this->employment_type);
        $jobseeker_profile->salary = $this->salary;
        $jobseeker_profile->salary_currency = $this->salary_currency;
        $jobseeker_profile->specialization = $this->specialization;
        $jobseeker_profile->categories = implode(', ', $this->categories);
        $jobseeker_profile->is_general = false;
        
        $jobseeker_profile->save();
        
        return Yii::$app->response->redirect(Yii::$app->urlManager->createUrl('jobseeker/my/settings/profile'));
        
    }
}