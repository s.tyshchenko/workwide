<?php

namespace jobseeker\models;

use Yii;
use yii\base\Model;
use common\models\Users;
use common\models\Jobseekers;
use borales\extensions\phoneInput\PhoneInputValidator;
use jobseeker\components\validators\PhoneVerificationCodeValidator;

class SignupForm extends Model 
{
    public $email; 
    public $phone;
    public $full_name;
    public $password;
    public $password_repeat;
    public $phone_verification_code;
    
    public  function rules()
    {
        return [
            ['full_name', 'trim'],
            ['full_name', 'required'],
            ['full_name', 'string', 'min' => 3, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\Users', 'message' => Yii::t('jobseeker', 'This email address is already registered.')],
            
            ['phone', 'required'],
            ['phone', 'unique', 'targetClass' => '\common\models\Users', 'message' => Yii::t('jobseeker', 'This phone is already registered.')],
            ['phone', PhoneInputValidator::className(), 'message' => Yii::t('jobseeker', 'Invalid phone number')],
            
            ['phone_verification_code', 'required'],
            ['phone_verification_code', PhoneVerificationCodeValidator::className(), 'message' => Yii::t('jobseeker', 'Invalid code')],
            
            ['password', 'required'],
            ['password', 'string', 'min' => 8],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t('jobseeker', "Passwords don't match") ],
        ];
    }
    
    public function scenarios()
    {
        $scenarios = [
            'phone_verify' => ['full_name', 'email', 'password', 'password_repeat', 'phone_verification_code', 'phone'],
        ];

        return array_merge(parent::scenarios(), $scenarios);
    }
    
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new Users();
        $user->email    = $this->email;
        $user->phone    = $this->phone;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->status   = $user::STATUS_ACTIVE;
        $user->role     = $user::ROLE_JOBSEEKER;
        $user->generateEmailVerificationToken();        
        $user->save();
        
        $jobseeker = new Jobseekers();
        $jobseeker->full_name   = $this->full_name;
        $jobseeker->user_id     = $user->getId();
        $jobseeker->timezone    = Yii::$app->geoip->getInfoByIP()->location->timezone;
        $jobseeker->timezone    = Yii::$app->geoip->getInfoByIP()->city;
        
        return $jobseeker->save();
    }
    
        /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }
    
}