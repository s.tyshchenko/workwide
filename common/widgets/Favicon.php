<?php

namespace common\widgets;

class Favicon extends \yii\bootstrap4\Widget
{
    public function init(){}

    public function run() {
        return $this->render('favicon/view');
    }
}