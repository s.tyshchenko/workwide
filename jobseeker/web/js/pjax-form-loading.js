function pjaxContainerReload(pjaxContainer, url = []){
    if(url.length) {
        $.pjax.reload({container: pjaxContainer, url: url, timeout: 5000, async: false});
    } else {
        $.pjax.reload({container: pjaxContainer, timeout: 5000, async: false});
    }

    setTimeout(function(){ $(pjaxContainer).toggleClass('loading'); }, 3000);
}


function submitFormAjax(form, pjaxContainer) {
    if ($(form).find('.has-error').length) 
        return false;

    $.ajax({
        method: 'POST',
        url: '',
        dataType: 'json',
        data: $(form).serialize(),
        success: function(response){
            if(response){
                $(pjaxContainer).toggleClass('loading');
                $(form).parents('.modal').modal('hide');
                pjaxContainerReload(pjaxContainer);
            }else{
                console.log('Validation error');
            }
        }
    });

    return false;
}