<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class SpecializationSkills extends ActiveRecord {

    public static function tableName()
    {
        return '_specialization_skills';
    }
    
    public function fields()
    {
        return ['title', 'group_title', 'description', 'specialization_id'];
    }
    
    public function combineSpecializationSkills($specialization_id = 1) {
                
        $return_array = \yii\helpers\ArrayHelper::map(self::find(['specialization_id' => $specialization_id])->all(), 'title', 'title', 'group_title');
        
        return $return_array;
    }
}