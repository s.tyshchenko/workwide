<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Countries extends ActiveRecord {

    public static function tableName()
    {
        return '_countries';
    }
    
    public function fields()
    {
        return ['code', 'title'];
    }
    
    public function combineCountries() {
        $return_array = [];
        foreach (self::find()->all() as $Country)
        {
            $return_array[$Country->code] = Yii::t('jobseeker', $Country->title); 
        }
        
        return $return_array;
    }
}