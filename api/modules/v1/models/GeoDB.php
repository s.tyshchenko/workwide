<?php

namespace api\modules\v1\models;

class GeoDB {
    
    public $languageCode;

    
    function __construct() {
        $this->languageCode = Yii::$app->language;
    }

    public function getCountries($data = ['currencyCode' => '', 'offset' => '', 'limit' => '', 'namePrefix' => '', 'asciiMode' => false, 'languageCode' => 'en']) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $curl = curl_init();

        curl_setopt_array($curl, [
                CURLOPT_URL => "https://wft-geo-db.p.rapidapi.com/v1/geo/countries?" . http_build_query($data),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => [
                        "x-rapidapi-host: wft-geo-db.p.rapidapi.com",
                        "x-rapidapi-key: d53204ea6cmshec6074d3e1ed68bp1eee4bjsn9bfcdf28a6c2"
                ],
        ]);

        $response = json_decode(curl_exec($curl));
        $err = curl_error($curl);

        curl_close($curl);

        $results_array['results'][] = ['id' => $response->data[0]->code,'text' => $response->data[0]->name];
        
        if ($err) {
                return "cURL Error #:" . $err;
        } else {
                return $results_array;
        }
    }
    
    public function getCountryDetails($countryId, $data = ['asciiMode' => false, 'languageCode' => '']) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $curl = curl_init();

        curl_setopt_array($curl, [
                CURLOPT_URL => "https://wft-geo-db.p.rapidapi.com/v1/geo/countries/" . $countryId . "?" . http_build_query($data),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => [
                        "x-rapidapi-host: wft-geo-db.p.rapidapi.com",
                        "x-rapidapi-key: d53204ea6cmshec6074d3e1ed68bp1eee4bjsn9bfcdf28a6c2"
                ],
        ]);

        $response = json_decode(curl_exec($curl));
        $err = curl_error($curl);

        curl_close($curl);

        $results_array['result'] = $response->data;
        
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $results_array;
        }
    }
    
    public function getCities($data = ['limit' => '', 'offset' => '', 'countryIds' => '', 'includeDeleted' => false, 'minPopulation' => 0, 'namePrefix' => '', 'location' => '', 'radius' => '', 'distanceUnit' => 'KM', 'excludedCountryIds' => '', 'sort' => 'population', 'timeZoneIds' => '',  'types' => 'CITY', 'asciiMode' => false, 'languageCode' => '']) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $curl = curl_init();

        curl_setopt_array($curl, [
                CURLOPT_URL => "https://wft-geo-db.p.rapidapi.com/v1/geo/cities?" . http_build_query($data),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => [
                        "x-rapidapi-host: wft-geo-db.p.rapidapi.com",
                        "x-rapidapi-key: d53204ea6cmshec6074d3e1ed68bp1eee4bjsn9bfcdf28a6c2"
                ],
        ]);

        $response = json_decode(curl_exec($curl));
        $err = curl_error($curl);

        curl_close($curl);

        $results_array['results'][] = ['id' => $response->data[0]->id,'text' => $response->data[0]->name];
        
        if ($err) {
                return "cURL Error #:" . $err;
        } else {
                return $results_array;
        }
    }
    
    public function getCityDetails($cityId, $data = ['asciiMode' => false, 'languageCode' => '']) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $curl = curl_init();

        curl_setopt_array($curl, [
                CURLOPT_URL => "https://wft-geo-db.p.rapidapi.com/v1/geo/cities/" . $cityId . "?" . http_build_query($data),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => [
                        "x-rapidapi-host: wft-geo-db.p.rapidapi.com",
                        "x-rapidapi-key: d53204ea6cmshec6074d3e1ed68bp1eee4bjsn9bfcdf28a6c2"
                ],
        ]);

        $response = json_decode(curl_exec($curl));
        $err = curl_error($curl);

        curl_close($curl);

        $results_array['result'] = $response->data;
        
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $results_array;
        }
    }
    
    public function getTimezoneTime($timezoneId)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $curl = curl_init();

        curl_setopt_array($curl, [
                CURLOPT_URL => "https://wft-geo-db.p.rapidapi.com/v1/locale/timezones/" . $timezoneId . "/time",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => [
                        "x-rapidapi-host: wft-geo-db.p.rapidapi.com",
                        "x-rapidapi-key: d53204ea6cmshec6074d3e1ed68bp1eee4bjsn9bfcdf28a6c2"
                ],
        ]);

        $response = json_decode(curl_exec($curl));
        $err = curl_error($curl);

        curl_close($curl);

        $results_array['results'][] = $response->data;
        
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $results_array;
        }
    }
    
}