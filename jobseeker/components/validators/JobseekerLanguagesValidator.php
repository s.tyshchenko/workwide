<?php

namespace jobseeker\components\validators;

use Yii;
use yii\validators\Validator;
use yii\helpers\Json;
use common\models\Jobseekers;
use common\models\JobseekerLanguages;

class JobseekerLanguagesValidator extends Validator
{
    public function init() {
        if (!$this->message) {
            $this->message = Yii::t('jobseeker', 'Language already added');
        }
        parent::init();
    }
    
    public function validateAttribute($model, $attribute)
    {
        if (JobseekerLanguages::find()->where(['language' => $model->$attribute, 'jobseeker_id' => Jobseekers::find(['user_id' => Yii::$app->user->id])->one()->id])->one())
        {
            $this->addError($model, $attribute, $this->message);
        }
    }
    
    public function clientValidateAttribute($model, $attribute, $view)
    {
        $options = Json::htmlEncode([
            'message' => $this->message
        ]);
        
        return <<<JS
            var options = $options;
            var formdata = new FormData(document.querySelector('#add-language-form'));
                formdata.append('action', 'JobseekerLanguageValidate');
            $.ajax({
                async: false,
                url: '',
                data: formdata,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(message){
                    if(!$.isEmptyObject(JSON.parse(message)))
                         messages.push(options.message);
                }
            });
        JS;
    }
    
}