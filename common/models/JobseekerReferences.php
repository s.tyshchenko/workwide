<?php

namespace common\models;

use Yii;
use common\models\Jobseekers;
use yii\db\ActiveRecord;

class JobseekerReferences extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jobseeker_references';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['jobseeker_id', 'required'],
            ['referrer_full_name', 'required'],
            ['referrer_phone', 'required'],
            ['referrer_email', 'required'],
        ];
    }
    
    public function getJobseeker()
    {
        return $this->hasOne(Jobseekers::className(), ['id' => 'jobseeker_id']);
    }
}