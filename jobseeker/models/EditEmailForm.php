<?php

namespace jobseeker\models;

use Yii;
use yii\base\Model;
use common\models\Users;

class EditEmailForm extends Model 
{
    public $email; 
        
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\Users', 'message' => Yii::t('jobseeker', 'This email address is already registered.')],
        ];
    }
    
    public function editEmail() {
        if(!$this->validate()) {
            return false;
        }
        
        $user = Users::findOne(['id' => Yii::$app->user->id]);
        $user->email = $this->email;
                
        //SEND VERIFICATION CODE
        
        return $user->save();        
    }
}