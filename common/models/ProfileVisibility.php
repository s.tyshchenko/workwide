<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class ProfileVisibility extends ActiveRecord {

    public static function tableName()
    {
        return '_profile_visibility';
    }
    
    public function fields()
    {
        return ['name', 'title'];
    }
    
    public function combineProfileVisibility() {
        $return_array = [];
        foreach (self::find()->all() as $ProfileVisibility)
        {
            $return_array[$ProfileVisibility->name] = Yii::t('jobseeker', $ProfileVisibility->title); 
        }
        
        return $return_array;
    }
}