<?php

use yii\helpers\Html;
use yii\helpers\Url;

$current_url = explode('/', Url::current());
$current_url = array_pop($current_url);

?>


        <nav class="my-settings-nav d-none d-md-block col-3">
            <ul class="list-group list-group-flush br-2">
                <li class="list-group-item <?= (($current_url == 'account') ? 'active' : '') ?>">
                    <a href="<?= Yii::$app->urlManager->createUrl('jobseeker/my/settings/account') ?>">
                        <i class="fad fa-user mr-2"></i>
                        <?= Yii::t('jobseeker', 'Account') ?>
                    </a>
                </li>
                <li class="list-group-item  <?= (($current_url == '') ? 'active' : '') ?>">
                    <a href="<?= Yii::$app->urlManager->createUrl('jobseeker/my/profile') ?>">
                        <i class="fad fa-id-card-alt mr-2"></i>
                        <?= Yii::t('jobseeker', 'Profile') ?>
                    </a>
                </li>
                <li class="list-group-item  <?= (($current_url == 'profile') ? 'active' : '') ?>">
                    <a href="<?= Yii::$app->urlManager->createUrl('jobseeker/my/settings/profile') ?>">
                        <i class="fad fa-users-cog mr-2"></i>
                        <?= Yii::t('jobseeker', 'Profile settings') ?>
                    </a>
                </li>
                <li class="list-group-item  <?= (($current_url == 'notifications') ? 'active' : '') ?>">
                    <a href="<?= Yii::$app->urlManager->createUrl('jobseeker/my/settings/notifications') ?>">
                        <i class="fad fa-envelope-open-text mr-2"></i>
                        <?= Yii::t('jobseeker', 'Messages & Notifications') ?>
                    </a>                    
                </li>
                <li class="list-group-item  <?= (($current_url == 'password') ? 'active' : '') ?>">
                    <a href="<?= Yii::$app->urlManager->createUrl('jobseeker/my/settings/security') ?>">
                        <i class="fad fa-shield-alt mr-2"></i>
                        <?= Yii::t('jobseeker', 'Password & Security') ?>
                    </a>
                </li>
            </ul>
        </nav>
        
        <nav class="my-settings-nav d-block d-md-none col-12 px-0 mb-4 mt-n4">
            <ul class="list-group list-group-horizontal br-2 b-1">
                <li class="list-group-item b-0 br-1 <?= (($current_url == 'account') ? 'active' : '') ?>">
                    <a href="<?= Yii::$app->urlManager->createUrl('jobseeker/my/settings/account') ?>">
                        <i class="fad fa-user mr-2"></i>
                        <?= Yii::t('jobseeker', 'Account') ?>
                    </a>
                </li>
                <li class="list-group-item b-0 br-1 <?= (($current_url == '') ? 'active' : '') ?>">
                    <a href="<?= Yii::$app->urlManager->createUrl('jobseeker/my/profile') ?>">
                        <i class="fad fa-id-card-alt mr-2"></i>
                        <?= Yii::t('jobseeker', 'Profile') ?>
                    </a>
                </li>
                <li class="list-group-item b-0 br-1 <?= (($current_url == 'profile') ? 'active' : '') ?>">
                    <a href="<?= Yii::$app->urlManager->createUrl('jobseeker/my/settings/profile') ?>">
                        <i class="fad fa-users-cog mr-2"></i>
                        <?= Yii::t('jobseeker', 'Profile settings') ?>
                    </a>
                </li>
                <li class="list-group-item b-0 br-1 <?= (($current_url == 'notifications') ? 'active' : '') ?>">
                    <a href="<?= Yii::$app->urlManager->createUrl('jobseeker/my/settings/notifications') ?>">
                        <i class="fad fa-envelope-open-text mr-2"></i>
                        <?= Yii::t('jobseeker', 'Messages & Notifications') ?>
                    </a>
                </li>
                <li class="list-group-item b-0 <?= (($current_url == 'password') ? 'active' : '') ?>">
                    <a href="<?= Yii::$app->urlManager->createUrl('jobseeker/my/settings/security') ?>">
                        <i class="fad fa-shield-alt mr-2"></i>
                        <?= Yii::t('jobseeker', 'Password & Security') ?>
                    </a>
                </li>
            </ul>
        </nav>