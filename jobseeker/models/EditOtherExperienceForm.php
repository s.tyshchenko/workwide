<?php

namespace jobseeker\models;

use Yii;
use yii\base\Model;
use common\models\Jobseekers;
use common\models\JobseekerOtherExperience;

class EditOtherExperienceForm extends Model 
{
    public $other_experience_id;
    public $title;
    public $description;
       
    
    public function rules()
    {
        return [
            [['title', 'description', 'other_experience_id'], 'required'],
            ['description', 'string', 'max' => 1000]
        ];
    }
    
    public function editOtherExperience() {
        if(!$this->validate()) {
            return false;
        }
        
        $jobseeker_other_experience = JobseekerOtherExperience::find()->where(['jobseeker_id' => Jobseekers::find(['id' => Yii::$app->user->id])->one()->id, 'id' => $this->other_experience_id])->one();
        $jobseeker_other_experience->title = $this->title;
        $jobseeker_other_experience->description = $this->description;
        
        return $jobseeker_other_experience->save();
        
    }
}