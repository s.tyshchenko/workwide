<?php

namespace common\models;

use Yii;
use common\models\JobseekerProfiles;
use yii\db\ActiveRecord;

class ProfileEmploymentType extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile_employment_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['profile_id', 'required'],
            ['full_time', 'required'],
            ['part_time', 'required'],
            ['remote', 'required'],
        ];
    }
    
    public function getJobseeker()
    {
        return $this->hasOne(JobseekerProfiles::className(), ['id' => 'profile_id']);
    }
}