<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class i18nLanguages extends ActiveRecord {

    public static function tableName()
    {
        return '_i18n_languages';
    }
    
    public function fields()
    {
        return ['id', 'english_name'];
    }
    
    public function rules()
    {
        return [
            ['id', 'unique'],
        ];
    }
}