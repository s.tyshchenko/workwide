<?php

namespace common\models;

use Yii;
use common\models\Jobseekers;
use common\models\i18nLanguages;
use yii\db\ActiveRecord;

class JobseekerLanguages extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jobseeker_languages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['jobseeker_id', 'required'],
            ['language', 'required'],
            ['proficiency', 'required']
        ];		
    }
    
    public function getJobseeker()
    {
        return $this->hasOne(Jobseekers::className(), ['id' => 'jobseeker_id']);
    }
    
    public function geti18nLanguage()
    {
        return $this->hasOne(i18nLanguages::className(), ['id' => 'language']);
    }
    
}