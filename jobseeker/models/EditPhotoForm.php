<?php

namespace jobseeker\models;

use Yii;
use yii\base\Model;
use common\models\Jobseekers;
use common\models\JobseekerPhotos;
use yii\web\UploadedFile;
use common\models\Images;

class EditPhotoForm extends Model 
{
    public $photo; 
    public $crop_x1, $crop_y1, $crop_width, $crop_height; 

    public function rules()
    {
        return [
            [['photo'], 'image', 'skipOnEmpty' => true, 'extensions' => 'png,jpeg,jpg', 'minWidth' => 400, 'minHeight' => 400, 'maxSize' => 1024 * 1024 * 10],
            [['crop_x1', 'crop_y1', 'crop_width', 'crop_height'], 'required', 'skipOnEmpty' => true],
        ];
    }
    
    public function uploadPhoto() {
        if(!$this->validate()) {
            return false;
        }      
                
        $Jobseeker = Jobseekers::findJobseekerByUserId(Yii::$app->user->id);
        
        $jobseekerPhotoModel = JobseekerPhotos::findOne(['jobseeker_id' => $Jobseeker->id]);
        
        if($jobseekerPhotoModel == null)
        {
            $jobseekerPhotoModel = new JobseekerPhotos(); 
        }
        else
        {
            unlink(Yii::getAlias('@common') . '/web' . $jobseekerPhotoModel->path);
            unlink(Yii::getAlias('@common') . '/web' . $jobseekerPhotoModel->path_webp);
        }
        
        $global_path        = Images::generateImageDir('jobseeker-' . $Jobseeker->id . '-photo', $this->photo->extension, '/web/images');
        $global_path_webp   = Images::generateImageDir('jobseeker-' . $Jobseeker->id . '-photo', 'webp', '/web/images');
        
        $local_path         = substr($global_path, strlen(Yii::getAlias('@common').'/web'));
        $local_path_webp    = substr($global_path_webp, strlen(Yii::getAlias('@common').'/web'));
        
        $jobseekerPhotoModel->jobseeker_id  = $Jobseeker->id;
        $jobseekerPhotoModel->extension     = $this->photo->extension;
        $jobseekerPhotoModel->path      = $local_path;
        $jobseekerPhotoModel->path_webp = $local_path_webp;
               
        $jobseekerPhotoModel->path_cropped = null;
        $jobseekerPhotoModel->crop_x1 = null;
        $jobseekerPhotoModel->crop_y1 = null;
        $jobseekerPhotoModel->crop_width = null;
        $jobseekerPhotoModel->crop_height = null;
        
        $this->photo->saveAs($global_path, false);
        
        $jobseekerPhotoModel->save();
    
        if($this->photo->extension == 'jpeg' || $this->photo->extension == 'jpg')
            $img = imagecreatefromjpeg($global_path);
        elseif($this->photo->extension == 'png')
            $img = imagecreatefrompng($global_path);
        
        imagepalettetotruecolor($img);
        imagealphablending($img, true);
        imagesavealpha($img, true);
        
        if($this->photo->extension == 'jpeg' || $this->photo->extension == 'jpg')
            imagejpeg($img, $global_path, -1);
        elseif($this->photo->extension == 'png')
            imagepng($img, $global_path, -1);
        
        imagewebp($img, $global_path_webp, -1);
        
        return 'photo_upload_success';
    }
    
    public function editPhoto() {
        if(!$this->validate()) {
            return false;
        }
        
        $crop_array = [
            'x' => $this->crop_x1 , 
            'y' => $this->crop_y1, 
            'width' => $this->crop_width, 
            'height'=> $this->crop_height
        ];
        
        $Jobseeker = Jobseekers::findJobseekerByUserId(Yii::$app->user->id);
        
        $jobseekerPhotoModel = JobseekerPhotos::findOne(['jobseeker_id' => $Jobseeker->id]);
        
        if($jobseekerPhotoModel->path_cropped != null)
        {
            unlink(Yii::getAlias('@common') . '/web' . $jobseekerPhotoModel->path_cropped);
        }
        
        $local_path         = $jobseekerPhotoModel->path;
        $global_path        = Yii::getAlias('@common') . '/web' . $local_path;
        
        $global_path_cropped    = Images::generateImageDir('jobseeker-' . $Jobseeker->id . '-photo-' . implode('-', $crop_array), $jobseekerPhotoModel->extension, '/web/images');
        $local_path_cropped     = substr($global_path_cropped, strlen(Yii::getAlias('@common').'/web')); 
               
        
        $info = getimagesize($global_path);
        if ($jobseekerPhotoModel->extension == 'jpeg' || $jobseekerPhotoModel->extension == 'jpg') 
        {   
            $image = imagecreatefromjpeg($global_path);
            $image_cropped = imagecrop($image, $crop_array);
            imagejpeg($image_cropped, $global_path_cropped , -1);
        }
        elseif ($info['mime'] == 'image/png') 
        {   
            $image = imagecreatefrompng($global_path);
            $image_cropped = imagecrop($image, $crop_array);
            imagepng($image_cropped, $global_path_cropped, -1);
        }
        
        $jobseekerPhotoModel->crop_x1       = $this->crop_x1;
        $jobseekerPhotoModel->crop_y1       = $this->crop_y1;
        $jobseekerPhotoModel->crop_width    = $this->crop_width;
        $jobseekerPhotoModel->crop_height   = $this->crop_height;
        $jobseekerPhotoModel->path_cropped  = $local_path_cropped;
        
        return $jobseekerPhotoModel->save();        
    }
}