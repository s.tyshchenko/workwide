<?php

namespace common\models;

use Yii;
use common\models\SpecializationCategories;
use yii\db\ActiveRecord;

class Specializations extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '_specializations';
    }

    public function fields()
    {
        return ['id', 'title'];
    }

    public function extraFields()
    {
        return ['categories'];
    }
    
    public function rules()
    {
        return [
            ['id', 'unique'],
        ];
    }
    
    public function getCategories()
    {
        return $this->hasMany(SpecializationCategories::className(), ['specialization_id' => 'id']);
    }
    
    public function combineSpecializations()
    {
        $return_array = [];
        foreach (self::find()->all() as $Specialization)
        {
            $return_array[$Specialization->id] = Yii::t('jobseeker', $Specialization->title); 
        }
        
        return $return_array;
    }
}