<?php

namespace jobseeker\models;

use Yii;
use yii\base\Model;
use common\models\Jobseekers;
use common\models\JobseekerAdditionalEducation;

use common\models\Countries;

class AddAdditionalEducationForm extends Model 
{
    public $educational_institution;
    public $specialization;
    public $country;
    public $education_start_date_month;
    public $education_start_date_year;
    public $education_end_date_month;
    public $education_end_date_year;
    public $description;
    
    public function rules()
    {
        return [
            [['specialization', 'country', 'education_start_date_year', 'education_end_date_year', 'education_start_date_month', 'education_end_date_month'], 'required'],
            ['educational_institution', 'string'],
            ['specialization', 'string'],
            ['country', 'string'],
            ['description', 'string', 'length' => 1000],
            [['education_start_date_month', 'education_end_date_month'], 'integer', 'min' => 1, 'max' => 12],
            [['education_start_date_year', 'education_end_date_year'], 'integer', 'min' => 1900],
            ['education_end_date_year', 'compare', 'operator' => '>=', 'compareAttribute' => 'education_start_date_year']
        ];
    }
    
    public function addAdditionalEducation() {
        if (!$this->validate()) {
            return Yii::$app->response->redirect(Yii::$app->urlManager->createUrl('jobseeker/my/settings/profile'));
        }
        
        if(!Countries::findOne(['code' => $this->country]))
        {
            
            /*
             * Remove with non-Basic GeoDB Plan
             */
            sleep(2);
            
            $country_data = json_decode(file_get_contents("http://34.123.108.88/api/v1/geodb/country-details?countryId=" . $this->country))->result;
            
            $Country = new Countries();
            
            $Country->code = $this->country;
            $Country->title = $country_data->name;
            
            $Country->save();
        }
        
        $jobseeker_education = new JobseekerAdditionalEducation();
        $jobseeker_education->educational_institution   = $this->educational_institution;
        $jobseeker_education->specialization            = $this->specialization;
        $jobseeker_education->country                   = $this->country;
        $jobseeker_education->description               = $this->description;
        $jobseeker_education->education_start_date      = date('Y-m-d', strtotime('01.'.$this->education_start_date_month.'.'.$this->education_start_date_year));
        $jobseeker_education->education_end_date        = date('Y-m-d', strtotime('01.'.$this->education_end_date_month.'.'.$this->education_end_date_year));
        
        $jobseeker_education->jobseeker_id = Jobseekers::find(['id' => Yii::$app->user->id])->one()->id;
        
        $jobseeker_education->save();
        
        return Yii::$app->response->redirect(Yii::$app->urlManager->createUrl('jobseeker/my/settings/profile'));
        
    }
}