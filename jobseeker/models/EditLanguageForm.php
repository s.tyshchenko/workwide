<?php

namespace jobseeker\models;

use Yii;
use yii\base\Model;
use common\models\JobseekerLanguages;
use common\models\Jobseekers;

class EditLanguageForm extends Model 
{
    public $language;
    public $proficiency; 
        
    public function rules()
    {
        return [
            [['language', 'proficiency'], 'required'],
        ];
    }
    
    public function editLanguage() {
        if(!$this->validate()) {
            return false;
        }
        
        $language = JobseekerLanguages::findOne(
                [
                    'jobseeker_id' => Jobseekers::find(['id' => Yii::$app->user->id])->one()->id,
                    'language' => $this->language
                ]);
        $language->proficiency = $this->proficiency;
        
        return $language->save();
        
    }
}