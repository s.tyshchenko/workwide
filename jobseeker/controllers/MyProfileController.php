<?php

namespace jobseeker\controllers;

use Yii;
use yii\web\Controller;
use common\models\Users;
use common\models\JobseekerProfiles;
use common\models\JobseekerLanguages;
use common\models\JobseekerEducation;
use common\models\JobseekerAdditionalEducation;
use common\models\JobseekerWorkExperience;
use common\models\JobseekerOtherExperience;
use common\models\JobseekerPhotos;
use yii\widgets\ActiveForm;
use yii\web\Response;

class MyProfileController extends Controller
{

    public function actionIndex()
    {
        $this->layout = 'account';
        
        $User = Users::findOne(Yii::$app->user->id);
        $Jobseeker = $User->jobseeker;

        $editLanguagesForm           = new \jobseeker\models\EditLanguagesForm();

        /*
         * GET Request
         */

        if(!empty(Yii::$app->request->get('profile_id')))
        {
            $JobseekerCurrentProfile = JobseekerProfiles::find()->where(['jobseeker_id' => $Jobseeker->id, 'id' => Yii::$app->request->get('profile_id')])->with(['profileProfessionalSkills', 'profileSpecializationSkills'])->one();
        }
        else
        {
            $JobseekerCurrentProfile = JobseekerProfiles::find()->where(['jobseeker_id' => $Jobseeker->id, 'is_general' => true])->with(['profileProfessionalSkills', 'profileSpecializationSkills'])->one();
        }

        /*
         * PJAX Request
         */

        if(!empty(Yii::$app->request->get()['_pjax']))
        {
            $renderParams = ['User' => $User, 'Jobseeker' => $Jobseeker];

            switch(Yii::$app->request->get()['_pjax']) {
                case  '#JobseekerLanguages':
                    $renderParams['editLanguagesFormModel'] = $editLanguagesForm;
                    $renderParams['JobseekerLanguages'] = JobseekerLanguages::find(['jobseeker_id' => $Jobseeker->id])->all();
                    break;
            }
        }

        /*
         * POST request
         */

        if($editLanguagesForm->load(Yii::$app->request->post()))
        {
            return $editLanguagesForm->editLanguages();
        }

            return $this->render('index', [
            'User'          => $User,
            'Jobseeker'     => $Jobseeker,

            'JobseekerProfiles'             => JobseekerProfiles::find()->where(['jobseeker_id' => $Jobseeker->id])->with(['profileProfessionalSkills', 'profileSpecializationSkills'])->all(),
            'JobseekerCurrentProfile'       => $JobseekerCurrentProfile,
            'JobseekerLanguages'            => JobseekerLanguages::find(['jobseeker_id' => $Jobseeker->id])->all(),
            'JobseekerEducation'            => JobseekerEducation::find(['jobseeker_id' => $Jobseeker->id])->all(),
            'JobseekerAdditionalEducation'  => JobseekerAdditionalEducation::find(['jobseeker_id' => $Jobseeker->id])->all(),
            'JobseekerWorkExperience'       => JobseekerWorkExperience::find(['jobseeker_id' => $Jobseeker->id])->all(),
            'JobseekerOtherExperience'      => JobseekerOtherExperience::find(['jobseeker_id' => $Jobseeker->id])->all(),

            'editLanguagesFormModel'             => $editLanguagesForm,
        ]);
    }
    
   
}