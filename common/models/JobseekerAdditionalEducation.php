<?php

namespace common\models;

use Yii;
use common\models\Jobseekers;
use yii\db\ActiveRecord;

class JobseekerAdditionalEducation extends ActiveRecord
{
    
    public static function tableName()
    {
        return 'jobseeker_additional_education';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['jobseeker_id', 'required'],
        ];
    }
    
    public function getJobseeker()
    {
        return $this->hasOne(Jobseekers::className(), ['id' => 'jobseeker_id']);
    }
}