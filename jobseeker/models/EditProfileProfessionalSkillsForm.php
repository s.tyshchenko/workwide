<?php

namespace jobseeker\models;

use Yii;
use yii\base\Model;
use common\models\ProfileProfessionalSkills;

class EditProfileProfessionalSkillsForm extends Model 
{
    public $skill;
    public $skill_checked;
    public $proficiency;
    public $last_used;
    public $years_of_experience;
    public $profile_id;
        
    public function rules()
    {
        return [
            [['skill', 'profile_id', 'skill_checked'], 'required'],
            ['profile_id', 'integer'],
            ['skill', 'each', 'rule' => ['string', 'max' => '255']],
            ['skill_checked', 'each', 'rule' => ['boolean']],
            ['proficiency', 'each', 'rule' => ['string', 'max' => '255'], 'skipOnEmpty' => true],
            ['last_used', 'each', 'rule' => ['string', 'max' => '255'], 'skipOnEmpty' => true],
            ['years_of_experience', 'each', 'rule' => ['integer'], 'skipOnEmpty' => true],
        ];
    }
    
    public function editProfileProfessionalSkills() {

        if(!$this->validate()) {
            return false;
        }
       
        foreach($this->skill as $key => $value){
            
            if($this->skill_checked[$key])
            {
                if(ProfileProfessionalSkills::find()->where(['skill' => $value])->exists())
                    $profile_skill = ProfileProfessionalSkills::find()->where(['skill' => $value])->one();
                else
                    $profile_skill = new ProfileProfessionalSkills();

                $profile_skill->profile_id  = $this->profile_id;
                $profile_skill->skill       = $this->skill[$key];
                $profile_skill->proficiency = $this->proficiency[$key];
                $profile_skill->last_used   = $this->last_used[$key];
                $profile_skill->years_of_experience = $this->years_of_experience[$key];

                $profile_skill->save();
            }
            else 
            {
                if(ProfileProfessionalSkills::find()->where(['skill' => $value])->exists())
                    ProfileProfessionalSkills::findOne(['skill' => $value])->delete();
            }
        }
        
        return true;
    }
}