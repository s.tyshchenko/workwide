<?php
return [
    'id' => 'app',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
        ],
        'geoip' => [
            'class' => 'common\components\GeoIP',
            'mmdb' => '@common/components/geoip/geoip_city.mmdb',
            'lng' => 'en'
        ],
    ],
    'modules' => [
//        'api' => [
//            'class' => 'api\modules\v1\Module',
//            'controllerNamespace' => 'api\modules\v1\controllers',
//        ],
        'adminpanel' => [
            'class' => 'backend\Module',
//            'layout' => '@app/views/layouts/admin',
            'modules' => [
//                'user' => [
//                    'class' => 'app\modules\user\Module',
//                    'controllerNamespace' => 'app\modules\user\controllers\backend',
//                    'viewPath' => '@app/modules/user/views/backend',
//                ],
            ]
        ],
        'jobseeker' => [
            'class' => 'jobseeker\Module',
            'controllerNamespace' => 'jobseeker\controllers',
        ],
        'employer' => [
            'class' => 'employer\Module',
            'controllerNamespace' => 'employer\controllers',
            'viewPath' => '@app/employer/views',
        ],
    ],
];
