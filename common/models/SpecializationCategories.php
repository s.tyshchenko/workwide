<?php

namespace common\models;

use Yii;
use common\models\Specializations;
use yii\db\ActiveRecord;

class SpecializationCategories extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '_specialization_categories';
    }

    public function getSpecialization()
    {
        return $this->hasOne(Specializations::className(), ['id' => 'specialization_id']);
    }
    
    public function combineSpecializationCategories($specialization_id)
    {
        $return_array = [];
        foreach (self::find(['specialization_id' => $specialization_id])->all() as $SpecializationCategory)
        {
            $return_array[$SpecializationCategory->name] = Yii::t('jobseeker', $SpecializationCategory->title); 
        }
        
        return $return_array;
    }
}