<?php

namespace jobseeker\controllers;

use Yii;
use yii\web\Controller;
use common\models\Users;
use common\models\Jobseekers;
use common\models\JobseekerProfiles;
use common\models\JobseekerLanguages;
use common\models\JobseekerEducation;
use common\models\JobseekerAdditionalEducation;
use common\models\JobseekerWorkExperience;
use common\models\JobseekerOtherExperience;
use common\models\JobseekerPhotos;
use yii\widgets\ActiveForm;
use yii\web\Response;

class MySettingsController extends Controller
{

    public function actionIndex() 
    {
        Yii::$app->response->redirect(Yii::$app->urlManager->createUrl('jobseeker/my/settings/account'));
        return;
    }
    
    public function actionAccount()
    {
        $this->layout = 'account';
        
        $editFullNameForm   = new \jobseeker\models\EditFullNameForm();
        $editBirthDateForm  = new \jobseeker\models\EditBirthDateForm();
        $editLocationForm   = new \jobseeker\models\EditLocationForm();
        $editEmailForm      = new \jobseeker\models\EditEmailForm();
        $editPhoneForm      = new \jobseeker\models\EditPhoneForm();
        
        $User = Users::findOne(Yii::$app->user->id);
        $Jobseeker = $User->jobseeker;
        
        /*
         * AJAX Request
         */
        
        if(Yii::$app->request->isAjax && isset(Yii::$app->request->post()['action']))
        {
            switch(Yii::$app->request->post('action'))
            {
                case 'PhoneVerificationCodeSend':
                    $phone_verification_code = rand(100000, 999999);
                    
                    Yii::$app->session->set('phone_verification_code', ['code' => $phone_verification_code, 'timestamp' => time()]);
                    
//                    return $this->phoneVerificationCodeSend(Yii::$app->request->post('phone_number'));     
                    return;
                case 'PhoneVerificationCodeValidate':
                    $editPhoneForm->load(Yii::$app->request->post());
                    	
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    
                    return json_encode(ActiveForm::validate($editPhoneForm, ['phone_verification_code']));
            }
        }
        
        /*
         * PJAX Request
         */
        
        if(!empty(Yii::$app->request->get()['_pjax']))
        {
            $renderParams = ['User' => $User, 'Jobseeker' => $Jobseeker];
            
            switch(Yii::$app->request->get()['_pjax']){
                case  '#JobseekerAccountSettings':
                    $renderParams['editFullNameFormModel']  = $editFullNameForm;
                    $renderParams['editBirthDateFormModel']  = $editBirthDateForm;
                    break;
                case '#JobseekerLocation':
                    $renderParams['editLocationFormModel']  = $editLocationForm;
                    break;
                case  '#JobseekerContactInfo':
                    $renderParams['editEmailFormModel']  = $editEmailForm;
                    $renderParams['editPhoneFormModel']  = $editPhoneForm;
                    break;
            }
            
            return $this->renderPartial('account', $renderParams);
        }
        
        /*
         * POST Request
         */
        
        if($editFullNameForm->load(Yii::$app->request->post()))
        {
            return $editFullNameForm->editFullName();
        }
        
        if($editBirthDateForm->load(Yii::$app->request->post()))
        {
            return $editBirthDateForm->editBirthDate();
        }
        
        if($editLocationForm->load(Yii::$app->request->post()))
        {
            return $editLocationForm->editLocation();
        }
        
        if($editEmailForm->load(Yii::$app->request->post()))
        {
            return $editEmailForm->editEmail();
        }
        
        if($editPhoneForm->load(Yii::$app->request->post()))
        {
            return $editPhoneForm->editPhone();
        }
        
        
        return $this->render('account', [
            'editFullNameFormModel' => $editFullNameForm,
            'editBirthDateFormModel' => $editBirthDateForm,
            'editLocationFormModel' => $editLocationForm,
            'editEmailFormModel' => $editEmailForm,
            'editPhoneFormModel' => $editPhoneForm,
            'User' => $User,
            'Jobseeker' => $Jobseeker
        ]);
        
    }
    
    public function actionProfile()
    {
        $this->layout = 'account';
        
        $editVisibilityForm         = new \jobseeker\models\EditVisibilityForm();
        $editPhotoForm              = new \jobseeker\models\EditPhotoForm();
        $addProfileForm             = new \jobseeker\models\AddProfileForm();
        $editProfileForm            = new \jobseeker\models\EditProfileForm();
        $editProfileProfessionalSkillsForm = new \jobseeker\models\EditProfileProfessionalSkillsForm();
        $addLanguageForm            = new \jobseeker\models\AddLanguageForm();
        $editLanguageForm           = new \jobseeker\models\EditLanguageForm();
        $addEducationForm           = new \jobseeker\models\AddEducationForm();
        $editEducationForm          = new \jobseeker\models\EditEducationForm();
        $addAdditionalEducationForm = new \jobseeker\models\AddAdditionalEducationForm();
        $editAdditionalEducationForm = new \jobseeker\models\EditAdditionalEducationForm();
        $addWorkExperienceForm      = new \jobseeker\models\AddWorkExperienceForm();
        $editWorkExperienceForm      = new \jobseeker\models\EditWorkExperienceForm();
        $addOtherExperienceForm     = new \jobseeker\models\AddOtherExperienceForm();
        $editOtherExperienceForm     = new \jobseeker\models\EditOtherExperienceForm();
        
        $User = Users::findOne(Yii::$app->user->id);
        $Jobseeker = $User->jobseeker;
        
        /*
         * AJAX Request
         */
        
        if(Yii::$app->request->isAjax && isset(Yii::$app->request->post()['action']))
        {
            
            if(Yii::$app->request->post('action') == "JobseekerLanguageValidate" && $addLanguageForm->load(Yii::$app->request->post()))
            {
                return json_encode(ActiveForm::validate($addLanguageForm, ['language']));
            }
            
            if(Yii::$app->request->post('action') == "JobseekerProfileRemove")
            {
                return JobseekerProfiles::findOne(['id' => Yii::$app->request->post()['profile_id']])->delete();
            }
            
            if(Yii::$app->request->post('action') == "JobseekerProfileDisable")
            {
                $JobseekerProfile = JobseekerProfiles::findOne(['id' => Yii::$app->request->post()['profile_id']]);
                $JobseekerProfile->visibility = 'none';
                return $JobseekerProfile->save();
            }
            
            if(Yii::$app->request->post('action') == "JobseekerProfileEnable")
            {
                $JobseekerProfile = JobseekerProfiles::findOne(['id' => Yii::$app->request->post()['profile_id']]);
                $JobseekerProfile->visibility = 'all';
                return $JobseekerProfile->save();
            }
            
            if(Yii::$app->request->post('action') == "JobseekerLanguageRemove")
            {
                return JobseekerLanguages::findOne(['jobseeker_id' => $Jobseeker->id, 'language' => Yii::$app->request->post('language')])->delete();
            }
            
            if(Yii::$app->request->post('action') == "JobseekerEducationRemove")
            {
                return JobseekerEducation::findOne(['jobseeker_id' => $Jobseeker->id, 'id' => Yii::$app->request->post('education_id')])->delete();
            }
            
            if(Yii::$app->request->post('action') == "JobseekerAdditionalEducationRemove")
            {
                return JobseekerAdditionalEducation::findOne(['jobseeker_id' => $Jobseeker->id, 'id' => Yii::$app->request->post('additional_education_id')])->delete();
            }
            
            if(Yii::$app->request->post('action') == "JobseekerWorkExperienceRemove")
            {
                return JobseekerWorkExperience::findOne(['jobseeker_id' => $Jobseeker->id, 'id' => Yii::$app->request->post('work_experience_id')])->delete();
            }
            
            if(Yii::$app->request->post('action') == "JobseekerOtherExperienceRemove")
            {
                return JobseekerOtherExperience::findOne(['jobseeker_id' => $Jobseeker->id, 'id' => Yii::$app->request->post('other_experience_id')])->delete();
            }
            
            if(Yii::$app->request->post('action') == "JobseekerPhotoRemove")
            {
                return JobseekerPhotos::findOne(['jobseeker_id' => $Jobseeker->id])->delete();
            }
        }
        
        /*
         * PJAX Request
         */
        
        if(!empty(Yii::$app->request->get()['_pjax']))
        {
            $renderParams = ['User' => $User, 'Jobseeker' => $Jobseeker];
            
            switch(Yii::$app->request->get()['_pjax']){
                case  '#JobseekerProfiles':
                    $renderParams['editProfileFormModel']  = $editProfileForm;
                    $renderParams['editProfileProfessionalSkillsFormModel'] = $editProfileProfessionalSkillsForm;
                    $renderParams['JobseekerProfiles'] = JobseekerProfiles::find()->where(['jobseeker_id' => $Jobseeker->id, 'is_general' => false])->all();
                    break;
                case '#JobseekerGeneralProfile':
                    $renderParams['editProfileFormModel']  = $editProfileForm;
                    $renderParams['JobseekerGeneralProfile'] = JobseekerProfiles::find()->where(['jobseeker_id' => $Jobseeker->id, 'is_general' => true])->one();
                    break;
                case  '#JobseekerLanguages':
                    $renderParams['editLanguageFormModel']  = $editLanguageForm;
                    $renderParams['JobseekerLanguages']     = JobseekerLanguages::find(['jobseeker_id' => $Jobseeker->id])->all();
                    break;
                case  '#JobseekerEducation':
                    $renderParams['editEducationFormModel'] = $editEducationForm;
                    $renderParams['JobseekerEducation'] = JobseekerEducation::find(['jobseeker_id' => $Jobseeker->id])->all();
                    break;
                case  '#JobseekerAdditionalEducation':
                    $renderParams['editAdditionalEducationFormModel'] = $editAdditionalEducationForm;
                    $renderParams['JobseekerAdditionalEducation'] = JobseekerAdditionalEducation::find(['jobseeker_id' => $Jobseeker->id])->all();
                    break;
                case  '#JobseekerWorkExperience':
                    $renderParams['editWorkExperienceFormModel'] = $editWorkExperienceForm;
                    $renderParams['JobseekerWorkExperience'] = JobseekerWorkExperience::find(['jobseeker_id' => $Jobseeker->id])->all();
                    break;
                case  '#JobseekerOtherExperience':
                    $renderParams['editOtherExperienceFormModel'] = $editOtherExperienceForm;
                    $renderParams['JobseekerOtherExperience'] = JobseekerOtherExperience::find(['jobseeker_id' => $Jobseeker->id])->all();
                    break;
                case  '#EditPhotoForm':
                    $renderParams['editPhotoFormModel'] = $editPhotoForm;
                    break;
            }
            return $this->renderPartial('profile', $renderParams);
        }
        
        /*
         * POST Request
         */
       
        if($editVisibilityForm->load(Yii::$app->request->post()))
        {
            return $editVisibilityForm->editVisibility();
        }
                
        if($addProfileForm->load(Yii::$app->request->post()))
        {
            return $addProfileForm->addProfile();
        }
        
        if($editProfileForm->load(Yii::$app->request->post()))
        {
            return $editProfileForm->editProfile();
        }
        
        if($editProfileProfessionalSkillsForm->load(Yii::$app->request->post()))
        {
            return $editProfileProfessionalSkillsForm->editProfileProfessionalSkills();
        }
        
        if($addLanguageForm->load(Yii::$app->request->post()))
        {
            return $addLanguageForm->addLanguage();
        }
        
        if($editLanguageForm->load(Yii::$app->request->post()))
        {
            return $editLanguageForm->editLanguage();
        }
        
        if($addEducationForm->load(Yii::$app->request->post()))
        {
            return $addEducationForm->addEducation();
        }
        
        if($editEducationForm->load(Yii::$app->request->post()))
        {
            return $editEducationForm->editEducation();
        }
        
        if($addAdditionalEducationForm->load(Yii::$app->request->post()))
        {
            return $addAdditionalEducationForm->addAdditionalEducation();
        }
        
        if($editAdditionalEducationForm->load(Yii::$app->request->post()))
        {
            return $editAdditionalEducationForm->editAdditionalEducation();
        }
        
        if($addWorkExperienceForm->load(Yii::$app->request->post()))
        {
            return $addWorkExperienceForm->addWorkExperience();
        }
        
        if($editWorkExperienceForm->load(Yii::$app->request->post()))
        {
            return $editWorkExperienceForm->editWorkExperience();
        }
        
        if($addOtherExperienceForm->load(Yii::$app->request->post()))
        {
            return $addOtherExperienceForm->addOtherExperience();
        }
        
        if($editOtherExperienceForm->load(Yii::$app->request->post()))
        {
            return $editOtherExperienceForm->editOtherExperience();
        }
        
        if($editPhotoForm->load(Yii::$app->request->post()))
        {
            if(!empty(\yii\web\UploadedFile::getInstance($editPhotoForm, 'photo'))){
                $editPhotoForm->photo = \yii\web\UploadedFile::getInstance($editPhotoForm, 'photo');
                return $editPhotoForm->uploadPhoto();
            }
            else
            {
                return $editPhotoForm->editPhoto();
            }
                
        }
        
        
        return $this->render('profile', [
            'User'          => $User,
            'Jobseeker'     => $Jobseeker,
            
            'editVisibilityFormModel'       => $editVisibilityForm,
            
            'editPhotoFormModel'            => $editPhotoForm,
            
            'addProfileFormModel'           => $addProfileForm,
            'editProfileFormModel'          => $editProfileForm,
            'editProfileProfessionalSkillsFormModel'          => $editProfileProfessionalSkillsForm,
            
            'addLanguageFormModel'          => $addLanguageForm,
            'editLanguageFormModel'         => $editLanguageForm,
            
            'addEducationFormModel'         => $addEducationForm,
            'editEducationFormModel'        => $editEducationForm,
            
            'addAdditionalEducationFormModel'   => $addAdditionalEducationForm,
            'editAdditionalEducationFormModel'  => $editAdditionalEducationForm,
            
            'addWorkExperienceFormModel'    => $addWorkExperienceForm,
            'editWorkExperienceFormModel'   => $editWorkExperienceForm,
            
            'addOtherExperienceFormModel'   => $addOtherExperienceForm,
            'editOtherExperienceFormModel'   => $editOtherExperienceForm,
            
            'JobseekerProfiles'             => JobseekerProfiles::find()->where(['jobseeker_id' => $Jobseeker->id, 'is_general' => false])->with(['profileProfessionalSkills', 'profileSpecializationSkills'])->all(),
            'JobseekerGeneralProfile'       => JobseekerProfiles::find()->where(['jobseeker_id' => $Jobseeker->id, 'is_general' => true])->with(['profileProfessionalSkills', 'profileSpecializationSkills'])->one(),
            'JobseekerLanguages'            => JobseekerLanguages::find(['jobseeker_id' => $Jobseeker->id])->with('i18nLanguage')->all(),
            'JobseekerEducation'            => JobseekerEducation::find(['jobseeker_id' => $Jobseeker->id])->all(),
            'JobseekerAdditionalEducation'  => JobseekerAdditionalEducation::find(['jobseeker_id' => $Jobseeker->id])->all(),
            'JobseekerWorkExperience'       => JobseekerWorkExperience::find(['jobseeker_id' => $Jobseeker->id])->all(),
            'JobseekerOtherExperience'      => JobseekerOtherExperience::find(['jobseeker_id' => $Jobseeker->id])->all(),
        ]);
    }
}