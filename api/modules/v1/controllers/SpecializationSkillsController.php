<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use common\models\SpecializationSkills;

class SpecializationSkillsController extends ActiveController
{
    public $modelClass = 'common\models\SpecializationSkills';  
    
    public function actionGetSkills() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $query = new \yii\db\Query;
        $query->select('title AS id, title, description, group_title')
            ->from('_specialization_skills')
            ->where(['specialization_id' => $_GET['specialization_id']])
            ->andWhere(['like', 'title',  (isset($_GET['title_prefix']) ? $_GET['title_prefix'] : '')]);
        $command = $query->createCommand();
        $data = $command->queryAll();
        
        $return_data['results'] = \yii\helpers\ArrayHelper::map($data, 'title', 'title', 'group_title');
        
        return $return_data;
    }
}


