<?php

namespace api\modules\v1\models;

use yii\base\Model;

class i18nLanguagesSearch extends Model 
{
    public $iso639_2;
    public $english_name;
    
    public function rules()
    {
        return [
            ['id', 'string', 'min' => 1],
            ['english_name', 'string', 'min' => 1, 'max' => 255],            
        ];
    }
}
