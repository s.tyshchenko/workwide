<?php

namespace jobseeker\widgets;

class Header extends \yii\bootstrap4\Widget
{
    public $action = null;
    
    public function init(){}

    public function run() {
        switch ($this->action) {
            case 'main':
                return $this->render('header/main');
                
            case 'login':
                return $this->render('header/login');
            
            case 'signup':
                return $this->render('header/signup');

            default:
                return $this->render('header/common');
        }
        
    }
}