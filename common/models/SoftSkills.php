<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class SoftSkills extends ActiveRecord {

    public static function tableName()
    {
        return '_soft_skills';
    }
    
    public function fields()
    {
        return ['title', 'description'];
    }
    
    public function combineSoftSkills() {
        $return_array = [];
        foreach (self::find()->all() as $SoftSkill)
        {
            $return_array[$SoftSkill->title] = $SoftSkill->title; 
        }
                
        return $return_array;
    }
}