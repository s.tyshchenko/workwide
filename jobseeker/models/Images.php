<?php

namespace jobseeker\models;

use yii\db\ActiveRecord;

class Images extends ActiveRecord {

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['date_create', 'date_update'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['date_update'],
                ],
            ],
        ];
    }
    
    public static function tableName()
    {
        return 'images';
    }
    
    public static function getImagePath($title)
    {
        if(strpos($_SERVER['HTTP_ACCEPT'], 'image/webp') !== false) 
        {
            return ((isset($_SERVER['HTTPS'])) ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . '/' . Images::findOne(['title' => $title, 'extension' => 'webp'])->one()->toArray()['path'];
        }
        else
        {
            return ((isset($_SERVER['HTTPS'])) ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . '/' . Images::find()->where(['title' => $title])->andWhere(['!=', 'extension', 'webp'])->one()->toArray()['path'];
        }
    }
}