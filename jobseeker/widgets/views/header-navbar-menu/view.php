<?php

//use yii\helpers\Url;
use yii\bootstrap4\Html;
//use common\widgets\WLanguages;

if(Yii::$app->user->isGuest):

?>


<nav id="header-menu" class="navbar-menu hidden-navbar px-0 py-0">
    <header id="navbar-menu-header">
        <div class="d-block py-1">
            <nav class="navbar navbar-expand float-left px-1 py-2">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link--header mx-3" data-toggle="header-navbar-menu"><i class="far fa-arrow-left fa-lg mr-2"></i></a>
                    </li>
                </ul>
            </nav>
            <nav class="navbar navbar-expand float-left px-1 py-2">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link--header mx-3"><?= Yii::t('jobseeker', 'Menu') ?></a>
                    </li>
                </ul>
            </nav>
        </div>
    </header>
    <nav id="navbar-menu-container">
        <ul class="navbar-nav mt-4">
            <li class="nav-item">
                <?= Html::a('<i class="far fa-user-lock mr-2 text-link-red"></i>' . Yii::t('jobseeker', 'Log in'), Yii::$app->urlManager->createUrl('jobseeker/my/login'), ['class' => 'nav-link bb-1']) ?>
            </li>
            <li class="nav-item">
                <?= Html::a('<i class="far fa-user-plus mr-2 text-link-red"></i>' . Yii::t('jobseeker', 'Sign Up'), Yii::$app->urlManager->createUrl('jobseeker/my/signup'), ['class' => 'nav-link bb-1']) ?>
            </li>
        </ul>
        <ul class="navbar-nav mt-4">
            <li class="nav-item">
                <?= Html::a('<i class="far fa-search mr-2 text-link-red"></i>' .Yii::t('jobseeker', 'Find Job'), Yii::$app->urlManager->createUrl('jobseeker/search'), ['class' => 'nav-link bb-1']) ?>
            </li>
            <li class="nav-item">
                <?= Html::a('<i class="far fa-file-plus mr-2 text-link-red"></i>' .Yii::t('jobseeker', 'Post CV'), Yii::$app->urlManager->createUrl('jobseeker/my'), ['class' => 'nav-link bb-1']) ?>
            </li>
        </ul>
        <ul class="navbar-nav mt-4">
            <li class="nav-item">
                <?= Html::a(Yii::t('jobseeker', 'Employer'). '<i class="far fa-chevron-double-right ml-2 text-link-red"></i>', Yii::$app->urlManager->createUrl('employer'), ['class' => 'nav-link bb-1']) ?>
            </li>
        </ul>
    </nav>
</nav>   

<?php else: ?>

<nav id="header-menu" class="navbar-menu hidden-navbar px-0 py-0">
     <header id="navbar-menu-header">
        <div class="d-block py-1">
            <nav class="navbar navbar-expand float-left px-1 py-2">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link--header mx-3" data-toggle="header-navbar-menu"><i class="far fa-arrow-left fa-lg mr-2"></i></a>
                    </li>
                </ul>
            </nav>
            <nav class="navbar navbar-expand float-left px-1 py-2">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link--header mx-3"><?= Yii::t('jobseeker', 'Menu') ?></a>
                    </li>
                </ul>
            </nav>
        </div>
    </header>
    <nav id="navbar-menu-container">
        <ul class="navbar-nav mt-4">
            <li class="nav-item">
                <?= Html::a('<i class="far fa-search text-link-red"></i> ' . Yii::t('jobseeker', 'Find work') , Yii::$app->urlManager->createUrl('jobseeker/my'), ['class' => 'nav-link bb-1']) ?>
            </li>
            <li class="nav-item">
                <?= Html::a('<i class="far fa-folders text-link-red"></i> ' . Yii::t('jobseeker', 'My applications') , Yii::$app->urlManager->createUrl('jobseeker/my/applications'), ['class' => 'nav-link bb-1']) ?>
            </li>
        </ul>
        <ul class="navbar-nav mt-4">
            <li class="nav-item">
                <?= Html::a('<i class="far fa-user text-link-red"></i> ' . Yii::t('jobseeker', 'Profile') , Yii::$app->urlManager->createUrl('jobseeker/my/profile'), ['class' => 'nav-link bb-1']) ?>
            </li>
            <li class="nav-item">
                <?= Html::a('<i class="far fa-cog text-link-red"></i> ' . Yii::t('jobseeker', 'Settings'), Yii::$app->urlManager->createUrl('jobseeker/my/settings'), ['class' => 'nav-link bb-1']) ?>
            </li>
            <li class="nav-item">
                <?= Html::a('<i class="far fa-sign-out-alt text-link-red"></i> ' . Yii::t('jobseeker', 'Log out'), Yii::$app->urlManager->createUrl('jobseeker/my/logout'), ['class' => 'nav-link bb-1']) ?>
            </li>
        </ul>
        <ul class="navbar-nav mt-4">
            <li class="nav-item">
                <?= Html::a(Yii::t('jobseeker', 'Employer') . ' <i class="far fa-chevron-double-right text-link-red"></i>', Yii::$app->urlManager->createUrl('employer'), ['class' => 'nav-link bb-1']) ?>
            </li>
        </ul>
    </nav>
</nav>

<?php endif; ?>
