
/* To initialize BS3 tooltips set this below */
$(function () { 
    $("[data-toggle='tooltip']").tooltip(); 
});;

/* To initialize BS3 popovers set this below */
$(function () { 
    $("[data-toggle='popover']").popover(); 
});

$( "[data-toggle='header-navbar-menu']" ).click(function() {
    $("#header-menu").toggleClass('hidden-navbar');
    $("html").toggleClass('no-scrolling');
});