<?php

namespace jobseeker\components\validators;

use Yii;
use yii\validators\Validator;
use yii\helpers\Json;
 
class PhoneVerificationCodeValidator extends Validator
{
    public function init() {
        if (!$this->message) {
            $this->message = Yii::t('jobseeker', 'Invalid code');
        }
        parent::init();
    }
    
//    public function validateAttribute($model, $attribute)
//    {   
//        $value = $model->$attribute;
//        if ($value != Yii::$app->session['phone_verification_code']['code']) {
//            $model->addError($attribute, $this->message);
//        }
//    }
    
    public function validateValue($value)
    {
        return ($value != Yii::$app->session['phone_verification_code']['code']) ? [Yii::t('jobseeker', 'Invalid code'), []] : null;
    }
    
    public function clientValidateAttribute($model, $attribute, $view)
    {
        $options = Json::htmlEncode([
            'message' => $this->message
        ]);
        
        return <<<JS
            var options = $options;
            var formdata = new FormData(document.querySelector('.form-phone-validation'));
            formdata.append("action", "PhoneVerificationCodeValidate");
            $.ajax({
                async: false,
                url: '',
                data: formdata,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(message){
                    if(!$.isEmptyObject($.parseJSON(message)))
                         messages.push(options.message);
                }
            });
        JS;
    }
    
}