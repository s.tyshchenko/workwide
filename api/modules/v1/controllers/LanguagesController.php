<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\data\ActiveDataFilter;
use common\models\i18nLanguages;
//use api\modules\v1\models\i18nLanguagesSearch;

class LanguagesController extends ActiveController
{
    public $modelClass = 'common\models\i18nLanguages';   
    
    public function actionSearch()
    {

        var_dump($filterCondition);

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $filter = new ActiveDataFilter([
            'searchModel' => 'api\modules\v1\models\i18nLanguagesSearch'
        ]);

        $filterCondition = null;

        if ($filter->load(Yii::$app->request->get())) { 
            $filterCondition = $filter->build();
            if ($filterCondition === false) {
                return $filter;
            }
        }

        $query = i18nLanguages::find();
        if ($filterCondition !== null) {
            $query->andWhere($filterCondition);
        }

        return ['results' => new ActiveDataProvider([
            'query' => $query,
        ])];
    }
}


