<?php

use jobseeker\widgets\MySettingsNav;
use yii\widgets\ActiveForm;
use borales\extensions\phoneInput\PhoneInput;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\Html;
use yii\widgets\Pjax;

use jobseeker\assets\AppAsset;
use common\assets\CommonAsset;


$this->registerJsFile(
    '@web/js/pjax-form-loading.js',
    [
        'position' => \yii\web\View::POS_HEAD,
        'depends' => [
            AppAsset::class,
            CommonAsset::class
        ]
    ]
);

$this->registerJsFile(
    '@web/js/select2.js',
    [
        'position' => \yii\web\View::POS_HEAD,
        'depends' => [
            AppAsset::class,
            CommonAsset::class
        ]
    ]
);


$this->registerJs("
    phone_input     = $('input[data-id=\"phone-input\"]');
    phone_verified  = $('.phone-verified-sign');
    code_input      = $('input[data-id=\"verification-code-input\"]');
    button          = $('button[name=\"phone-submit-button\"]');
    
    button.on('click', function(){
        if(button.attr('data-disabled') == 'false')
        {
            button.append(' <i class=\"fad fa-spinner-third fa-spin\"></i>');
            
            $.ajax({
                url: '',
                data: {
                    action:'PhoneVerificationCodeSend',
                    phone_number:phone_input.val()
                },
                type: 'POST',
                success: function(res) {
                    button.html(60);
                    button.attr('data-disabled', 'true');
                    var _seconds = button.text(), int;
                    int = setInterval(function() {
                      if (_seconds > 0) 
                      {
                        _seconds--;
                        button.text(_seconds);
                      } 
                      else
                      {
                        clearInterval(int); 
                        button.text('". Yii::t('jobseeker', 'Send again') ."');
                        button.attr('data-disabled', 'false');
                      }
                    }, 1000);
                }
            });
        }
    });
    
    $('#edit-phone-form').on('afterValidateAttribute', function (event, attribute, messages)
    {
        console.log(attribute);
        console.log(messages);
        if(attribute['name'] == 'phone')
        {
            if($.isEmptyObject(messages))
            {
                button.attr('data-disabled', 'false');
                code_input.removeAttr('readonly');
            }
            else
            {
                button.attr('data-disabled', 'true');
                code_input.prop('readonly', true);
            }
        }
        
        if(attribute['name'] == 'phone_verification_code')
        {
            if($.isEmptyObject(messages))
            {
                button.remove();
                code_input.attr('class', 'form-control col-6 float-left text-center');
                code_input.attr('aria-validated', 'true');
                code_input.prop('readonly', true);
                phone_verified.removeClass('d-none');
            }
        }
    });",
    \yii\web\View::POS_LOAD
);

$countryChange = <<<JS
function(e){
    if(e.params._type == 'select')
    {
        $('#edit-location-form-city-select').prop('disabled', false);
    }
    else
    {
        $('#edit-location-form-city-select').prop('disabled', true);
    }
}
JS;

?>

<div class="container my-5 py-0 px-0">
    
    <div class="row mx-0">
        
        <?= MySettingsNav::widget() ?>
        
        <div class="col-12 col-md-9 b-1 bg-white br-2 py-4 px-4">
            
            <?php
                
                Pjax::begin([ 'id' => 'JobseekerAccountSettings', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n4 pt-4 mb-n3 pb-3']]); 
                
            ?>
            
            <div class="row mx-0">
                <div class="container mb-2">
                    <h3 class="font-weight-normal"><?= Yii::t('jobseeker', 'Account settings') ?></h3>
                </div>
                <div class="container">
                    <div class="col text-main font-weight-bold pl-0 py-1">
                        <?= Yii::t('jobseeker', 'Full name') ?>:
                        <span class="profile-control-buttons-container">
                            <div class="profile-control-button-block" data-toggle="modal" data-target="#edit-full-name-modal">
                                <button class="profile-control-text-button ml-2 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Edit full name') ?>">
                                    <i class="fas fa-pencil"></i>
                                    &nbsp;<?= Yii::t('jobseeker', 'Edit') ?>
                                </button>   
                            </div> 
                        </span>
                    </div>
                    <div class="col text-secondary pl-0 py-1">
                        <?= $Jobseeker->full_name ?>
                    </div>
                </div>
                <div class="container">
                    <div class="col text-main font-weight-bold pl-0 py-1">
                        <?= Yii::t('jobseeker', 'Date of birth') ?>
                        <span class="profile-control-buttons-container">
                            <div class="profile-control-button-block" data-toggle="modal" data-target="#edit-birth-date-modal">
                                <button class="profile-control-text-button ml-2 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Edit birth date') ?>">
                                    <i class="fas fa-pencil"></i>
                                    &nbsp;<?= Yii::t('jobseeker', 'Edit') ?>
                                </button>   
                            </div> 
                        </span>
                    </div>
                    <div class="col text-secondary pl-0 py-1">
                        <?= date('d.m.Y', strtotime($Jobseeker->birth_date)) ?>
                    </div>
                </div>
            </div>
            
            <?php
            
                Pjax::end(); 
                
            ?>
            
            <hr class="bt-1 mx-n4">    
                
            <?php 
            
                Pjax::begin([ 'id' => 'JobseekerContactInfo', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n3 pb-3']]); 
                
            ?>
            
            <div class="row mx-0">
                <div class="container mb-2">
                    <h5 class="font-weight-normal"><?= Yii::t('jobseeker', 'Contact info') ?></h5>
                </div>
                <div class="container">
                    <div class="col text-main font-weight-bold pl-0 py-1">
                        <?= Yii::t('jobseeker', 'E-mail') ?>:
                        <span class="profile-control-buttons-container">
                            <div class="profile-control-button-block" data-toggle="modal" data-target="#edit-email-modal">
                                <button class="profile-control-text-button ml-2 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Edit email') ?>">
                                    <i class="fas fa-pencil"></i>
                                    &nbsp;<?= Yii::t('jobseeker', 'Edit') ?>
                                </button>   
                            </div> 
                        </span>
                    </div>
                    <div class="col text-secondary pl-0 py-1">
                        <?= $User->email ?>
                    </div>
                </div>
                <div class="container">
                    <div class="col text-main font-weight-bold pl-0 py-1">
                        <?= Yii::t('jobseeker', 'Phone') ?>:
                        <span class="profile-control-buttons-container">
                            <div class="profile-control-button-block" data-toggle="modal" data-target="#edit-phone-modal">
                                <button class="profile-control-text-button ml-2 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Edit phone') ?>">
                                    <i class="fas fa-pencil"></i>
                                    &nbsp;<?= Yii::t('jobseeker', 'Edit') ?>
                                </button>   
                            </div> 
                        </span>
                    </div>
                    <div class="col text-secondary pl-0 py-1">
                        <?= $User->phone ?>
                    </div>
                </div>
            </div>
            
            <?php
            
                Pjax::end(); 
                
            ?>
            
            <hr class="bt-1 mx-n4">
            
            <?php 
            
                Pjax::begin([ 'id' => 'JobseekerLocation', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n3 pb-3']]); 
                
            ?>
            
            <div class="row mx-0">
                <div class="container mb-2">
                    <h5 class="font-weight-normal">
                        <?= Yii::t('jobseeker', 'Location') ?>
                    </h5>
                </div>
                <div class="container">
                    <div class="col text-main font-weight-bold pl-0 py-1">
                        <?= Yii::t('jobseeker', 'Location') ?>:
                        <span class="profile-control-buttons-container">
                            <div class="profile-control-button-block" data-toggle="modal" data-target="#edit-location-modal">
                                <button class="profile-control-text-button ml-2 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Edit location') ?>">
                                    <i class="fas fa-pencil"></i>
                                    &nbsp;<?= Yii::t('jobseeker', 'Edit') ?>
                                </button>   
                            </div> 
                        </span>
                    </div>
                    <div class="col text-secondary pl-0 py-1">
                        <?= $Jobseeker->city . ", " . $Jobseeker->country ?>
                    </div>
                </div>
                <div class="container">
                    <div class="col text-main font-weight-bold pl-0 py-1">
                        <?= Yii::t('jobseeker', 'Timezone') ?>:
                    </div>
                    <div class="col text-secondary pl-0 py-1">
                        <?= $Jobseeker->timezone ?>
                    </div>
                </div>
            </div>
            
            <?php
            
                Pjax::end(); 
                
            ?>
            
            <hr class="bt-1 mx-n4">
            
            <?php 
            
                Pjax::begin([ 'id' => 'JobseekerConnectedServices', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n3 pb-3']]); 
                
            ?>
            
            <div class="row mx-0">
                <div class="container mb-2">
                    <h5 class="font-weight-normal"><?= Yii::t('jobseeker', 'Connected services') ?></h5>
                </div>
                <div class="container">
                    <div class="col-auto text-main pl-0 py-0 float-left">
                        <span class="fa-stack float-left fa-2x">
                            <i class="fas fa-square fa-stack-2x"></i>
                            <i class="fab fa-google fa-stack-1x fa-inverse"></i>
                        </span>
                    </div>
                    <div class="col-7 text-main px-0 py-1 float-left">
                        <h5 class="w-100"><?= Yii::t('jobseeker', 'Sign in with Google') ?></h5>
                        <span class="text-secondary"><?= Yii::t('jobseeker', 'You are not signed in through Google.') ?></span>
                    </div>
                    <div class="col-auto text-main pl-0 py-1 float-left mt-3">
                        <button class="btn btn-primary">
                            <i class="fab fa-google"></i> 
                            <?= Yii::t('jobseeker', 'Sign in with Google') ?>
                        </button>
                    </div>
                </div>
                <div class="container mt-3">
                    <div class="col-auto text-main pl-0 py-0 float-left">
                        <span class="fa-stack float-left fa-2x">
                            <i class="fas fa-square fa-stack-2x"></i>
                            <i class="fab fa-apple fa-stack-1x fa-inverse"></i>
                        </span>
                    </div>
                    <div class="col-7 text-main px-0 py-1 float-left">
                        <h5 class="w-100"><?= Yii::t('jobseeker', 'Sign in with Apple') ?></h5>
                        <span class="text-secondary"><?= Yii::t('jobseeker', 'You are not signed in through Apple.') ?></span>
                    </div>
                    <div class="col-auto text-main pl-0 py-1 float-left mt-3">
                        <button class="btn btn-primary">
                            <i class="fab fa-apple"></i> 
                            <?= Yii::t('jobseeker', 'Sign in with Apple') ?>
                        </button>
                    </div>
                </div>
            </div>
            
            <?php
            
                Pjax::end(); 
                
            ?>
            
        </div>
    </div>
</div>

<?php

if(!empty($editFullNameFormModel)):

?>

<div class="modal my-settings-modal fade" id="edit-full-name-modal" tabindex="-1" aria-labelledby="edit-full-name-modal-title" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content br-2 b-none">
            <?php 
                $editFullNameForm = ActiveForm::begin([
                    'id' => 'edit-full-name-form',
                    'options' => [
                        'onsubmit' => 'return submitFormAjax(this, "#JobseekerAccountSettings");',
                        'style' => 'height: 100%; overflow-y: auto;'
                    ]
                ]);
            ?>
            <div class="modal-header">
                <h5 class="modal-title" id="edit-full-name-modal-title"><?= Yii::t('jobseeker', 'Edit full name') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            
                <?= $editFullNameForm->field($editFullNameFormModel, 'full_name', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'value' => $Jobseeker->full_name] ])->label(Yii::t('jobseeker', 'Your full name'), ['class' => 'control-label font-weight-normal text-main-2']) ?>
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Save') ?></button>
            </div>
            <?php 
                ActiveForm::end();
            ?>
        </div>
    </div>
</div>

<?php

endif;

if(!empty($editBirthDateFormModel)):
    
?>

<div class="modal my-settings-modal fade" id="edit-birth-date-modal" tabindex="-1" aria-labelledby="edit-birth-date-modal-title" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content br-2 b-none">
            <?php 
                $editBirthDateForm = ActiveForm::begin([
                    'id' => 'edit-birth-date-form',
                    'options' => [
                        'onsubmit' => 'return submitFormAjax(this, "#JobseekerAccountSettings");',
                        'style' => 'height: 100%; overflow-y: auto;'
                    ]
                ]);
            ?>
            <div class="modal-header">
                <h5 class="modal-title" id="edit-birth-date-modal-title"><?= Yii::t('jobseeker', 'Edit birth date') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                        <span aria-hidden="true">×</span>
                    </button>
            </div>
            <div class="modal-body">
           
                <?= $editBirthDateForm->field($editBirthDateFormModel, 'birth_date', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'value' => date('d.m.Y', strtotime($Jobseeker->birth_date))] ])->label(Yii::t('jobseeker', 'Your birth date'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(DatePicker::className(), [
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'value' => date('d.m.Y', strtotime($Jobseeker->birth_date)),
                    'pluginOptions' => [
                        'autoclose' =>true,
                        'format' => 'dd.mm.yyyy'
                    ]
                ]) ?>    
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Save') ?></button>
            </div>
            <?php 
                ActiveForm::end();
            ?>
        </div>
    </div>
</div>

<?php

endif;

if(!empty($editLocationFormModel)):
    
?>

<div class="modal my-settings-modal fade" id="edit-location-modal" tabindex="-1" aria-labelledby="edit-location-modal-title" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content br-2 b-none">
            <?php 
                $editLocationForm = ActiveForm::begin([
                    'id' => 'edit-location-form',
                    'options' => [
                        'onsubmit' => 'return submitFormAjax(this, "#JobseekerLocation");',
                        'style' => 'height: 100%; overflow-y: auto;'
                    ]
                ]);
            ?>
            <div class="modal-header">
                <h5 class="modal-title" id="edit-location-modal-title"><?= Yii::t('jobseeker', 'Edit location') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                        <span aria-hidden="true">×</span>
                    </button>
            </div>
            <div class="modal-body">
           
                <?= $editLocationForm->field($editLocationFormModel, 'country', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-location-form-country-select'] ])->label(Yii::t('jobseeker', 'Country'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                    [
                        'theme' => 'my-settings',
                        'options' => ['placeholder' => Yii::t('jobseeker', 'Country')],
                        'pluginLoading' => false,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 1,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return '" . Yii::t('jobseeker', 'Waiting for results...') . "'; }"),
                            ],
                            'ajax' => [
                                'url' => 'http://34.123.108.88/api/v1/geodb/countries',
                                'dataType' => 'json',
                                'delay' => 250,
                                'data' => new JsExpression('function(params) { return {namePrefix:params.term}; }'),
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ],
                        'pluginEvents' => [
                            'select2:select' => new JsExpression($countryChange),
                            'select2:clear' => new JsExpression($countryChange)
                        ]
                    ])
                ?>  

                <?= $editLocationForm->field($editLocationFormModel, 'city', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-location-form-city-select'] ])->label(Yii::t('jobseeker', 'City'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                    [
                        'theme' => 'my-settings',
                        'options' => ['placeholder' => Yii::t('jobseeker', 'City')],
                        'disabled' => true,
                        'pluginLoading' => false,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 1,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return '" . Yii::t('jobseeker', 'Waiting for results...') . "'; }"),
                            ],
                            'ajax' => [
                                'url' => 'http://34.123.108.88/api/v1/geodb/cities',
                                'dataType' => 'json',
                                'delay' => 250,
                                'data' => new JsExpression('function(params) { return {namePrefix:params.term,types:"CITY",countryIds:$("#edit-location-form-country-select").val()}; }'),
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ]
                    ])
                ?>  
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Save') ?></button>
            </div>
            <?php 
                ActiveForm::end();
            ?>
        </div>
    </div>
</div>

<?php

endif;

if(!empty($editEmailFormModel)):
    
?>

<div class="modal my-settings-modal fade" id="edit-email-modal" tabindex="-1" aria-labelledby="edit-email-modal-title" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content br-2 b-none">
            <?php 
                $editEmailForm = ActiveForm::begin([
                    'id' => 'edit-email-form',
                    'options' => [
                        'onsubmit' => 'return submitFormAjax(this, "#JobseekerContactInfo");',
                        'style' => 'height: 100%; overflow-y: auto;'
                    ]
                ]);
            ?>
            <div class="modal-header">
                <h5 class="modal-title" id="edit-full-name-modal-title"><?= Yii::t('jobseeker', 'Edit email') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            
                <?= $editEmailForm->field($editEmailFormModel, 'email', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control'] ])->label(Yii::t('jobseeker', 'Your new email'), ['class' => 'control-label font-weight-normal text-main-2']) ?>
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Save') ?></button>
            </div>
            <?php 
                ActiveForm::end();
            ?>
        </div>
    </div>
</div>

<?php

endif;

if(!empty($editPhoneFormModel)):
    
?>

<div class="modal my-settings-modal fade" id="edit-phone-modal" tabindex="-1" aria-labelledby="edit-phone-modal-title" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content br-2 b-none">
            <?php 
                $editPhoneForm = ActiveForm::begin([
                    'id' => 'edit-phone-form',
                    'options' => [
                        'class' => 'form-phone-validation',
                        'onsubmit' => 'return submitFormAjax(this, "#JobseekerContactInfo");',
                        'style' => 'height: 100%; overflow-y: auto;'
                    ]
                ]);
            ?>
            <div class="modal-header">
                <h5 class="modal-title" id="edit-full-name-modal-title"><?= Yii::t('jobseeker', 'Edit phone') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            
                <?= $editPhoneForm->field($editPhoneFormModel, 'phone', ['addAriaAttributes' => true, 'validateOnType' => true, 'template' => "{label}\n{input}\n{error}", 'inputOptions' => ['class' => 'form-control', 'data-id' => 'phone-input'] ])->label(Yii::t('jobseeker', 'Phone'), ['class' => 'form-control-label h5 text-main-2'])->widget(PhoneInput::className(), [
                            'jsOptions' => [
                                'nationalMode' => false,
                                'onlyCountries' => ['us', 'ru', 'ua']
                            ],
                    ]); ?>
                <div class="form-group field-signupform-phone_verification_code float-left required">
                    <label class="form-control-label h5 float-left px-0 col-12 text-main-2"><?= Yii::t('jobseeker', 'Phone verification') ?></label>
                    <?= Html::button(Yii::t('jobseeker', 'Send'), ['class' => 'col-5 float-left', 'name' => 'phone-submit-button', 'data-disabled' => 'true']) ?>
                    <?= $editPhoneForm->field($editPhoneFormModel, 'phone_verification_code', ['addAriaAttributes' => true, 'enableClientValidation' => true, 'validateOnType' => true, 'template' => "{input}\n{error}", 'selectors' => ['error' => '.help-block.col-6.float-right'], 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control col-6 text-center float-right', 'readonly' => '', 'data-id' => 'verification-code-input', 'placeholder' => Yii::t('jobseeker', 'Code')] ])->error(['class' => 'help-block col-6 float-right']); ?>
                    <div class="phone-verified-sign col-6 float-right d-none"><?= Yii::t('jobseeker', 'Verified') ?> <i class="fad fa-check-double"></i></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Save') ?></button>
            </div>
            <?php 
                ActiveForm::end();
            ?>
        </div>
    </div>
</div>

<?php

endif;
    
?>