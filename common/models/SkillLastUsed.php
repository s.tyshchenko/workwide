<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class SkillLastUsed extends ActiveRecord {

    public static function tableName()
    {
        return '_skill_last_used';
    }
    
    public function fields()
    {
        return ['title', 'description'];
    }
    
    public function combineSkillLastUsed() {
        $return_array = [];
        foreach (self::find()->orderBy(['index_number' => SORT_ASC])->all() as $SkillLastUsed)
        {
            $return_array[$SkillLastUsed->title] = Yii::t('jobseeker', $SkillLastUsed->title); 
        }
        
        return $return_array;
    }
}