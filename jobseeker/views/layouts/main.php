<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use jobseeker\assets\AppAsset;
use common\widgets\Favicon;
use common\widgets\Alert;
use jobseeker\widgets\Header;

AppAsset::register($this);

$this->registerJsFile(
    '@web/js/less.js',
    [
        'data-env' => 'development',
        'position' => \yii\web\View::POS_HEAD,
        'depends' => 'yii\bootstrap4\BootstrapPluginAsset'
    ]
);


\yii\web\View::registerJsFile(
    '@web/js/less.js',
    [
        'position' => \yii\web\View::POS_HEAD,
        'depends' => 'yii\bootstrap4\BootstrapPluginAsset'
    ]
);

\yii\web\View::registerJsFile(
    '@web/js/placeholder-animation.js',
    [
        'position' => \yii\web\View::POS_END,
        'depends' => 'yii\bootstrap4\BootstrapPluginAsset'
    ]
);

$this->registerJs(
    "animated_placeholder({
	el: document.getElementsByClassName('placeholder-animated')[0],
	sentences: ['Web Designer', 'Accountant', 'Sales', 'Surgeon', 'Banking', 'Engineer', 'IT Specialist', 'Finances', 'Chef', 'HR Manager', 'Economist'],
        options: {
            letterDelay: 80,
            loop: true,
            startOnFocus: false
        }
    })",
    \yii\web\View::POS_LOAD
);


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
    <?= Favicon::widget() ?>
   
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body id="<?= 'p-' . Yii::$app->controller->id . '-' . Yii::$app->controller->action->id ?>">
<?php $this->beginBody() ?>

        <?= Alert::widget() ?>
        <?= $content ?>

<footer class="footer">
    <div class="container">

    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
