<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\widgets\WLanguages;
use common\widgets\WImages;
use common\models\Users;
use jobseeker\widgets\Header;
use jobseeker\widgets\HeaderNavbarMenu;

$this->title = Yii::t('jobseeker', 'Find a Work worldwide');

?>

<?= HeaderNavbarMenu::widget() ?>

<header id="header" style="background: no-repeat center/auto 90% url(<?= WImages::widget(['title' => 'main-header-background']) ?>), -webkit-linear-gradient(-90deg, #1c1c1e 30%, #48484a 100%);">
    <?= Header::widget() ?>  


    <section id="main-search" class="container pt-3">
        <div class="container" >

            <?php 
                $searchForm = ActiveForm::begin([
                    'id' => 'main-search-form',
                    'options' => ['class' => 'row justify-content-center'],
                ]);
            ?>

            <div class="col-12 col-sm-12 col-md-4 col-lg-4 px-0" data-toggle="collapse" data-target="#keywords-suggestions-expand" aria-expanded="false">
                <?= $searchForm->field($searchFormModel, 'keywords', ['template' => "{input}\n{hint}", 'inputOptions' => ['class' => 'form-control placeholder-animated', 'placeholder' => Yii::t('jobseeker', 'Search Keywords')] ])->hint('Search Keywords') ?>
                <div class="form-group-suggestions col-12 collapse d-md-none" data-parent="#main-search-form" id="keywords-suggestions-expand">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</div>
            </div>
            <div class="col-6 col-6 col-md-4 col-lg-4 px-0" data-toggle="collapse" data-target="#specialism-suggestions-expand" aria-expanded="false">
                <?= $searchForm->field($searchFormModel, 'specialism', ['template' => "{input}\n{hint}", 'inputOptions' => ['class' => 'form-control' ]])->dropDownList([['WebDesign', 'IT'],['prompt' => Yii::t('jobseeker', 'All specialisms')]])->hint('Filter by specialism e.g. IT') ?>

            </div>
            <div class="col-6 col-6 col-md-4 col-lg-4 px-0" data-toggle="collapse" data-target="#country-suggestions-expand" aria-expanded="false">
                <?= $searchForm->field($searchFormModel, 'country', ['options' => ['class' => 'form-group border-0 col-12 col-md-8'], 'template' => "{input}\n{hint}", 'inputOptions' => ['class' => 'form-control'] ])->dropDownList([['Germany', 'Poland'],['prompt' => Yii::t('jobseeker', 'City, region or country')]])->hint('Filter by location') ?>
                <div class="form-group d-none d-md-block col-md-4" style="padding: 5px 5px;">
                    <?= Html::submitButton( '<i class="far fa-search fa-flip-horizontal"></i>') ?>
                </div>
            </div>
            <div class="col-12 px-0" id="form-group-suggestions-container">
                <div class="form-group-suggestions col-12 collapse d-md-none" data-parent="#main-search-form" id="country-suggestions-expand">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</div>
                <div class="form-group-suggestions col-12 collapse d-md-none" data-parent="#main-search-form" id="specialism-suggestions-expand">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</div>
            </div>
            <div class="col-12 d-md-none px-0">
                <div class="form-group col-12" style="padding: 0;">
                    <?= Html::submitButton( '<i class="far fa-search fa-flip-horizontal"></i>', ['style' => 'min-height:35px;']) ?>
                </div>
            </div>

            <?php 
                ActiveForm::end();
            ?>
            <div class="row">
                <div id="advanced-search-button" class="col text-right px-2 mt-1">
                    <?= Html::a('<i class="fal fa-sliders-h-square"></i> ' . Yii::t('jobseeker', 'Advanced Search'), Url::to('jobseeker/search', true), ['class' => 'text-white']) ?>
                </div>
            </div>
        </div>

        <div class="container my-1" >
            <div class="row d-none d-sm-none d-md-block" id="search-form-suggestions-container">
                <div class="form-group-suggestions col-12 collapse" data-parent="#search-form-suggestions-container" id="keywords-suggestions-expand">
            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
          </div>
                <div class="form-group-suggestions col-12 collapse" data-parent="#search-form-suggestions-container" id="specialism-suggestions-expand">
            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
          </div>
                <div class="form-group-suggestions col-12 collapse" data-parent="#search-form-suggestions-container" id="country-suggestions-expand">
            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
          </div>
            </div>
        </div>
        
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-5 px-0 mt-5">
                    <h1 class="font-weight-light text-white font-weight-bold" style="text-shadow: rgba(0, 0, 0, 0.3) 0px 0px 35px;"><?= Yii::t('jobseeker', 'Find your  {work}  worldwide', ['work' => '<br><b class="text-red" style="background: white; border-radius:2px; text-shadow: none;">&nbsp;'.Yii::t('jobseeker', 'Work').'&nbsp;</b>']) ?></h1>
                </div>
            </div>
        </div>
    </section>
</header>    


<main id="main" class="wrap">
    
    <section id="main-post-cv" class="py-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <i class="fad fa-file-user fa-5x float-left mr-5 text-secondary"></i>
                    <h2 style="color: #222;"><?= Yii::t('jobseeker', 'Post your CV') ?></h2>
                    <p class="text-gray" style="color: #5f6368;"><?= Yii::t('jobseeker', 'Employers will be able to find you and offer you a great job.') ?></p>
                </div>

                <div class="col-md-3">
                    <a class="btn btn-link" href="#">

                        <?= '<i class="fal fa-plus fa-lg "> </i>&nbsp;' . Yii::t('jobseeker', 'Create CV') ?>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section id="main-categories" class="py-5">
        <div class="container tabs-container">
            <div class="row justify-content-center">
                <div class="col py-4 tab">
                    <i class="fad fa-th fa-3x"></i><br>
                    <?= Html::tag('p', Yii::t('jobseeker', 'Search by specialism')) ?>
                </div>
                <div class="col py-4 tab">
                    <i class="fad fa-compass fa-3x"></i><br>
                    <?= Html::tag('p', Yii::t('jobseeker', 'Search by region')) ?>
                </div>
                <div class="col py-4 tab">
                    <i class="fad fa-star fa-3x"></i><br>
                    <?= Html::tag('p', Yii::t('jobseeker', 'Popular jobs')) ?>
                </div>
                <div class="col py-4 tab">
                    <i class="fad fa-user-graduate fa-3x"></i><br>
                    <?= Html::tag('p', Yii::t('jobseeker', 'Student-friendly jobs')) ?>
                </div>
                <div class="col py-4 tab">
                    <i class="fad fa-laptop-house fa-3x"></i><br>
                    <?= Html::tag('p', Yii::t('jobseeker', 'Remote jobs')) ?>
                </div>
            </div>
        </div>
         <div class="container tab-content-container">
            <div class="row" style="height:200px;">
    <!--            <div class="col-xs-12"></div>-->
            </div>
        </div>
    </section>
</main>