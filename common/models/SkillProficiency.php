<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class SkillProficiency extends ActiveRecord {

    public static function tableName()
    {
        return '_skill_proficiency';
    }
    
    public function fields()
    {
        return ['title', 'description'];
    }
    
    public function combineSkillProficiency() {
        $return_array = [];
        foreach (self::find()->orderBy(['index_number' => SORT_ASC])->all() as $SkillProficiency)
        {
            $return_array[$SkillProficiency->title] = Yii::t('jobseeker', $SkillProficiency->title); 
        }
        
        return $return_array;
    }
}