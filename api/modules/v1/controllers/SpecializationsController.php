<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use common\models\Specializations;
use common\models\SpecializationCategories;

class SpecializationsController extends ActiveController
{
    public $modelClass = 'common\models\Specializations';  
    
    public function actionCategories() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $query = new \yii\db\Query;
        $query->select('title AS id, title')
            ->from('_specialization_categories')
            ->where(['specialization_id' => $_GET['specialization_id']])    
            ->andWhere(['like', 'title',  (isset($_GET['title_prefix']) ? $_GET['title_prefix'] : '')]);
        $command = $query->createCommand();
        $data = $command->queryAll();
        
        $return_data['results'] = array_values($data);
        
        return $return_data;
    }
}


