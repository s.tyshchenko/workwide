<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use jobseeker\assets\AppAsset;
use common\widgets\WLanguages;
use common\widgets\Favicon;
use common\widgets\Alert;
use jobseeker\widgets\Header;
use jobseeker\widgets\HeaderNavbarMenu;

AppAsset::register($this);

$this->registerJsFile(
    '@web/js/less.js',
    [
        'data-env' => 'development',
        'position' => \yii\web\View::POS_HEAD,
        'depends' => 'yii\bootstrap4\BootstrapPluginAsset'
    ]
);

\yii\web\View::registerJs(
    "
    phone_input     = $('input[data-id=\"phone-input\"]');
    phone_verified  = $('.phone-verified-sign');
    code_input      = $('input[data-id=\"verification-code-input\"]');
    button          = $('button[name=\"phone-submit-button\"]');
    
    button.on('click', function(){
        if(button.attr('data-disabled') == 'false')
        {
            button.append(' <i class=\"fad fa-spinner-third fa-spin\"></i>');
            
            $.ajax({
                url: '',
                data: {
                    action:'send',
                    phone_number:phone_input.val()
                },
                type: 'POST',
                success: function(res) {
                    button.html(60);
                    button.attr('data-disabled', 'true');
                    var _seconds = button.text(), int;
                    int = setInterval(function() {
                      if (_seconds > 0) 
                      {
                        _seconds--;
                        button.text(_seconds);
                      } 
                      else
                      {
                        clearInterval(int); 
                        button.text('". Yii::t('jobseeker', 'Send again') ."');
                        button.attr('data-disabled', 'false');
                      }
                    }, 1000);
                }
            });
        }
    });
    $('#signup_block').on('afterValidateAttribute', function (event, attribute, messages)
    {
        console.log(attribute);
        console.log(messages);
        if(attribute['name'] == 'phone')
        {
            if($.isEmptyObject(messages))
            {
                button.attr('data-disabled', 'false');
                code_input.removeAttr('readonly');
            }
            else
            {
                button.attr('data-disabled', 'true');
                code_input.prop('readonly', true);
            }
        }
        
        if(attribute['name'] == 'phone_verification_code')
        {
            if($.isEmptyObject(messages))
            {
                button.remove();
                code_input.attr('class', 'form-control col-6 float-left text-center');
                code_input.attr('aria-validated', 'true');
                code_input.prop('readonly', true);
                phone_verified.removeClass('d-none');
            }
        }
    });
    ",
    \yii\web\View::POS_LOAD
);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <?= Favicon::widget() ?>

    
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body id="<?= 'p-' . Yii::$app->controller->id . '-' . Yii::$app->controller->action->id ?>">
<?php $this->beginBody() ?>

    <?=  HeaderNavbarMenu::widget() ?>
    <header id="header">
        <?= Header::widget(['action' => Yii::$app->controller->action->id]) ?>
    </header>
    
<main id="main" class="wrap">      
    <?= Alert::widget() ?>
    <?= $content ?>
</main>

<footer class="footer">
    <div class="container">

    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
