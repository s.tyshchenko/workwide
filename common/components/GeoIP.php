<?php
namespace common\components;

use MaxMind\Db\Reader;
use Yii;
use Exception;
use yii\base\BaseObject;

/**
 * Class GeoIP
 */
class GeoIP extends BaseObject {
    /**
     * @var string
     */
    public $mmdb = '@common/components/geoip/geoip_city.mmdb';
    /**
     * @var string
     */
    public $lng = 'en';
    /**
     * @var Reader
     */
    private $reader;

    /**
     * @var array
     */
    private $availableLng = ['de', 'en', 'es', 'ja', 'fr', 'ru', 'zh-CN'];

    /**
     * @inheritDoc
     */
    public function init() {
        $db = Yii::getAlias($this->mmdb);
        $this->reader = new Reader($db);

        if(!in_array($this->lng, $this->availableLng)) throw new Exception("Unknown language");

        parent::init();
    }

    /**
     * @param string|null $ip
     * @return Result
     */
    public function ip($ip = null) {
        if ($ip === null) {
            $ip = Yii::$app->request->getUserIP();
        }

        $result = $this->reader->get($ip);
        return new Result($result, $this->lng);
    }
}

class Result {
    /**
     * @var array
     */
    private $data;
    /**
     * @var string
     */
    private $lng;

    public function __construct($data, $lng) {
        $this->data = $data;
        $this->lng = $lng;
    }

    public function __get($name) {
        $getter = 'get' . ucfirst($name);

        if (method_exists($this, $getter)) {
            return $this->$getter($this->data);
        }

        throw new Exception("Unknown property");
    }

    protected function getCity() {
        $value = null;

        if (isset($this->data['city']['names'][$this->lng])) {
            $value = $this->data['city']['names'][$this->lng];
        }

        return $value;
    }

    protected function getCountry() {
        $value = null;

        if (isset($this->data['country']['names'][$this->lng])) {
            $value = $this->data['country']['names'][$this->lng];
        }

        return $value;
    }

    protected function getContinent() {
        $value = null;

        if (isset($this->data['continent']['names'][$this->lng])) {
            $value = $this->data['continent']['names'][$this->lng];
        }

        return $value;
    }

    protected function getLocation() {
        $value = new Location();

        if (isset($this->data['location'])) {
            $latitude = $this->data['location']['latitude'];
            $longitude = $this->data['location']['longitude'];
            $timezone = $this->data['location']['time_zone'];
            $value = new Location($latitude, $longitude, $timezone);
        }

        return $value;
    }

    protected function getTimeZone() {
        $value = new Location();

        if (isset($this->data['location'])) {
            $value = $this->data['location']['time_zone'];
        }

        return $value;
    }

    protected function getSubdivisions() {
        $value = null;

        if(isset($this->data['subdivisions']) && is_array($this->data['subdivisions'])) {
            $arr = [];
            foreach($this->data['subdivisions'] as $subdivision){
                $arr[] = $subdivision['names'][$this->lng];
            }
            $value = implode(', ', $arr);
        }

        return $value;
    }

    protected function getIsoCode() {
        $value = null;
        if (isset($this->data['country']['iso_code'])) {
            $value = $this->data['country']['iso_code'];
        }
        return $value;
    }

}

class Location {

    /**
     * @var float|null
     */
    public $latitude;

    /**
     * @var float|null
     */
    public $longitude;
    
    /**
     * @var string|null
     */
    public $timezone;

    public function __construct($latitude = null, $longitude = null, $timezone = null) {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->timezone = $timezone;
    }
}