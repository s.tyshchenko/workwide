<?php

namespace common\components;

use Yii;
use yii\i18n\MissingTranslationEvent;

class TranslationEventHandler
{
    public static function handleMissingTranslation(MissingTranslationEvent $event)
    {        
        $category = $event->category;
        $message = $event->message;
        $language = $event->language;
        
        $translations_file = Yii::getAlias('@jobseeker/messages/' . $language . '/' . $category . '.json');
                
        $missed_translations = json_decode(file_get_contents($translations_file));
        
        if(!isset($missed_translations->$message))
        {
            $missed_translations->$message = $message;
        }
        
        file_put_contents($translations_file, json_encode($missed_translations, JSON_UNESCAPED_UNICODE));
        
        $event->translatedMessage = $event->message;
    }
    
}