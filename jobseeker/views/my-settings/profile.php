<?php

use jobseeker\widgets\MySettingsNav;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use yii\helpers\Html;
use jobseeker\assets\AppAsset;
use common\assets\CommonAsset;
use common\widgets\WImages;
use jobseeker\models\ProfileSettingsVariables;

$this->registerCssFile(
    '@web/css/jcrop.min.css',
    [
        'rel' => 'stylesheet',
        'type' => 'text/css',
        'depends' => AppAsset::class
    ]
);

$this->registerJsFile(
    '@web/js/jcrop.min.js',
    [
        'position' => \yii\web\View::POS_HEAD,
        'depends' => [
            AppAsset::class,
            CommonAsset::class
        ]
    ]
);

$this->registerJsFile(
    '@web/js/select2.js',
    [
        'position' => \yii\web\View::POS_HEAD,
        'depends' => [
            AppAsset::class,
            CommonAsset::class
        ]
    ]
);

$this->registerJsFile(
    '@web/js/actual.min.js',
    [
        'position' => \yii\web\View::POS_HEAD,
        'depends' => [
            AppAsset::class,
            CommonAsset::class
        ]
    ]
);

$this->registerJsFile(
    '@web/js/pjax-form-loading.js',
    [
        'position' => \yii\web\View::POS_HEAD,
        'depends' => [
            AppAsset::class,
            CommonAsset::class
        ]
    ]
);

$this->registerJs("
    function imageJcrop(image, c = [0,0,200,200], boxWidth,  trueSize)
    {
        $(image).Jcrop({
            minSize: [200,200],
            bgColor: 'black',
            bgOpacity: .4,
            aspectRatio: 1,
            onSelect: profileImageCrop,
            setSelect: c,
            boxWidth: boxWidth,
            trueSize: trueSize
        });
    }

    function updateProfilePhotoAjax(form, pjaxModalContainer, pjaxContainer)
    {
        if ($(form).find('.has-error').length) 
            return false;
        
        var formData = new FormData(form);

        $.ajax({
            url: '',
            cache: false,
            contentType: false,
            timeout: 50000,
            processData: false,
            type: 'POST',
            data: formData,
            success: function(response){
                $(pjaxModalContainer).toggleClass('loading');
                pjaxContainerReload(pjaxModalContainer);
                
                if(response != 'photo_upload_success')
                {
                    $(pjaxContainer).toggleClass('loading');
                    $(form).parents('.modal').modal('hide');
                    pjaxContainerReload(pjaxContainer);
                }
            }
        });

        return false;
    }

    function profileImageCrop(c){
        $('#edit-photo-form-crop-x1').val(c.x);
        $('#edit-photo-form-crop-y1').val(c.y);
        $('#edit-photo-form-crop-height').val(c.h);
        $('#edit-photo-form-crop-width').val(c.w);
    }
        
    var formatRepo = function (repo) {
        if (repo.loading) {
            return repo.title;
        }
        
        var markup =
        '<div class=\"row\"><b>' + repo.title + '</b></div>';
        
        if (repo.description) {
          markup += '<p>' + repo.description + '</p>';
        }
        return '<div style=\"overflow:hidden;\">' + markup + '</div>';
    };

    ",
    \yii\web\View::POS_HEAD
);

$this->registerJs("
    function profileSkillSelect(e, skill){
        if($(e.srcElement).hasClass('professional-skill'))
        {
            $(skill).toggleClass('active');
            $(skill).find('input[name*=\"skill_checked\"]').prop('value', function(i, val){ return (val == '1') ? '0' : '1'; });
        }
    }

    ",
    \yii\web\View::POS_HEAD
);

$AddProfileFormSpecializationChange = <<<JS
function(e){
    if(e.params._type == 'select')
    {
        $('#add-profile-form-categories-select').prop('disabled', false);
    }
    else
    {
        $('#add-profile-form-categories-select').prop('disabled', true);
    }
}
JS;

$EditProfileFormSpecializationChange = <<<JS
function(e){
    if(e.params._type == 'select')
    {
        $('#add-profile-form-categories-select').prop('disabled', false);
    }
    else
    {
        $('#add-profile-form-categories-select').prop('disabled', true);
    }
}
JS;

$AddEducationCountryChange = <<<JS
function(e){
    if(e.params._type == 'select')
    {
        $('#add-education-form-educational-institution-select').prop('disabled', false);
    }
    else
    {
        $('#add-education-form-educational-institution-select').prop('disabled', true);
    }
}
JS;

?>

<?php
                
$SettingsSelectData = new ProfileSettingsVariables();

?>

<div class="container my-5 py-0 px-0">
    <div class="row mx-0">
        
        <?= MySettingsNav::widget() ?>
                        
        <div class="col-12 col-md-9 px-0">
            <div class="b-1 bg-white br-2 py-4 px-4">
            <?php
                
                Pjax::begin([ 'id' => 'Jobseeker', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n4 pt-4 mb-n3 pb-3']]); 
                
            ?>
                <div class="row mx-0">
                    <div class="container mb-2">
                        <h3 class="font-weight-normal"><?= Yii::t('jobseeker', 'Profile settings') ?></h3>
                    </div>
                    <div class="container mb-4">
                        <div id="profile-photo-container" class="mt-2 px-0" style="background-image: url(<?= (empty($Jobseeker->photo->path_cropped)) ?  WImages::widget(['title' => 'profile-photo-empty']) : $Jobseeker->photo->path_cropped ?>);">
                            <span class="profile-control-buttons-container">
                                <div class="profile-control-button-block" data-toggle="modal" data-target="#edit-photo-modal">
                                    <button class="profile-control-text-button position-relative" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit photo" style="top:-3px;">
                                        <i class="fas fa-pencil"></i>
                                        &nbsp;Edit                                    
                                    </button>   
                                </div> 
                            </span>
                        </div>
                    </div>
                    <div class="container">
                        <div class="col text-main font-weight-bold pl-0 py-1">
                            <?= Yii::t('jobseeker', 'Visibility') ?>:
                            <span class="profile-control-buttons-container">
                                <div class="profile-control-button-block" data-toggle="modal" data-target="#edit-visibility-modal">
                                    <button class="profile-control-text-button ml-2 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Edit visibility') ?>">
                                        <i class="fas fa-pencil"></i>
                                        &nbsp;<?= Yii::t('jobseeker', 'Edit') ?>
                                    </button>   
                                </div> 
                            </span>
                        </div>
                        <div class="col text-secondary pl-0 py-1">
                            <?= $SettingsSelectData->profile_visibility[$Jobseeker->visibility] ?>
                        </div>
                    </div>
                    
                </div>
            <?php
            
                Pjax::end(); 
                
            ?>
            
            <hr class="bt-1 mx-n4">    
                
            <?php 
            
            if(!empty($JobseekerGeneralProfile)):   
                
                Pjax::begin([ 'id' => 'JobseekerGeneralProfile', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n3 pb-3']]); 
                
            ?>
                
                <div class="row mx-0" id="general-profile-container">  
                    
                    <?php
                    
                    $JobseekerGeneralProfileTitle = md5($JobseekerGeneralProfile->title);
                    
                    ?>
                    
                    <div class="container mb-2">
                        <h5 class="font-weight-normal">
                            <?= Yii::t('jobseeker', 'General profile') ?>:
                            <span class="profile-control-buttons-container">
                                <div class="profile-control-button-block d-inline-block" data-toggle="modal" data-target="#edit-general-profile-modal">
                                    <button class="profile-control-text-button ml-2 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Edit general profile') ?>">
                                        <i class="fas fa-pencil"></i>
                                        &nbsp;<?= Yii::t('jobseeker', 'Edit') ?>
                                    </button>                                     
                                </div>

                                <div class="profile-control-button-block d-inline-block" data-toggle="modal" data-target="#edit-general-profile-modal">
                                    <button class="profile-control-text-button ml-2 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'View general profile') ?>">
                                        <i class="fas fa-eye"></i>
                                        &nbsp;<?= Yii::t('jobseeker', 'View') ?>
                                    </button>                                     
                                </div>
                            </span>
                        </h5>
                    </div>
                                    
                    <div class="container mb-2 px-0 general-profile">
                        
                        <div class="col px-3 mb-2 py-2" data-toggle="collapse" data-target="#general-profile" aria-expanded="true">
                            <h5 class="mb-0">
                                <p class="text-secondary mb-0 mt-2" style="font-size: 13px;"></p>
                                <?= $JobseekerGeneralProfile->title ?>
                                <span class="h6 my-1 text-secondary"> - <?= $SettingsSelectData->specialization[$JobseekerGeneralProfile->specialization] ?></span>
                            </h5>
                            
                            <?php if(isset($JobseekerGeneralProfile->salary)): ?>
                            <p class="mb-1">
                                <?= Yii::t('jobseeker', 'Desired Salary: {desired_salary}', ['desired_salary' => $JobseekerGeneralProfile->salary]) ?>
                                <span class="h6 font-weight-light"><?= Yii::t('jobseeker', '{salary_currency}', ['salary_currency' => $JobseekerGeneralProfile->salary_currency]) ?></span>
                            </p>
                            <?php endif; ?>
                            
                            <ul class="profile-skills-list small mb-3">
                            <?php foreach (explode(', ', $JobseekerGeneralProfile->categories) as $ProfileCategory): ?>
                                <li><?= Yii::t('jobseeker', $ProfileCategory) ?></li>
                            <?php endforeach; ?>
                            </ul>
                            
                            <div class="profile-education-course-info  word-break text-pre-line mt-n2 mb-2 collapse <?= (!empty($JobseekerGeneralProfile->description)) ?: 'd-none' ?>" data-parent="#generall-profile-container" id="general-profile" style=""><?= $JobseekerGeneralProfile->description ?></div>
                        </div>
                        
                    </div> 
                    
                     <div class="modal fade" id="edit-general-profile-skills-modal" tabindex="-1" aria-labelledby="edit-general-profile-skills-modal-title" aria-hidden="true" role="dialog">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content br-2 b-none">
                                 <?php 
                                    $editProfileProfessionalSkillsForm = ActiveForm::begin([
                                        'id' => 'edit-general-profile-skills-form',
                                        'options' => [
                                            'onsubmit' => 'return submitFormAjax(this, "#JobseekerProfiles");',
                                            'style' => 'height: 100%; overflow-y: auto;'
                                        ]
                                    ]);
                                ?>
                                <div class="modal-header">
                                    <h5 class="modal-title" id="edit-general-profile-skills-modal-title"><?= Yii::t('jobseeker', 'Edit profile skills') ?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="professional-skills-container">
                                    <?php
                                    
                                    $SpecializationSkillGroups = \yii\helpers\ArrayHelper::map($JobseekerGeneralProfile->profileSpecializationSkills, 'title', 'title', 'group_title');
                                                                        
                                    foreach ($SpecializationSkillGroups as $SkillGroupTitle => $SkillGroupSkills):
                                    
                                    ?>
                                        <div class="professional-skills-group">
                                            <div class="group-title-container">
                                                <h6><?= $SkillGroupTitle ?></h6>
                                                <hr>
                                            </div>
                                            <div class="group-skills-container">
                                            <?php
                                            
                                            foreach($SkillGroupSkills as $Skill): 
                                                $SkillActive = array_search($Skill, array_column($JobseekerGeneralProfile->profileProfessionalSkills, 'skill'));
                                            ?>
                                                <div class="professional-skill <?= ($SkillActive === false) ?: 'active' ?>" onclick="profileSkillSelect(event, this)">
                                                    <?= $Skill ?>
                                                    <div class="skill-control-buttons dropdown">
                                                        <i class="skill-control-button--edit fas fa-pen-square fa-lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                                                        <div class="skill-edit-dropdown dropdown-menu py-0">
                                                            <span class="skill-edit-dropdown-header px-2 py-1 bb-1"><?= $Skill ?></span>
                                                            <?=
                                                                $editProfileProfessionalSkillsForm->field($editProfileProfessionalSkillsFormModel, 'skill[]', ['options' => ['tag' => false]])
                                                                    ->hiddenInput(['value' => $Skill])
                                                                    ->label(false) ?>
                                                            <?=
                                                                $editProfileProfessionalSkillsForm->field($editProfileProfessionalSkillsFormModel, 'skill_checked[]', ['options' => ['tag' => false]])
                                                                    ->hiddenInput(['value' => (($SkillActive !== false) ? 1 : 0)])
                                                                    ->label(false) ?>
                                                            <?=
                                                                $editProfileProfessionalSkillsForm->field($editProfileProfessionalSkillsFormModel, 'proficiency[]', ['options' => ['tag' => false]])
                                                                    ->dropDownList($SettingsSelectData->skill_proficiency,['prompt' => Yii::t('jobseeker', 'Proficiency'), 'class' => 'custom-select outline-none px-2 b-0 bb-1 br-0', 'value' => ($SkillActive === false) ?: $JobseekerGeneralProfile->profileProfessionalSkills[$SkillActive]->proficiency])
                                                                    ->label(false) ?>
                                                            
                                                            <?=
                                                                $editProfileProfessionalSkillsForm->field($editProfileProfessionalSkillsFormModel, 'last_used[]', ['options' => ['tag' => false]])
                                                                    ->dropDownList($SettingsSelectData->skill_last_used,['prompt' => Yii::t('jobseeker', 'Last used'), 'class' => 'custom-select outline-none px-2 b-0 bb-1 br-0', 'value' => ($SkillActive === false) ?: $JobseekerGeneralProfile->profileProfessionalSkills[$SkillActive]->last_used])
                                                                    ->label(false) ?>
                                                            
                                                            <?=
                                                                $editProfileProfessionalSkillsForm->field($editProfileProfessionalSkillsFormModel, 'years_of_experience[]', ['options' => ['tag' => false]])
                                                                    ->dropDownList(array_combine(range(1,10),range(1,10)),['prompt' => Yii::t('jobseeker', 'Years of experience'), 'class' => 'custom-select outline-none px-2 b-0 bb-1 br-0', 'value' => ($SkillActive === false) ?: $JobseekerGeneralProfile->profileProfessionalSkills[$SkillActive]->years_of_experience])
                                                                    ->label(false) ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                            </div>
                                        
                                        </div>
                                    <?php
                                    
                                    endforeach;
                                    
                                    ?>
                                    </div>
                                </div>
                               
                                <?=
                                    $editProfileProfessionalSkillsForm->field($editProfileProfessionalSkillsFormModel, 'profile_id')
                                        ->hiddenInput(['value'=> $JobseekerGeneralProfile->id])
                                        ->label(false) ?>
                                
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                                    <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Save') ?></button>
                                </div>
                                <?php 
                                    ActiveForm::end();
                                ?>
                            </div>
                        </div>
                    </div>
                    
                     <div class="modal fade" id="edit-general-profile-modal" tabindex="-1" aria-labelledby="edit-general-profile-modal-title" aria-hidden="true" role="dialog">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content br-2 b-none">
                                <?php 
                                    $editProfileForm = ActiveForm::begin([
                                        'id' => 'edit-general-profile-form',
                                        'options' => [
                                            'onsubmit' => 'return submitFormAjax(this, "#JobseekerGeneralProfile");',
                                            'style' => 'height: 100%; overflow-y: auto;'
                                            ]
                                    ]);
                                ?>
                                <div class="modal-header">
                                    <h5 class="modal-title" id="edit-general-profile-modal-title"><?= Yii::t('jobseeker', 'Edit '.$JobseekerGeneralProfile->title.' profile') ?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">

                                    <?= $editProfileForm
                                        ->field($editProfileFormModel, 'title', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-general-profile-form-title', 'placeholder' => Yii::t('jobseeker', 'e.g. Senior Web Developer')] ])
                                        ->label(Yii::t('jobseeker', 'Profile title') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label label-popover font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                                        ->hint(Yii::t('jobseeker', 'Enter your specialization title'), ['class' => 'hint-block text-secondary'])
                                        ->textInput(['value' => $JobseekerGeneralProfile->title]) ?>

                                    <?php 

                                        $initValueText = [];
                                        $editProfileFormModel->employment_type = [];
                                        foreach (explode(', ', $JobseekerGeneralProfile->employment_type) as $EmploymentType) {
                                            $initValueText[$EmploymentType]             = $SettingsSelectData->employment_types[$EmploymentType];
                                            $editProfileFormModel->employment_type[]    = $EmploymentType;
                                        }

                                    ?>

                                    <?= $editProfileForm
                                        ->field($editProfileFormModel, 'employment_type', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-general-profile-form-employment-type-select'] ])
                                        ->label(Yii::t('jobseeker', 'Employment type'), ['class' => 'control-label font-weight-normal text-main-2'])
                                        ->widget(Select2::classname(), 
                                        [
                                            'data' => $SettingsSelectData->employment_types,
                                            'theme' => 'my-settings','pluginLoading' => false,
                                            'initValueText' => $initValueText,
                                            'options' => [
                                                'multiple' => true,
                                                'placeholder' => Yii::t('jobseeker', 'Select desired employment type(s)')
                                            ],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'maximumInputLength' => 10
                                            ],
                                        ])
                                    ?>
                                    <div class="form-group field-edit-general-profile-form-salary col-6 px-0 float-left">
                                    <?= $editProfileForm
                                        ->field($editProfileFormModel, 'salary', ['addAriaAttributes' => true, 'enableClientValidation' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-general-profile-form-salary'] ])
                                        ->label(Yii::t('jobseeker', 'Salary') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                                        ->hint(Yii::t('jobseeker', 'Enter your desired salary'), ['class' => 'hint-block text-secondary'])
                                        ->textInput(['value' => $JobseekerGeneralProfile->salary]) ?>
                                    </div>

                                    <div class="form-group field-edit-general-profile-form-salary-currency col-5 offset-1 px-0 float-left">
                                    <?php $editProfileFormModel->salary_currency = $JobseekerGeneralProfile->salary_currency; ?>

                                    <?= $editProfileForm->field($editProfileFormModel, 'salary_currency', ['addAriaAttributes' => true, 'enableClientValidation' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-general-profile-form-currency-select'] ])->label(Yii::t('jobseeker', 'Currency'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                                        [
                                            'data' => $SettingsSelectData->currencies,
                                            'theme' => 'my-settings','pluginLoading' => false,
                                            'initValueText' => $SettingsSelectData->currencies[$JobseekerGeneralProfile->salary_currency],
                                            'options' => [
                                                'placeholder' => Yii::t('jobseeker', 'Currency'),
                                            ],                        
                                            'pluginLoading' => false,
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ]
                                        ])
                                    ?>
                                    </div>
                                    
                                    <div class="form-group field-edit-general-profile-form-soft-skills-select col-12 px-0 float-left">
                                    <?php 
                                    
                                        $editProfileFormModel->soft_skills = explode(', ', $JobseekerGeneralProfile->soft_skills);
                                    
                                    ?>
                                    <?= $editProfileForm->field($editProfileFormModel, 'soft_skills', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-general-profile-form-soft-skills-select'] ])->label(Yii::t('jobseeker', 'Soft skills'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                                        [
                                            'maintainOrder' => true,
                                            'theme' => 'my-settings','pluginLoading' => false,
                                            'options' => [
                                                'multiple' => true,
                                                'placeholder' => Yii::t('jobseeker', 'Select soft skills')
                                            ],
                                            'pluginLoading' => false,
                                            'pluginOptions' => [
                                                'tags' => true,
                                                'allowClear' => true,
                                                'minimumInputLength' => 2,
                                                'ajax' => [
                                                    'url' => Yii::getAlias('@web') . '/api/v1/soft-skills/get-skills',
                                                    'dataType' => 'json',
                                                    'data' => new JsExpression('function(params) { return {title_prefix:params.term}; }'),
                                                ],
                                                'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                                                'templateResult' => new JsExpression('function(soft_skills) { return soft_skills.title; }'),
                                                'templateSelection' => new JsExpression('function(soft_skills) { return soft_skills.text || soft_skills.title; }'),
                                            ],
                                            
                                        ])
                                    ?>
                                    </div>

                                    <div class="form-group field-edit-general-profile-form-specialization-select required col-12 px-0 float-left">
                                    <?php $editProfileFormModel->specialization = $JobseekerGeneralProfile->specialization; ?>

                                    <?= $editProfileForm->field($editProfileFormModel, 'specialization', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-general-profile-form-specialization-select'] ])->label(Yii::t('jobseeker', 'Specialization'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                                        [
                                            'data' => $SettingsSelectData->specialization,
                                            'theme' => 'my-settings','pluginLoading' => false,
                                            'options' => [
                                                'placeholder' => Yii::t('jobseeker', 'Select specialization')
                                            ],
                                            'pluginLoading' => false,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                            ],
                                            'pluginEvents' => [
                                                'select2:select' => new JsExpression('function(e){ $("#edit-general-profile-form-categories-select").prop("disabled", ((e.params._type == "select") ? false : true));}'),
                                                'select2:clear' => new JsExpression('function(e){ $("#edit-general-profile-form-categories-select").prop("disabled", ((e.params._type == "select") ? false : true));}')
                                            ]
                                        ])
                                    ?>
                                    </div>
                                    <div class="form-group field-edit-general-profile-form-categories-select required col-12 px-0 float-left">
                                    <?php

                                        $editProfileFormModel->categories = explode(', ', $JobseekerGeneralProfile->categories);
                                        
                                    ?>    

                                    <?= $editProfileForm->field($editProfileFormModel, 'categories', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}",  'inputOptions' => ['class' => 'form-control', 'id' => 'edit-general-profile-form-categories-select'] ])->label(Yii::t('jobseeker', 'Categories'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                                        [
                                            'theme' => 'my-settings','pluginLoading' => false,
                                            'options' => [
                                                'multiple' => true,
                                                'placeholder' => Yii::t('jobseeker', 'Select categories')
                                            ],
                                            'hideSearch' => true,
                                            'pluginLoading' => true,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'minimumInputLength' => 0,
                                                'language' => [
                                                    'errorLoading' => new JsExpression("function () { return '" . Yii::t('jobseeker', 'Waiting for results...') . "'; }"),
                                                ],
                                                'ajax' => [
                                                    'url' => Yii::getAlias('@web') . '/api/v1/specializations/categories',
                                                    'dataType' => 'json',
                                                    'data' => new JsExpression('function(params) { return {specialization_id:$("#edit-general-profile-form-specialization-select").val(), title_prefix:params.term}; }'),
                                                ],
                                                'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                                                'templateResult' => new JsExpression('function(categories) { return categories.text || categories.title; }'),
                                                'templateSelection' => new JsExpression('function(categories) { return categories.text || categories.title; }'),
                                            ]
                                        ])
                                    ?>
                                    </div>
                                    <div class="form-group field-edit-general-profile-form-description col-12 px-0 float-left">
                                    <?= $editProfileForm
                                        ->field($editProfileFormModel, 'description', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-general-profile-form-description'] ])
                                        ->label(Yii::t('jobseeker', 'Pitch') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                                        ->hint(Yii::t('jobseeker', 'Use this space to show employers you have the skills and experience they\'re looking for.') . '<ul class="pl-3 mb-0"><li>' . Yii::t('jobseeker', 'Describe your strengths and skills') . '</li><li>' . Yii::t('jobseeker', 'Highlight projects, accomplishments and education.' . '</li><li>' . Yii::t('jobseeker', 'Keep it short and make sure it\'s error-free').'</li></ul>'), ['class' => 'hint-block text-secondary'])
                                        ->textarea(['value' => $JobseekerGeneralProfile->description])?> 
                                    </div>
                                    <?=
                                        $editProfileForm->field($editProfileFormModel, 'profile_id')
                                            ->hiddenInput(['value'=> $JobseekerGeneralProfile->id])
                                            ->label(false) ?>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                                    <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Save') ?></button>
                                </div>
                                <?php 
                                    ActiveForm::end();
                                ?>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                
                
            <?php
                
                Pjax::end(); 
            endif;
            
            ?>
            
            <hr class="bt-1 mx-n4">    
                
            <?php
            
            if(!empty($JobseekerProfiles)):
                    
                Pjax::begin([ 'id' => 'JobseekerProfiles', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n4 pb-4']]); 
                
            ?> 
                
                <div class="row mx-0" id="spezialized-profiles-container">
                    
                    <div class="container mb-2">
                        <h5 class="font-weight-normal">
                            <?= Yii::t('jobseeker', 'Specialized profiles') ?>:
                            <span class="profile-control-buttons-container">
                                <div class="profile-control-button-block" data-toggle="modal" data-target="#add-profile-modal">
                                    <button class="profile-control-text-button ml-2 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Add specialized profile') ?>">
                                        <i class="fas fa-plus"></i>
                                        &nbsp;<?= Yii::t('jobseeker', 'Add new') ?>
                                    </button>   
                                </div> 
                            </span>
                        </h5>
                    </div>
                    
                    <?php
                    
                        foreach ($JobseekerProfiles as $JobseekerProfile):
                            $JobseekerProfileTitle = md5($JobseekerProfile->title);
                    ?>
                    
                    <div class="container mb-2 px-0 specialized-profile">
                       
                                            
                        <div class="col  px-3 mb-2 py-2" data-toggle="collapse" data-target="#specialized-profile--<?= $JobseekerProfileTitle ?>" aria-expanded="true">
                            <h5 class="mb-0">
                                <p class="text-secondary mb-0 mt-2" style="font-size: 13px;"><?= $SettingsSelectData->specialization[$JobseekerProfile->specialization] ?></p>
                                <?= $JobseekerProfile->title ?>
                                <span class="h6 text-secondary font-weight-normal ml-2">
                                    <?php switch($JobseekerProfile->visibility):
                                        case 'all':
                                            echo '<i class="fas fa-toggle-on mr-1 text-link"></i> ' . Yii::t('jobseeker', 'Published');
                                            break;
                                        case 'none':
                                            echo '<i class="fas fa-toggle-off mr-1"></i> ' . Yii::t('jobseeker', 'Disabled');
                                            break;
                                    endswitch; ?>
                                </span>
                                
                            </h5>
                            
                            <?php if(isset($JobseekerProfile->salary)): ?>
                            <p class="mb-1">
                                <?= Yii::t('jobseeker', 'Desired Salary: {desired_salary}', ['desired_salary' => $JobseekerProfile->salary]) ?>
                                <span class="h6 font-weight-light"><?= Yii::t('jobseeker', '{salary_currency}', ['salary_currency' => $JobseekerProfile->salary_currency]) ?></span>
                            </p>
                            <?php endif; ?>
                            
                            <ul class="profile-skills-list small mb-3">
                            <?php foreach (explode(', ', $JobseekerProfile->categories) as $ProfileCategory): ?>
                                <li><?= Yii::t('jobseeker', $ProfileCategory) ?></li>
                            <?php endforeach; ?>
                            </ul>
                            
                            <div class="profile-education-course-info word-break text-pre-line mt-n2 mb-2 collapse  <?= (!empty($JobseekerProfile->description)) ?: 'd-none' ?>" data-parent="#spezialized-profiles-container" id="specialized-profile--<?= $JobseekerProfileTitle ?>" style=""><?= $JobseekerProfile->description ?></div>
                            <span class="profile-control-buttons-container mr-3 mt-3" style="top: 0;right: 0;">
                                <div class="profile-control-button-block d-inline-block" data-toggle="modal" data-target="#edit-profile-modal--<?= $JobseekerProfileTitle ?>">
                                    <button class="profile-control-button" style="height: 32px;width: 32px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Remove specialization') ?>">
                                        <i class="fas fa-pencil"></i>
                                    </button>                                     
                                </div> 
                                <div class="profile-control-button-block d-inline-block" >

                                    <button class="profile-control-button outline-none" style="height: 32px;width: 32px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-h"></i>
                                    </button>      
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <?php

                                        if($JobseekerProfile->visibility == 'all'):

                                        ?>

                                        <?= Html::button(Yii::t('jobseeker', 'Disable'), ['class' => 'dropdown-item', 'onclick' => 
                                           '$("#JobseekerProfiles").toggleClass("loading");
                                            $.ajax({
                                                type: "POST",
                                                url: "",
                                                data: {
                                                    action: "JobseekerProfileDisable",
                                                    profile_id: "'.$JobseekerProfile->id.'"
                                                }, success: function(response) {
                                                    if(response) {
                                                        pjaxContainerReload("#JobseekerProfiles");
                                                    }
                                                }
                                            });'
                                        ]) ?>

                                        <?php

                                        else:

                                        ?>

                                         <?= Html::button(Yii::t('jobseeker', 'Enable'), ['class' => 'dropdown-item', 'onclick' => 
                                           '$("#JobseekerProfiles").toggleClass("loading");
                                            $.ajax({
                                                type: "POST",
                                                url: "",
                                                data: {
                                                    action: "JobseekerProfileEnable",
                                                    profile_id: "'.$JobseekerProfile->id.'"
                                                }, success: function(response) {
                                                    if(response) {
                                                        pjaxContainerReload("#JobseekerProfiles");
                                                    }
                                                }
                                            });'
                                        ]) ?>

                                        <?php

                                        endif;

                                        ?>

                                        <a class="dropdown-item" href="#">View</a>

                                        <?= Html::button(Yii::t('jobseeker', 'Remove'), ['class' => 'dropdown-item', 'onclick' => 
                                           '$("#JobseekerProfiles").toggleClass("loading");
                                            $.ajax({
                                                type: "POST",
                                                url: "",
                                                data: {
                                                    action: "JobseekerProfileRemove",
                                                    profile_id: "'.$JobseekerProfile->id.'"
                                                }, success: function(response) {
                                                    if(response) {
                                                        pjaxContainerReload("#JobseekerProfiles");
                                                    }
                                                }
                                            });'
                                        ]) ?>
                                        
                                        <?= Html::button(Yii::t('jobseeker', 'Edit skills'), ['class' => 'dropdown-item', 'data-toggle' => 'modal', 'data-target' => '#edit-profile-skills-modal--' . $JobseekerProfileTitle]) ?>
                                        
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                    
                    <div class="modal fade" id="edit-profile-skills-modal--<?= $JobseekerProfileTitle ?>" tabindex="-1" aria-labelledby="edit-profile-skills-modal-title" aria-hidden="true" role="dialog">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content br-2 b-none">
                                 <?php 
                                    $editProfileProfessionalSkillsForm = ActiveForm::begin([
                                        'id' => 'edit-profile-skills-form--' . $JobseekerProfileTitle,
                                        'options' => [
                                            'onsubmit' => 'return submitFormAjax(this, "#JobseekerProfiles");',
                                            'style' => 'height: 100%; overflow-y: auto;'
                                        ]
                                    ]);
                                ?>
                                <div class="modal-header">
                                    <h5 class="modal-title" id="edit-profile-skills-modal-title"><?= Yii::t('jobseeker', 'Edit profile profile skills') ?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="professional-skills-container">
                                    <?php
                                                                                                            
                                    $SpecializationSkillGroups = \yii\helpers\ArrayHelper::map($JobseekerProfile->profileSpecializationSkills, 'title', 'title', 'group_title');

                                    foreach ($SpecializationSkillGroups as $SkillGroupTitle => $SkillGroupSkills):
                                    
                                    ?>
                                        <div class="professional-skills-group">
                                            <div class="group-title-container">
                                                <h6><?= $SkillGroupTitle ?></h6>
                                                <hr>
                                            </div>
                                            <div class="group-skills-container">
                                            <?php
                                            
                                            foreach($SkillGroupSkills as $Skill): 
                                                $SkillActive = array_search($Skill, array_column($JobseekerProfile->profileProfessionalSkills, 'skill'));
                                            ?>
                                                <div class="professional-skill <?= ($SkillActive === false) ?: 'active' ?>" onclick="profileSkillSelect(event, this)">
                                                    <?= $Skill ?>
                                                    <div class="skill-control-buttons dropdown">
                                                        <i class="skill-control-button--edit fas fa-pen-square fa-lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                                                        <div class="skill-edit-dropdown dropdown-menu py-0">
                                                            <span class="skill-edit-dropdown-header px-2 py-1 bb-1"><?= $Skill ?></span>
                                                            <?=
                                                                $editProfileProfessionalSkillsForm->field($editProfileProfessionalSkillsFormModel, 'skill[]', ['options' => ['tag' => false]])
                                                                    ->hiddenInput(['value' => $Skill])
                                                                    ->label(false) ?>
                                                            <?=
                                                                $editProfileProfessionalSkillsForm->field($editProfileProfessionalSkillsFormModel, 'skill_checked[]', ['options' => ['tag' => false]])
                                                                    ->hiddenInput(['value' => (($SkillActive !== false) ? 1 : 0)])
                                                                    ->label(false) ?>
                                                            <?=
                                                                $editProfileProfessionalSkillsForm->field($editProfileProfessionalSkillsFormModel, 'proficiency[]', ['options' => ['tag' => false]])
                                                                    ->dropDownList($SettingsSelectData->skill_proficiency,['prompt' => Yii::t('jobseeker', 'Proficiency'), 'class' => 'custom-select outline-none px-2 b-0 bb-1 br-0', 'value' => ($SkillActive === false) ?'': $JobseekerProfile->profileProfessionalSkills[$SkillActive]->proficiency])
                                                                    ->label(false) ?>
                                                            
                                                            <?=
                                                                $editProfileProfessionalSkillsForm->field($editProfileProfessionalSkillsFormModel, 'last_used[]', ['options' => ['tag' => false]])
                                                                    ->dropDownList($SettingsSelectData->skill_last_used,['prompt' => Yii::t('jobseeker', 'Last used'), 'class' => 'custom-select outline-none px-2 b-0 bb-1 br-0', 'value' => ($SkillActive === false) ?'': $JobseekerProfile->profileProfessionalSkills[$SkillActive]->last_used])
                                                                    ->label(false) ?>
                                                            
                                                            <?=
                                                                $editProfileProfessionalSkillsForm->field($editProfileProfessionalSkillsFormModel, 'years_of_experience[]', ['options' => ['tag' => false]])
                                                                    ->dropDownList(array_combine(range(1,10),range(1,10)),['prompt' => Yii::t('jobseeker', 'Years of experience'), 'class' => 'custom-select outline-none px-2 b-0 bb-1 br-0', 'value' => ($SkillActive === false) ?'': $JobseekerProfile->profileProfessionalSkills[$SkillActive]->years_of_experience])
                                                                    ->label(false) ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                            </div>
                                        
                                        </div>
                                    <?php
                                    
                                    endforeach;
                                    
                                    ?>
                                    </div>
                                    

                                </div>
                               
                                
                                
                                    
                                    
                                    
                                    <?=
                                        $editProfileProfessionalSkillsForm->field($editProfileProfessionalSkillsFormModel, 'profile_id')
                                            ->hiddenInput(['value'=> $JobseekerProfile->id])
                                            ->label(false) ?>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                                    <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Save') ?></button>
                                </div>
                                <?php 
                                    ActiveForm::end();
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="edit-profile-modal--<?= $JobseekerProfileTitle ?>" tabindex="-1" aria-labelledby="edit-profile-modal-title--<?= $JobseekerProfileTitle ?>" aria-hidden="true" role="dialog">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content br-2 b-none">
                                <?php 
                                    $editProfileForm = ActiveForm::begin([
                                        'id' => 'edit-profile-form--' . $JobseekerProfileTitle,
                                        'options' => [
                                            'onsubmit' => 'return submitFormAjax(this, "#JobseekerProfiles");',
                                            'style' => 'height: 100%; overflow-y: auto;'
                                            ]
                                    ]);
                                ?>
                                <div class="modal-header">
                                    <h5 class="modal-title" id="edit-profile-modal-title-<?= $JobseekerProfileTitle ?>"><?= Yii::t('jobseeker', 'Edit '.$JobseekerProfile->title.' profile') ?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">

                                    <?= $editProfileForm
                                        ->field($editProfileFormModel, 'title', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-profile-form-title-' . $JobseekerProfileTitle, 'placeholder' => Yii::t('jobseeker', 'e.g. Senior Web Developer')] ])
                                        ->label(Yii::t('jobseeker', 'Profile title') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label label-popover font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                                        ->hint(Yii::t('jobseeker', 'Enter your specialization title'), ['class' => 'hint-block text-secondary'])
                                        ->textInput(['value' => $JobseekerProfile->title]) ?>

                                    <?php 

                                        $initValueText = [];
                                        $editProfileFormModel->employment_type = [];
                                        foreach (explode(', ', $JobseekerProfile->employment_type) as $EmploymentType) {
                                            $initValueText[$EmploymentType]             = $SettingsSelectData->employment_types[$EmploymentType];
                                            $editProfileFormModel->employment_type[]    = $EmploymentType;
                                        }

                                    ?>

                                    <?= $editProfileForm
                                        ->field($editProfileFormModel, 'employment_type', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-profile-form-employment-type-select-' . $JobseekerProfileTitle] ])
                                        ->label(Yii::t('jobseeker', 'Employment type'), ['class' => 'control-label font-weight-normal text-main-2'])
                                        ->widget(Select2::classname(), 
                                        [
                                            'data' => $SettingsSelectData->employment_types,
                                            'theme' => 'my-settings','pluginLoading' => false,
                                            'initValueText' => $initValueText,
                                            'options' => [
                                                'multiple' => true,
                                                'placeholder' => Yii::t('jobseeker', 'Select desired employment type(s)')
                                            ],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'maximumInputLength' => 10
                                            ],
                                        ])
                                    ?>
                                    <div class="form-group field-edit-profile-form--<?= $JobseekerProfileTitle ?>-salary col-6 px-0 float-left">
                                    <?= $editProfileForm
                                        ->field($editProfileFormModel, 'salary', ['addAriaAttributes' => true, 'enableClientValidation' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-profile-form-salary--' . $JobseekerProfileTitle] ])
                                        ->label(Yii::t('jobseeker', 'Salary') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                                        ->hint(Yii::t('jobseeker', 'Enter your desired salary'), ['class' => 'hint-block text-secondary'])
                                        ->textInput(['value' => $JobseekerProfile->salary]) ?>
                                    </div>

                                    <div class="form-group field-edit-profile-form--<?= $JobseekerProfileTitle ?>-salary-currency col-5 offset-1 px-0 float-left">
                                    <?php $editProfileFormModel->salary_currency = $JobseekerProfile->salary_currency; ?>

                                    <?= $editProfileForm->field($editProfileFormModel, 'salary_currency', ['addAriaAttributes' => true, 'enableClientValidation' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-profile-form-currency-select-' . $JobseekerProfileTitle] ])->label(Yii::t('jobseeker', 'Currency'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                                        [
                                            'data' => $SettingsSelectData->currencies,
                                            'theme' => 'my-settings','pluginLoading' => false,
                                            'initValueText' => $SettingsSelectData->currencies[$JobseekerProfile->salary_currency],
                                            'options' => [
                                                'placeholder' => Yii::t('jobseeker', 'Currency'),
                                            ],                        
                                            'pluginLoading' => false,
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ]
                                        ])
                                    ?>
                                    </div>

                                    <div class="form-group field-edit-profile-form--<?= $JobseekerProfileTitle ?>-soft-skills-select col-12 px-0 float-left">
                                    <?php 
                                    
                                        $editProfileFormModel->soft_skills = explode(', ', $JobseekerProfile->soft_skills);
                                    
                                    ?>
                                    <?= $editProfileForm->field($editProfileFormModel, 'soft_skills', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-profile-form-soft-skills-select--' . $JobseekerProfileTitle] ])->label(Yii::t('jobseeker', 'Soft skills'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                                        [
                                            'maintainOrder' => true,
                                            'theme' => 'my-settings','pluginLoading' => false,
                                            'options' => [
                                                'multiple' => true,
                                                'placeholder' => Yii::t('jobseeker', 'Select soft skills')
                                            ],
                                            'pluginLoading' => false,
                                            'pluginOptions' => [
                                                'tags' => true,
                                                'allowClear' => true,
                                                'minimumInputLength' => 2,
                                                'ajax' => [
                                                    'url' => Yii::getAlias('@web') . '/api/v1/soft-skills/get-skills',
                                                    'dataType' => 'json',
                                                    'data' => new JsExpression('function(params) { return {title_prefix:params.term}; }'),
                                                ],
                                                'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                                                'templateResult' => new JsExpression('function(soft_skills) { return soft_skills.title; }'),
                                                'templateSelection' => new JsExpression('function(soft_skills) { return soft_skills.text || soft_skills.title; }'),
                                            ],
                                            
                                        ])
                                    ?>
                                    </div>

                                    <div class="form-group field-edit-profile-form--<?= $JobseekerProfileTitle ?>-specialization-select required col-12 px-0 float-left">
                                    <?php $editProfileFormModel->specialization = $JobseekerProfile->specialization; ?>

                                    <?= $editProfileForm->field($editProfileFormModel, 'specialization', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-profile-form-specialization-select--' . $JobseekerProfileTitle] ])->label(Yii::t('jobseeker', 'Specialization'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                                        [
                                            'data' => $SettingsSelectData->specialization,
                                            'theme' => 'my-settings','pluginLoading' => false,
                                            'options' => [
                                                'placeholder' => Yii::t('jobseeker', 'Select specialization')
                                            ],
                                            'pluginLoading' => false,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                            ],
                                            'pluginEvents' => [
                                                'select2:select' => new JsExpression('function(e){ $("#edit-profile-form-categories-select--' . $JobseekerProfileTitle .'").prop("disabled", ((e.params._type == "select") ? false : true));}'),
                                                'select2:clear' => new JsExpression('function(e){ $("#edit-profile-form-categories-select--' . $JobseekerProfileTitle .'").prop("disabled", ((e.params._type == "select") ? false : true));}')
                                            ]
                                        ])
                                    ?>
                                    </div>
                                    <div class="form-group field-edit-profile-form--<?= $JobseekerProfileTitle ?>-categories-select required col-12 px-0 float-left">
                                    <?php

                                        $editProfileFormModel->categories = explode(', ', $JobseekerProfile->categories);
                                        
                                    ?>    

                                    <?= $editProfileForm->field($editProfileFormModel, 'categories', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}",  'inputOptions' => ['class' => 'form-control', 'id' => 'edit-profile-form-categories-select--' . $JobseekerProfileTitle] ])
                                        ->label(Yii::t('jobseeker', 'Categories'), ['class' => 'control-label font-weight-normal text-main-2'])
                                        ->widget(Select2::classname(), 
                                        [
                                            'theme' => 'my-settings','pluginLoading' => false,
                                            'options' => [
                                                'multiple' => true,
                                                'placeholder' => Yii::t('jobseeker', 'Select categories')
                                            ],
                                            'hideSearch' => true,
                                            'pluginLoading' => true,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'minimumInputLength' => 0,
                                                'language' => [
                                                    'errorLoading' => new JsExpression("function () { return '" . Yii::t('jobseeker', 'Waiting for results...') . "'; }"),
                                                ],
                                                'ajax' => [
                                                    'url' => Yii::getAlias('@web') . '/api/v1/specializations/categories',
                                                    'dataType' => 'json',
                                                    'data' => new JsExpression('function(params) { return {specialization_id:$("#edit-profile-form-specialization-select--' . $JobseekerProfileTitle . '").val(), title_prefix:params.term}; }'),
                                                ],
                                                'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                                                'templateResult' => new JsExpression('function(categories) { return categories.text || categories.title; }'),
                                                'templateSelection' => new JsExpression('function (categories) { return categories.text || categories.title; }'),
                                            ]
                                        ])
                                    ?>
                                    </div>
                                    <div class="form-group field-edit-profile-form--<?= $JobseekerProfileTitle ?>-description col-12 px-0 float-left">
                                    <?= $editProfileForm
                                        ->field($editProfileFormModel, 'description', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-profile-form-description--' . $JobseekerProfileTitle] ])
                                        ->label(Yii::t('jobseeker', 'Pitch') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                                        ->hint(Yii::t('jobseeker', 'Use this space to show employers you have the skills and experience they\'re looking for.') . '<ul class="pl-3 mb-0"><li>' . Yii::t('jobseeker', 'Describe your strengths and skills') . '</li><li>' . Yii::t('jobseeker', 'Highlight projects, accomplishments and education.' . '</li><li>' . Yii::t('jobseeker', 'Keep it short and make sure it\'s error-free').'</li></ul>'), ['class' => 'hint-block text-secondary'])
                                        ->textarea(['value' => $JobseekerProfile->description])?> 
                                    </div>
                                    <?=
                                        $editProfileForm->field($editProfileFormModel, 'profile_id')
                                            ->hiddenInput(['value'=> $JobseekerProfile->id])
                                            ->label(false) ?>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                                    <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Save') ?></button>
                                </div>
                                <?php 
                                    ActiveForm::end();
                                ?>
                            </div>
                        </div>
                    </div>
                    
                    <?php
                    
                        endforeach;
                    
                    
                    ?>
                </div>
                
            <?php
            
                Pjax::end(); 
            
            else: 
                Pjax::begin([ 'id' => 'JobseekerProfiles', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n4 pb-4']]); 
            
            ?>
                
                <div class="row mx-0" id="spezialized-profiles-container">
                    <div class="container mb-2">
                        <h5 class="font-weight-normal">
                            <?= Yii::t('jobseeker', 'Specialized profiles') ?>:
                            <span class="profile-control-buttons-container">
                                <div class="profile-control-button-block" data-toggle="modal" data-target="#add-profile-modal">
                                    <button class="profile-control-text-button ml-2 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Add specialized profile') ?>">
                                        <i class="fas fa-plus"></i>
                                        &nbsp;<?= Yii::t('jobseeker', 'Add new') ?>
                                    </button>   
                                </div> 
                            </span>
                        </h5>
                    </div>
                    <div class="container px-3 py-3 mx-3 bg-light">
                        <h6 class="text-secondary font-weight-normal mb-0">
                            <?= Yii::t('jobseeker', 'No specialized profiles added yet') ?>
                        </h6>
                    </div>
                </div>
                
            <?php
            
                Pjax::end();
            endif;
            
            ?>
                
            </div>
            
            
            <div class=" b-1 bg-white br-2 py-4 px-4 mt-3">  
                
                <div class="row mx-0">
                    <div class="container mb-2">
                        <h3 class="font-weight-normal"><?= Yii::t('jobseeker', 'Profile information') ?></h3>
                    </div>
                </div>
                
                <hr class="bt-1 mx-n4">
                
            <?php
                
            if(!empty($JobseekerLanguages)):
                    
                Pjax::begin([ 'id' => 'JobseekerLanguages', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n4 pb-4']]); 
                
            ?> 
                
                <div class="row mx-0"  id="jobseeker-languages-container">
                    <div class="container mb-2">
                        <h5 class="font-weight-normal">
                            <?= Yii::t('jobseeker', 'Languages') ?>:
                            <span class="profile-control-buttons-container">
                                <div class="profile-control-button-block" data-toggle="modal" data-target="#add-language-modal">
                                    <button class="profile-control-text-button ml-2 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Add language') ?>">
                                        <i class="fas fa-plus"></i>
                                        &nbsp;<?= Yii::t('jobseeker', 'Add new') ?>
                                    </button>   
                                </div> 
                            </span>
                        </h5>
                    </div>
                    
                    <?php

                        foreach ($JobseekerLanguages as $JobseekerLanguage):
                            $JobseekerLanguageTitle = md5($JobseekerLanguage->language);
                        
                    ?>
                    
                    <div class="container mb-2">
                        <div class="col-auto float-left text-main font-weight-bold pl-0 py-1">
                            <?= $JobseekerLanguage->i18nLanguage['english_name'] ?>:
                        </div>
                        <div class="col-auto float-left text-secondary pl-0 py-1">
                            <?= Yii::t('jobseeker', $SettingsSelectData->language_proficiency[$JobseekerLanguage->proficiency]) ?>
                        </div>
                        <span class="profile-control-buttons-container col-auto float-right">
                            <div class="profile-control-button-block d-inline-block" data-toggle="modal" data-target="#edit-language-modal--<?= $JobseekerLanguageTitle ?>">
                                <button class="profile-control-button" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Edit language') ?>">
                                    <i class="fas fa-pencil"></i>
                                </button>                                     
                            </div>
                            <div class="profile-control-button-block d-inline-block" >
                                <?= Html::button('<i class="fas fa-trash"></i>', ['class' => 'profile-control-button', 'style' => 'top:-3px;', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-original-title' => Yii::t('jobseeker', 'Edit language'), 'onclick' => 
                                   '$("#JobseekerLanguages").toggleClass("loading");
                                    $.ajax({
                                        type: "POST",
                                        url: "",
                                        data: {
                                            action: "JobseekerLanguageRemove",
                                            language: "'.$JobseekerLanguage->language.'"
                                        }, success: function(response) {
                                            if(response) {
                                                pjaxContainerReload("#JobseekerLanguages");
                                            }
                                        }
                                    });'
                                ]) ?>
                            </div>
                        </span>
                    </div>
                    
                    <div class="modal fade" id="edit-language-modal--<?= $JobseekerLanguageTitle ?>" tabindex="-1" aria-labelledby="edit-language-modal-title--<?= $JobseekerLanguageTitle ?>" aria-hidden="true" role="dialog">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content br-2 b-none">
                                <?php 
                                    $editLanguageForm = ActiveForm::begin([
                                        'id' => 'edit-language-form--' . $JobseekerLanguageTitle,
                                        'options' => [
                                            'onsubmit' => 'return submitFormAjax(this, "#JobseekerLanguages");',
                                            'style' => 'height: 100%; overflow-y: auto;'
                                            ]
                                    ]);
                                ?>
                                <div class="modal-header">
                                    <h5 class="modal-title" id="edit-language-modal-title--<?= $JobseekerLanguageTitle ?>"><?= Yii::t('jobseeker', 'Edit {language} language', ['language' => $JobseekerLanguage->i18nLanguage['english_name']]) ?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">

                                    <?php

                                    $editLanguageFormModel->language = $JobseekerLanguage->language;
                                    $editLanguageFormModel->proficiency = $JobseekerLanguage->proficiency;

                                    ?>

                                    <?= $editLanguageForm->field($editLanguageFormModel, 'language', ['addAriaAttributes' => true, 'enableClientValidation' => true, 'template' => "{input}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-language-form-select--'. $JobseekerLanguageTitle] ])
                                        ->hiddenInput()
                                    ?>

                                    <?= $editLanguageForm->field($editLanguageFormModel, 'proficiency', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'id' => 'add-language-form-proficiency-select--'. $JobseekerLanguageTitle] ])
                                        ->label(Yii::t('jobseeker', 'Proficiency'), ['class' => 'control-label font-weight-normal text-main-2'])
                                        ->widget(Select2::classname(), 
                                        [
                                            'data' => $SettingsSelectData->language_proficiency,
                                            'theme' => 'my-settings','pluginLoading' => false,
                                            'initValueText' => Yii::t('jobseeker', $SettingsSelectData->language_proficiency[$JobseekerLanguage->proficiency]),
                                            'options' => [
                                                'placeholder' => Yii::t('jobseeker', 'Proficiency'),
                                            ],                        
                                            'pluginLoading' => false,
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ]
                                        ])
                                    ?>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                                    <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Save') ?></button>
                                </div>
                                <?php 
                                    ActiveForm::end();
                                ?>
                            </div>
                        </div>
                    </div>
                    
                    <?php
                    
                        endforeach;
                    
                    ?>
                </div>
            
            <?php
            
                Pjax::end(); 
            
            else:
                Pjax::begin([ 'id' => 'JobseekerLanguages', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n4 pb-4']]); 
                
            ?>
                <div class="row mx-0"  id="jobseeker-languages-container">
                    <div class="container mb-2">
                        <h5 class="font-weight-normal">
                            <?= Yii::t('jobseeker', 'Languages') ?>:
                            <span class="profile-control-buttons-container">
                                <div class="profile-control-button-block" data-toggle="modal" data-target="#add-language-modal">
                                    <button class="profile-control-text-button ml-2 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Add language') ?>">
                                        <i class="fas fa-plus"></i>
                                        &nbsp;<?= Yii::t('jobseeker', 'Add new') ?>
                                    </button>   
                                </div> 
                            </span>
                        </h5>
                    </div>    
                    <div class="container px-3 py-3 mx-3 bg-light">
                        <h6 class="text-secondary font-weight-normal mb-0">
                            <?= Yii::t('jobseeker', 'No languages added yet') ?>
                        </h6>
                    </div>
                </div>
            
            <?php
            
                Pjax::end(); 
            endif;
                
            ?>
                <hr class="bt-1 mx-n4">

            <?php
                
            if(!empty($JobseekerEducation)):
                    
                Pjax::begin([ 'id' => 'JobseekerEducation', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n4 pb-4']]); 
                
            ?>
                
                <div class="row mx-0" id="profile-education-container">
                    <div class="container mb-2">
                        <h5 class="font-weight-normal">
                            <?= Yii::t('jobseeker', 'Education') ?>:
                            <span class="profile-control-buttons-container">
                                <div class="profile-control-button-block" data-toggle="modal" data-target="#add-education-modal">
                                    <button class="profile-control-text-button ml-2 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Add education') ?>">
                                        <i class="fas fa-plus"></i>
                                        &nbsp;<?= Yii::t('jobseeker', 'Add new') ?>
                                    </button>   
                                </div> 
                            </span>
                        </h5>
                    </div>
                    
                    <?php

                        foreach ($JobseekerEducation as $JobseekerEducationItem):
                            $JobseekerEducationTitle = md5($JobseekerEducationItem->educational_institution);
                    ?>
                    
                    <div class="container mb-2 px-0 profile-education-institution">
                        <div class="col col-education-institution-content px-3 mb-2 py-2" data-toggle="collapse" data-target="#education-institution--<?= $JobseekerEducationTitle ?>" aria-expanded="true">
                            <p class="text-secondary mb-0 mt-2" style="font-size: 13px;"><?= date('d.m.Y', strtotime($JobseekerEducationItem->education_start_date)) ?> - <?= date('d.m.Y', strtotime($JobseekerEducationItem->education_end_date)) ?></p>
                            <h5 class="mb-0">
                                <?= $JobseekerEducationItem->educational_institution ?>
                                <span class="h6 text-secondary font-weight-normal ml-2">
                                    <i class="fas fa-map-marker-alt mr-1"></i>
                                    <?= $SettingsSelectData->countries[$JobseekerEducationItem->country] ?> 
                                </span>
                            </h5>
                            <p><?= $JobseekerEducationItem->specialization ?><span class="text-secondary"> — <?= $SettingsSelectData->education_levels[$JobseekerEducationItem->education_level] ?></span></p>
                            <div class="profile-education-institution-info word-break text-pre-line mt-n2 mb-2 collapse <?= (!empty($JobseekerEducationItem->description)) ?: 'd-none' ?>" data-parent="#profile-education-container" id="education-institution--<?= $JobseekerEducationTitle ?>" style=""><?= $JobseekerEducationItem->description ?></div>
                            <span class="profile-control-buttons-container mr-3 mt-3"  style="top: 0;right: 0;">
                                <div class="profile-control-button-block d-inline-block" data-toggle="modal" data-target="#edit-education-modal--<?= $JobseekerEducationTitle ?>">
                                    <button class="profile-control-button" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Edit education') ?>">
                                        <i class="fas fa-pencil"></i>
                                    </button>                                     
                                </div>
                                <div class="profile-control-button-block d-inline-block" >
                                    <?= Html::button('<i class="fas fa-trash"></i>', ['class' => 'profile-control-button', 'style' => 'top:-3px;', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-original-title' => Yii::t('jobseeker', 'Remove education'), 'onclick' => 
                                       '$("#$JobseekerEducation").toggleClass("loading");
                                        $.ajax({
                                            type: "POST",
                                            url: "",
                                            data: {
                                                action: "JobseekerEducationRemove",
                                                education_id: "'.$JobseekerEducationItem->id.'"
                                            }, success: function(response) {
                                                if(response) {
                                                    pjaxContainerReload("#$JobseekerEducation");
                                                }
                                            }
                                        });'
                                    ]) ?>
                                </div>
                            </span>
                        </div>
                    </div>
                    
                    <div class="modal fade" id="edit-education-modal--<?= $JobseekerEducationTitle ?>" tabindex="-1" aria-labelledby="edit-education-modal-title--<?= $JobseekerEducationTitle ?>" aria-hidden="true" role="dialog">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content br-2 b-none">
                                <?php 
                                    $editEducationForm = ActiveForm::begin([
                                        'id' => 'edit-education-form--' . $JobseekerEducationTitle,
                                        'options' => [
                                            'onsubmit' => 'return submitFormAjax(this, "#JobseekerEducation");',
                                            'style' => 'height: 100%; overflow-y: auto;'
                                            ]
                                    ]);
                                ?>
                                <div class="modal-header">
                                    <h5 class="modal-title" id="add-education-modal-title--<?= $JobseekerEducationTitle ?>"><?= Yii::t('jobseeker', 'Add education') ?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <?php $editEducationFormModel->education_level = $JobseekerEducationItem->education_level ?>
                                    
                                    <?= $editEducationForm->field($editEducationFormModel, 'education_level', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-education-form-education-level-select--' . $JobseekerEducationTitle] ])->label(Yii::t('jobseeker', 'What is your education level?'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                                        [
                                            'data' => $SettingsSelectData->education_levels,
                                            'theme' => 'my-settings','pluginLoading' => false,
                                            'initValueText' => $SettingsSelectData->education_levels[$JobseekerEducationItem->education_level],
                                            'options' => ['placeholder' => Yii::t('jobseeker', 'Select your education level')],
                                            'pluginLoading' => false,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                            ]
                                        ])
                                    ?>
                                    
                                    <?php $editEducationFormModel->country = $JobseekerEducationItem->country; ?>

                                    <?= $editEducationForm->field($editEducationFormModel, 'country', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-education-form-country-select--' . $JobseekerEducationTitle] ])->label(Yii::t('jobseeker', 'Country'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                                        [
                                            'theme' => 'my-settings','pluginLoading' => false,
                                            'options' => ['placeholder' => Yii::t('jobseeker', 'Country')],
                                            'initValueText' => $SettingsSelectData->countries[$JobseekerEducationItem->country],
                                            'pluginLoading' => false,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'minimumInputLength' => 1,
                                                'language' => [
                                                    'errorLoading' => new JsExpression("function () { return '" . Yii::t('jobseeker', 'Waiting for results...') . "'; }"),
                                                ],
                                                'ajax' => [
                                                    'url' => Yii::getAlias('@web') . '/api/v1/geodb/countries',
                                                    'dataType' => 'json',
                                                    'delay' => 250,
                                                    'data' => new JsExpression('function(params) { return {namePrefix:params.term}; }'),
                                                ],
                                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                                                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                                            ],
                                            'pluginEvents' => [
                                                'select2:select' => new JsExpression('function(e){ $("#edit-education-form-educational-institution-select--' . $JobseekerEducationTitle . '").prop("disabled", ((e.params._type == "select") ? false : true));}'),
                                                'select2:clear' => new JsExpression('function(e){ $("#edit-education-form-educational-institution-select--' . $JobseekerEducationTitle . '").prop("disabled", ((e.params._type == "select") ? false : true));}')
                                            ]
                                        ])
                                    ?>  
                                    
                                    <?php $editEducationFormModel->educational_institution = $JobseekerEducationItem->educational_institution; ?>

                                    <?= $editEducationForm->field($editEducationFormModel, 'educational_institution', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-education-form-educational-institution-select--' . $JobseekerEducationTitle] ])->label(Yii::t('jobseeker', 'Educational Institution'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                                        [
                                            'theme' => 'my-settings','pluginLoading' => false,
                                            'options' => ['placeholder' => Yii::t('jobseeker', 'Educational institution')],
                                            'pluginLoading' => false,
                                            'maintainOrder' => true,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'minimumInputLength' => 1,
                                                'language' => [
                                                    'errorLoading' => new JsExpression("function () { return '" . Yii::t('jobseeker', 'Waiting for results...') . "'; }"),
                                                ],
                                                'ajax' => [
                                                    'url' => Yii::getAlias('@web') . '/api/v1/universities/country',
                                                    'dataType' => 'json',
                                                    'data' => new JsExpression('function(params) { return {countryCode:$("#edit-education-form-country-select--' . $JobseekerEducationTitle . '").val().toLowerCase(), namePrefix:params.term}; }'),
                                                ],
                                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                                'templateResult' => new JsExpression('function(university) { return university.text; }'),
                                                'templateSelection' => new JsExpression('function (university) { return university.text; }'),
                                            ]
                                        ])
                                    ?>  

                                    <?php $editEducationFormModel->specialization = $JobseekerEducationItem->specialization; ?>
                                    
                                    <?= $editEducationForm
                                        ->field($editEducationFormModel, 'specialization', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-education-form-specialization-select--' . $JobseekerEducationTitle, 'placeholder' => Yii::t('jobseeker', 'e.g. Finance')] ])
                                        ->label(Yii::t('jobseeker', 'Specialization title') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label label-popover font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                                        ->hint(Yii::t('jobseeker', 'Enter your specialization title'), ['class' => 'hint-block text-secondary']) ?>


                                    <?php $editEducationFormModel->education_start_date_month = date('m', strtotime($JobseekerEducationItem->education_start_date)); ?>
                                    <?php $editEducationFormModel->education_start_date_year = date('Y', strtotime($JobseekerEducationItem->education_start_date)); ?>
                                    
                                    <div class="col-9 col-md-6 float-left pr-3 pl-0">
                                        <label class="control-label label-popover font-weight-normal text-main-2 w-100"><?= Yii::t('jobseeker', 'Education start date') ?></label>
                                        <div class="form-group field-edit-education-form--<?= $JobseekerEducationTitle ?>-education-start-date-month col-6 pr-1 pl-0 float-left"> 
                                        <?= $editEducationForm->field($editEducationFormModel, 'education_start_date_month', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-education-form-education-start-date-month--' . $JobseekerEducationTitle] ])
                                        ->widget(Select2::classname(), 
                                            [
                                                'data' => $SettingsSelectData->months,
                                                'theme' => 'my-settings','pluginLoading' => false,
                                                'options' => ['placeholder' => Yii::t('jobseeker', 'Month')],
                                                'pluginLoading' => false,
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                ]
                                            ])
                                        ?>
                                        </div>

                                    
                                        
                                        <div class="form-group field-edit-education-form--<?= $JobseekerEducationTitle ?>-education-start-date-year col-6 pr-1 pl-0 float-left">
                                        <?= $editEducationForm->field($editEducationFormModel, 'education_start_date_year', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-education-form-education-start-date-year--' . $JobseekerEducationTitle]])
                                            ->widget(Select2::classname(), 
                                            [
                                                'data' => $SettingsSelectData->years,
                                                'theme' => 'my-settings','pluginLoading' => false,
                                                'options' => ['placeholder' => Yii::t('jobseeker', 'Year')],
                                                'pluginLoading' => false,
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                ]
                                            ])
                                        ?>
                                        </div>
                                    </div>
                                    
                                    <?php $editEducationFormModel->education_end_date_month = date('m', strtotime($JobseekerEducationItem->education_end_date)); ?>
                                    <?php $editEducationFormModel->education_end_date_year = date('Y', strtotime($JobseekerEducationItem->education_end_date)); ?>

                                    <div class="col-9 col-md-6 float-left pr-3 pl-0">
                                        <label class="control-label label-popover font-weight-normal text-main-2 w-100"><?= Yii::t('jobseeker', 'Education end date') ?></label>
                                        <div class="form-group field-edit-education-form--<?= $JobseekerEducationTitle ?>-education-end-date-month col-6 pr-1 pl-0 float-left"> 
                                        <?= $editEducationForm->field($editEducationFormModel, 'education_end_date_month', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-education-form-education-end-date-month--' . $JobseekerEducationTitle] ])
                                            ->widget(Select2::classname(), 
                                            [
                                                'data' => $SettingsSelectData->months,
                                                'theme' => 'my-settings','pluginLoading' => false,
                                                'options' => ['placeholder' => Yii::t('jobseeker', 'Month')],
                                                'pluginLoading' => false,
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                ]
                                            ])
                                        ?>
                                        </div>
                                        <div class="form-group field-edit-education-form--<?= $JobseekerEducationTitle ?>-education-end-date-year col-6 pr-0 pl-0 float-left">
                                        <?= $editEducationForm->field($editEducationFormModel, 'education_end_date_year', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-education-form-education-end-date-year--' . $JobseekerEducationTitle]])
                                            ->widget(Select2::classname(), 
                                            [
                                                'data' => $SettingsSelectData->years,
                                                'theme' => 'my-settings','pluginLoading' => false,
                                                'options' => ['placeholder' => Yii::t('jobseeker', 'Year')],
                                                'pluginLoading' => false,
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                ]
                                            ])
                                        ?>
                                        </div>
                                    </div>
                                    
                                    <?php $editEducationFormModel->description = $JobseekerEducationItem->description; ?>

                                    <div class="form-group field-edit-education-form--<?= $JobseekerEducationTitle ?>-description col-12 px-0 float-left">
                                    <?= $editEducationForm
                                        ->field($editEducationFormModel, 'description', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-education-form-description--' . $JobseekerEducationTitle] ])
                                        ->label(Yii::t('jobseeker', 'Pitch') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                                        ->hint(Yii::t('jobseeker', 'Describe briefly and succinctly your education.') . '<ul class="pl-3 mb-0"><li>' . Yii::t('jobseeker', 'List the main things you learned') . '</li><li>' . Yii::t('jobseeker', 'Highlight the main subjects' . '</li><li>' . Yii::t('jobseeker', 'Keep it short and informative').'</li></ul>'), ['class' => 'hint-block text-secondary'])
                                        ->textarea([])?> 
                                    </div>
                                    
                                    <?=
                                        $editEducationForm->field($editEducationFormModel, 'education_id')
                                            ->hiddenInput(['value'=> $JobseekerEducationItem->id])
                                            ->label(false) ?>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                                    <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Save') ?></button>
                                </div>
                                <?php 
                                    ActiveForm::end();
                                ?>
                            </div>
                        </div>
                    </div>

                    <?php
                    
                        endforeach;
                    
                    
                    ?>
                   
                </div>
                
            <?php
            
                Pjax::end(); 
            
            else:
                Pjax::begin([ 'id' => 'JobseekerEducation', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n4 pb-4']]); 
            
            ?>
                <div class="row mx-0" id="profile-education-container">
                    <div class="container mb-2">
                        <h5 class="font-weight-normal">
                            <?= Yii::t('jobseeker', 'Education') ?>:
                            <span class="profile-control-buttons-container">
                                <div class="profile-control-button-block" data-toggle="modal" data-target="#add-education-modal">
                                    <button class="profile-control-text-button ml-2 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Add education') ?>">
                                        <i class="fas fa-plus"></i>
                                        &nbsp;<?= Yii::t('jobseeker', 'Add new') ?>
                                    </button>   
                                </div> 
                            </span>
                        </h5>
                    </div>
                    
                    
                    <div class="container px-3 py-3 mx-3 bg-light">
                        <h6 class="text-secondary font-weight-normal mb-0">
                            <?= Yii::t('jobseeker', 'No education added yet') ?>
                        </h6>
                    </div>
                </div>
                
                

            <?php
            
                Pjax::end(); 
            endif;
            
            ?>
                <hr class="bt-1 mx-n4">
                
            <?php
                
            if(!empty($JobseekerAdditionalEducation)):
                    
                Pjax::begin([ 'id' => 'JobseekerAdditionalEducation', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n4 pb-4']]); 
                
            ?>
                
                <div class="row mx-0" id="profile-additional-education-container">
                    <div class="container mb-2">
                        <h5 class="font-weight-normal">
                            <?= Yii::t('jobseeker', 'Additional education') ?>:
                            <span class="profile-control-buttons-container">
                                <div class="profile-control-button-block" data-toggle="modal" data-target="#add-additional-education-modal">
                                    <button class="profile-control-text-button ml-2 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Add additional education') ?>">
                                        <i class="fas fa-plus"></i>
                                        &nbsp;<?= Yii::t('jobseeker', 'Add new') ?>
                                    </button>   
                                </div> 
                            </span>
                        </h5>
                    </div>
                    
                    <?php

                        foreach ($JobseekerAdditionalEducation as $JobseekerAdditionalEducationItem):
                            $JobseekerAdditionalEducationItemTitle = md5($JobseekerAdditionalEducationItem->educational_institution);
                    ?>
                    
                    <div class="container mb-2 px-0 profile-education-course">
                        <div class="col col-education-course-content px-3 mb-2 py-2" data-toggle="collapse" data-target="#education-course--<?= $JobseekerAdditionalEducationItemTitle ?>" aria-expanded="true">
                            <p class="text-secondary mb-0 mt-2" style="font-size: 13px;"><?= $JobseekerAdditionalEducationItem->education_start_date ?> - <?= $JobseekerAdditionalEducationItem->education_end_date ?></p>
                            <h5 class="mb-0">
                                <?= $JobseekerAdditionalEducationItem->educational_institution ?>
                                <span class="h6 text-secondary font-weight-normal ml-2">
                                    <i class="fas fa-map-marker-alt mr-1"></i>
                                    <?= $SettingsSelectData->countries[$JobseekerAdditionalEducationItem->country] ?>
                                </span>
                            </h5>
                            <p><?= $JobseekerAdditionalEducationItem->specialization ?></p>
                            <div class="profile-education-course-info word-break text-pre-line mt-n2 mb-2 collapse  <?= (!empty($JobseekerAdditionalEducationItem->description)) ?: 'd-none' ?>" data-parent="#profile-additional-education-container" id="education-course--<?= $JobseekerAdditionalEducationItemTitle ?>" style=""><?= $JobseekerAdditionalEducationItem->description ?></div>
                            <span class="profile-control-buttons-container mr-3 mt-3"  style="top: 0;right: 0;">
                                <div class="profile-control-button-block d-inline-block" data-toggle="modal" data-target="#edit-additional-education-modal--<?= $JobseekerAdditionalEducationItemTitle ?>">
                                    <button class="profile-control-button" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Edit additional education') ?>">
                                        <i class="fas fa-pencil"></i>
                                    </button>                                     
                                </div>
                                <div class="profile-control-button-block d-inline-block" >
                                    <?= Html::button('<i class="fas fa-trash"></i>', ['class' => 'profile-control-button', 'style' => 'top:-3px;', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-original-title' => Yii::t('jobseeker', 'Remove education'), 'onclick' => 
                                       '$("#JobseekerAdditionalEducation").toggleClass("loading");
                                        $.ajax({
                                            type: "POST",
                                            url: "",
                                            data: {
                                                action: "JobseekerAdditionalEducationRemove",
                                                additional_education_id: "'.$JobseekerAdditionalEducationItem->id.'"
                                            }, success: function(response) {
                                                if(response) {
                                                    pjaxContainerReload("#JobseekerAdditionalEducation");
                                                }
                                            }
                                        });'
                                    ]) ?>
                                </div>
                            </span>
                        </div>
                    </div>
                    
                    <div class="modal fade" id="edit-additional-education-modal--<?= $JobseekerAdditionalEducationItemTitle ?>" tabindex="-1" aria-labelledby="edit-additional-education-modal-title--<?= $JobseekerAdditionalEducationItemTitle ?>" aria-hidden="true" role="dialog">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content br-2 b-none">
                                <?php 
                                    $editAdditionalEducationForm = ActiveForm::begin([
                                        'id' => 'edit-additional-education-form--' . $JobseekerAdditionalEducationItemTitle,
                                        'options' => [
                                            'onsubmit' => 'return submitFormAjax(this, "#JobseekerAdditionalEducation");',
                                            'style' => 'height: 100%; overflow-y: auto;'
                                            ]
                                    ]);
                                ?>
                                <div class="modal-header">
                                    <h5 class="modal-title" id="edit-additional-education-modal-title--<?= $JobseekerAdditionalEducationItemTitle ?>"><?= Yii::t('jobseeker', 'Add additional education') ?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">

                                    <?php $editAdditionalEducationFormModel->educational_institution = $JobseekerAdditionalEducationItem->educational_institution ?>
                                    
                                    <?= $editAdditionalEducationForm
                                        ->field($editAdditionalEducationFormModel, 'educational_institution', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-education-form-educational-institution--'. $JobseekerAdditionalEducationItemTitle, 'placeholder' => Yii::t('jobseeker', 'e.g. Design Academy London')] ])
                                        ->label(Yii::t('jobseeker', 'Educational Institution') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label label-popover font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                                        ->hint(Yii::t('jobseeker', 'Enter the name of school, academy, college etc.'), ['class' => 'hint-block text-secondary']) ?>  

                                    <?php $editAdditionalEducationFormModel->specialization = $JobseekerAdditionalEducationItem->specialization ?>
                                    
                                    <?= $editAdditionalEducationForm
                                        ->field($editAdditionalEducationFormModel, 'specialization', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-additional-education-form-specialization--' . $JobseekerAdditionalEducationItemTitle, 'placeholder' => Yii::t('jobseeker', 'e.g. Web-Design')] ])
                                        ->label(Yii::t('jobseeker', 'Area of study') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label label-popover font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                                        ->hint(Yii::t('jobseeker', 'Enter the title of the course you\'ve attended'), ['class' => 'hint-block text-secondary']) ?>

                                    <?php $editAdditionalEducationFormModel->country = $JobseekerAdditionalEducationItem->country ?>
                                    
                                    <?= $editAdditionalEducationForm->field($editAdditionalEducationFormModel, 'country', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-additional-education-form-country-select--' . $JobseekerAdditionalEducationItemTitle] ])->label(Yii::t('jobseeker', 'Country'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                                        [
                                            'theme' => 'my-settings','pluginLoading' => false,
                                            'options' => ['placeholder' => Yii::t('jobseeker', 'Country')],
                                            'initValueText' => $SettingsSelectData->countries[$JobseekerAdditionalEducationItem->country],
                                            'pluginLoading' => false,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'minimumInputLength' => 1,
                                                'language' => [
                                                    'errorLoading' => new JsExpression("function () { return '" . Yii::t('jobseeker', 'Waiting for results...') . "'; }"),
                                                ],
                                                'ajax' => [
                                                    'url' => Yii::getAlias('@web') . '/api/v1/geodb/countries',
                                                    'dataType' => 'json',
                                                    'delay' => 250,
                                                    'data' => new JsExpression('function(params) { return {namePrefix:params.term}; }'),
                                                ],
                                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                                                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                                            ]
                                        ])
                                    ?>
                                    
                                    <?php $editAdditionalEducationFormModel->education_start_date_month = date('m', strtotime($JobseekerAdditionalEducationItem->education_start_date)); ?>
                                    <?php $editAdditionalEducationFormModel->education_start_date_year = date('Y', strtotime($JobseekerAdditionalEducationItem->education_start_date)); ?>

                                    <div class="col-9 col-md-6 float-left pr-3 pl-0">
                                        <label class="control-label label-popover font-weight-normal text-main-2 w-100"><?= Yii::t('jobseeker', 'Education start date') ?></label>
                                        <div class="form-group field-edit-additional-education-form--<?= $JobseekerAdditionalEducationItemTitle ?>-education-start-date-month-select required col-6 pr-1 pl-0 float-left"> 
                                        <?= $editAdditionalEducationForm->field($editAdditionalEducationFormModel, 'education_start_date_month', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-additional-education-form-education-start-date-month--' . $JobseekerAdditionalEducationItemTitle] ])
                                        ->widget(Select2::classname(), 
                                            [
                                                'data' => $SettingsSelectData->months,
                                                'theme' => 'my-settings','pluginLoading' => false,
                                                'options' => ['placeholder' => Yii::t('jobseeker', 'Month')],
                                                'pluginLoading' => false,
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                ]
                                            ])
                                        ?>
                                        </div>
                                        <div class="form-group field-edit-additional-education-form--<?= $JobseekerAdditionalEducationItemTitle ?>-education-start-date-year-select required col-6 pr-1 pl-0 float-left">
                                        <?= $editAdditionalEducationForm->field($editAdditionalEducationFormModel, 'education_start_date_year', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-additional-education-form-education-start-date-year--' . $JobseekerAdditionalEducationItemTitle]])
                                            ->widget(Select2::classname(), 
                                            [
                                                'data' => $SettingsSelectData->years,
                                                'theme' => 'my-settings','pluginLoading' => false,
                                                'options' => ['placeholder' => Yii::t('jobseeker', 'Year')],
                                                'pluginLoading' => false,
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                ]
                                            ])
                                        ?>
                                        </div>
                                    </div>
                                    
                                    <?php $editAdditionalEducationFormModel->education_end_date_month = date('m', strtotime($JobseekerAdditionalEducationItem->education_end_date)); ?>
                                    <?php $editAdditionalEducationFormModel->education_end_date_year = date('Y', strtotime($JobseekerAdditionalEducationItem->education_end_date)); ?>

                                    <div class="col-9 col-md-6 float-left pr-3 pl-0">
                                        <label class="control-label label-popover font-weight-normal text-main-2 w-100"><?= Yii::t('jobseeker', 'Education end date') ?></label>
                                        <div class="form-group field-edit-additional-education-form--<?= $JobseekerAdditionalEducationItemTitle ?>-education-end-date-month-select required col-6 pr-1 pl-0 float-left"> 
                                        <?= $editAdditionalEducationForm->field($editAdditionalEducationFormModel, 'education_end_date_month', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-additional-education-form-education-end-date-month--' . $JobseekerAdditionalEducationItemTitle] ])
                                            ->widget(Select2::classname(), 
                                            [
                                                'data' => $SettingsSelectData->months,
                                                'theme' => 'my-settings','pluginLoading' => false,
                                                'options' => ['placeholder' => Yii::t('jobseeker', 'Month')],
                                                'pluginLoading' => false,
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                ]
                                            ])
                                        ?>
                                        </div>
                                        
                                        <div class="form-group field-edit-additional-education-form--<?= $JobseekerAdditionalEducationItemTitle ?>-education-end-date-year-select required col-6 pr-0 pl-0 float-left">
                                        <?= $editAdditionalEducationForm->field($editAdditionalEducationFormModel, 'education_end_date_year', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-additional-education-form-education-end-date-year--' . $JobseekerAdditionalEducationItemTitle]])
                                            ->widget(Select2::classname(), 
                                            [
                                                'data' => $SettingsSelectData->years,
                                                'theme' => 'my-settings','pluginLoading' => false,
                                                'options' => ['placeholder' => Yii::t('jobseeker', 'Year')],
                                                'pluginLoading' => false,
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                ]
                                            ])
                                        ?>
                                        </div>
                                    </div>
                                    
                                    <?php $editAdditionalEducationFormModel->description = $JobseekerAdditionalEducationItem->description; ?>

                                    <div class="form-group field-edit-additional-education-form--<?= $JobseekerAdditionalEducationItemTitle ?>-description col-12 px-0 float-left">
                                    <?= $editAdditionalEducationForm
                                        ->field($editAdditionalEducationFormModel, 'description', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-additional-education-form-description--' . $JobseekerAdditionalEducationItemTitle] ])
                                        ->label(Yii::t('jobseeker', 'Pitch') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                                        ->hint(Yii::t('jobseeker', 'Describe briefly and succinctly your education.') . '<ul class="pl-3 mb-0"><li>' . Yii::t('jobseeker', 'List the main things you learned') . '</li><li>' . Yii::t('jobseeker', 'Highlight the main subjects' . '</li><li>' . Yii::t('jobseeker', 'Keep it short and informative').'</li></ul>'), ['class' => 'hint-block text-secondary'])
                                        ->textarea([])?> 
                                    </div>
                                    
                                    <?=
                                        $editAdditionalEducationForm->field($editAdditionalEducationFormModel, 'additional_education_id')
                                            ->hiddenInput(['value'=> $JobseekerAdditionalEducationItem->id])
                                            ->label(false) ?>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                                    <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Save') ?></button>
                                </div>
                                <?php 
                                    ActiveForm::end();
                                ?>
                            </div>
                        </div>
                    </div>
                    
                    <?php
                    
                        endforeach;
                                        
                    ?>
                    
                </div>

            <?php
            
                Pjax::end(); 
            
            else:
                Pjax::begin([ 'id' => 'JobseekerAdditionalEducation', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n4 pb-4']]); 
            
            ?>
                
                <div class="row mx-0" id="profile-additional-education-container">
                    <div class="container mb-2">
                        <h5 class="font-weight-normal">
                            <?= Yii::t('jobseeker', 'Additional education') ?>:
                            <span class="profile-control-buttons-container">
                                <div class="profile-control-button-block" data-toggle="modal" data-target="#add-additional-education-modal">
                                    <button class="profile-control-text-button ml-2 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Add additional education') ?>">
                                        <i class="fas fa-plus"></i>
                                        &nbsp;<?= Yii::t('jobseeker', 'Add new') ?>
                                    </button>   
                                </div> 
                            </span>
                        </h5>
                    </div>
                    <div class="container px-3 py-3 mx-3 bg-light">
                        <h6 class="text-secondary font-weight-normal mb-0">
                            <?= Yii::t('jobseeker', 'No additional education added yet') ?>
                        </h6>
                    </div>
                </div>
                
            <?php
            
                Pjax::end(); 
            endif;

            ?>
                <hr class="bt-1 mx-n4">

            <?php
                
            if(!empty($JobseekerWorkExperience)):
                    
                Pjax::begin([ 'id' => 'JobseekerWorkExperience', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n4 pb-4']]); 
                
            ?>
                
                <div class="row mx-0" id="profile-work-experince-container">
                    <div class="container mb-2">
                        <h5 class="font-weight-normal">
                            <?= Yii::t('jobseeker', 'Employment history') ?>:
                            <span class="profile-control-buttons-container">
                                <div class="profile-control-button-block" data-toggle="modal" data-target="#add-work-experience-modal">
                                    <button class="profile-control-text-button ml-2 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Add work experience') ?>">
                                        <i class="fas fa-plus"></i>
                                        &nbsp;<?= Yii::t('jobseeker', 'Add new') ?>
                                    </button>   
                                </div> 
                            </span>
                        </h5>
                    </div>
                    
                    <?php

                        foreach ($JobseekerWorkExperience as $JobseekerWorkExperienceItem):
                            $JobseekerWorkExperienceItemTitle = md5($JobseekerWorkExperienceItem->company_name . $JobseekerWorkExperienceItem->id);
                    ?>
                    
                    <div class="container mb-2 px-0 profile-work-experience" style="background: none;">
                        <div class="col col-work-experience-content px-3 mb-2 py-2" data-toggle="collapse" data-target="#work-experince--<?= $JobseekerWorkExperienceItemTitle ?>" aria-expanded="true">
                            <p class="text-secondary mb-0 mt-2" style="font-size: 13px;"><?= $JobseekerWorkExperienceItem->work_start_date ?> - <?= $JobseekerWorkExperienceItem->work_end_date ?></p>
                            <h5 class="mb-0">
                                <?= $JobseekerWorkExperienceItem->company_name ?> <span class="font-weight-normal text-secondary"> - <?= $SettingsSelectData->specialization[$JobseekerWorkExperienceItem->company_industry] ?></span>
                                <span class="h6 text-secondary font-weight-normal ml-2">
                                    <i class="fas fa-map-marker-alt mr-1"></i>
                                    <?= $SettingsSelectData->countries[$JobseekerWorkExperienceItem->country] ?>
                                </span>
                            </h5>
                            <p><?= $JobseekerWorkExperienceItem->job_title ?></p>
                            <div class="profile-work-experience-info word-break text-pre-line mt-n2 mb-2 collapse <?= (!empty($JobseekerAdditionalEducationItem->description)) ?: 'd-none' ?>" data-parent="#profile-work-experince-container" id="work-experince--<?= $JobseekerWorkExperienceItemTitle ?>" style=""><?= $JobseekerWorkExperienceItem->description ?></div>
                            <span class="profile-control-buttons-container mr-3 mt-3"  style="top: 0;right: 0;">
                                <div class="profile-control-button-block d-inline-block" data-toggle="modal" data-target="#edit-work-experience-modal--<?= $JobseekerWorkExperienceItemTitle ?>">
                                    <button class="profile-control-button" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Edit work experience') ?>">
                                        <i class="fas fa-pencil"></i>
                                    </button>                                     
                                </div>
                                <div class="profile-control-button-block d-inline-block" >
                                    <?= Html::button('<i class="fas fa-trash"></i>', ['class' => 'profile-control-button', 'style' => 'top:-3px;', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-original-title' => Yii::t('jobseeker', 'Remove work experience'), 'onclick' => 
                                       '$("#JobseekerWorkExperience").toggleClass("loading");
                                        $.ajax({
                                            type: "POST",
                                            url: "",
                                            data: {
                                                action: "JobseekerWorkExperienceRemove",
                                                work_experience_id: "'.$JobseekerWorkExperienceItem->id.'"
                                            }, success: function(response) {
                                                if(response) {
                                                    pjaxContainerReload("#JobseekerWorkExperience");
                                                }
                                            }
                                        });'
                                    ]) ?>
                                </div>
                            </span>
                        </div>
                    </div>
                    
                    <div class="modal fade" id="edit-work-experience-modal--<?= $JobseekerWorkExperienceItemTitle ?>" tabindex="-1" aria-labelledby="edit-work-experience-modal-title--<?= $JobseekerWorkExperienceItemTitle ?>" aria-hidden="true" role="dialog">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content br-2 b-none">
                                <?php 
                                    $editWorkExperienceForm = ActiveForm::begin([
                                        'id' => 'add-work-experience-form--' . $JobseekerWorkExperienceItemTitle,
                                        'options' => [
                                            'onsubmit' => 'return submitFormAjax(this, "#JobseekerWorkExperience");',
                                            'style' => 'height: 100%; overflow-y: auto;'
                                            ]
                                    ]);
                                ?>
                                <div class="modal-header">
                                    <h5 class="modal-title" id="add-additional-education-modal-title"><?= Yii::t('jobseeker', 'Add work experience') ?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <?php $editWorkExperienceFormModel->company_name = $JobseekerWorkExperienceItem->company_name; ?>
                                        
                                    <?= $editWorkExperienceForm
                                        ->field($editWorkExperienceFormModel, 'company_name', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'inputOptions' => ['class' => 'form-control', 'id' => 'add-work-experience-form-company-name--' . $JobseekerWorkExperienceItemTitle, 'placeholder' => Yii::t('jobseeker', 'e.g. Example Gmbh')] ])
                                        ->label(Yii::t('jobseeker', 'Company name') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label label-popover font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                                        ->hint(Yii::t('jobseeker', 'Enter the name of the company you worked for.'), ['class' => 'hint-block text-secondary']) ?>  

                                    <?php $editWorkExperienceFormModel->company_industry = $JobseekerWorkExperienceItem->company_industry; ?>
                                    
                                    <?= $editWorkExperienceForm->field($editWorkExperienceFormModel, 'company_industry', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'id' => 'add-work-experience-form-company-industry-select--' . $JobseekerWorkExperienceItemTitle] ])->label(Yii::t('jobseeker', 'Comapany industry'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                                        [
                                            'data' => $SettingsSelectData->specialization,
                                            'initValueText' => $SettingsSelectData->specialization[$JobseekerWorkExperienceItem->company_industry],
                                            'theme' => 'my-settings','pluginLoading' => false,
                                            'options' => [
                                                'placeholder' => Yii::t('jobseeker', 'Select company industry')
                                            ],
                                            'pluginLoading' => false,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                            ]
                                        ])
                                    ?>
                                    
                                    <?php $editWorkExperienceFormModel->job_title = $JobseekerWorkExperienceItem->job_title; ?>

                                    <?= $editWorkExperienceForm
                                        ->field($editWorkExperienceFormModel, 'job_title', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'inputOptions' => ['class' => 'form-control', 'id' => 'add-work-experience-form-job-title--' . $JobseekerWorkExperienceItemTitle, 'placeholder' => Yii::t('jobseeker', 'Senior Web Developer')] ])
                                        ->label(Yii::t('jobseeker', 'Job title') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label label-popover font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.']) ?>  

                                    <?php $editWorkExperienceFormModel->country = $JobseekerWorkExperienceItem->country; ?>
                                    
                                    <?= $editWorkExperienceForm->field($editWorkExperienceFormModel, 'country', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'id' => 'add-work-experience-form-country-select--' . $JobseekerWorkExperienceItemTitle] ])->label(Yii::t('jobseeker', 'Country'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                                        [
                                            'theme' => 'my-settings','pluginLoading' => false,
                                            'options' => ['placeholder' => Yii::t('jobseeker', 'Country')],
                                            'initValueText' => $SettingsSelectData->countries[$JobseekerWorkExperienceItem->country],
                                            'pluginLoading' => false,
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'minimumInputLength' => 1,
                                                'language' => [
                                                    'errorLoading' => new JsExpression("function () { return '" . Yii::t('jobseeker', 'Waiting for results...') . "'; }"),
                                                ],
                                                'ajax' => [
                                                    'url' => Yii::getAlias('@web') . '/api/v1/geodb/countries',
                                                    'dataType' => 'json',
                                                    'delay' => 250,
                                                    'data' => new JsExpression('function(params) { return {namePrefix:params.term}; }'),
                                                ],
                                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                                                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                                            ]
                                        ])
                                    ?>
                                    
                                    <?php $editWorkExperienceFormModel->work_start_date_month = date('m', strtotime($JobseekerWorkExperienceItem->work_start_date)); ?>
                                    <?php $editWorkExperienceFormModel->work_start_date_year = date('Y', strtotime($JobseekerWorkExperienceItem->work_start_date)); ?>

                                    <div class="col-9 col-md-6 float-left pr-3 pl-0">
                                        <label class="control-label label-popover font-weight-normal text-main-2 w-100"><?= Yii::t('jobseeker', 'Work begin date') ?></label>
                                        <div class="form-group field-edit-work-experience-form--<?= $JobseekerWorkExperienceItemTitle ?>-education-start-date-month col-6 pr-1 pl-0 float-left"> 
                                        <?= $editWorkExperienceForm->field($editWorkExperienceFormModel, 'work_start_date_month', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-work-experience-form-work-start-date-month--' . $JobseekerWorkExperienceItemTitle] ])
                                        ->widget(Select2::classname(), 
                                            [
                                                'data' => $SettingsSelectData->months,
                                                'theme' => 'my-settings','pluginLoading' => false,
                                                'options' => ['placeholder' => Yii::t('jobseeker', 'Month')],
                                                'pluginLoading' => false,
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                ]
                                            ])
                                        ?>
                                        </div>

                                        <div class="form-group field-edit-work-experience-form--<?= $JobseekerWorkExperienceItemTitle ?>-education-start-date-year col-6 pr-1 pl-0 float-left">
                                        <?= $editWorkExperienceForm->field($editWorkExperienceFormModel, 'work_start_date_year', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-work-experience-form-work-start-date-year--' . $JobseekerWorkExperienceItemTitle]])
                                            ->widget(Select2::classname(), 
                                            [
                                                'data' => $SettingsSelectData->years,
                                                'theme' => 'my-settings','pluginLoading' => false,
                                                'options' => ['placeholder' => Yii::t('jobseeker', 'Year')],
                                                'pluginLoading' => false,
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                ]
                                            ])
                                        ?>
                                        </div>
                                    </div>
                                    
                                    <?php $editWorkExperienceFormModel->work_end_date_month = date('m', strtotime($JobseekerWorkExperienceItem->work_end_date)); ?>
                                    <?php $editWorkExperienceFormModel->work_end_date_year = date('Y', strtotime($JobseekerWorkExperienceItem->work_end_date)); ?>

                                    <div class="col-9 col-md-6 float-left pr-3 pl-0">
                                        <label class="control-label label-popover font-weight-normal text-main-2 w-100"><?= Yii::t('jobseeker', 'Work end date') ?></label>
                                        <div class="form-group field-edit-work-experience-form--<?= $JobseekerWorkExperienceItemTitle ?>-education-end-date-month col-6 pr-1 pl-0 float-left"> 
                                        <?= $editWorkExperienceForm->field($editWorkExperienceFormModel, 'work_end_date_month', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-work-experience-form-work-end-date-month--' . $JobseekerWorkExperienceItemTitle] ])
                                            ->widget(Select2::classname(), 
                                            [
                                                'data' => $SettingsSelectData->months,
                                                'theme' => 'my-settings','pluginLoading' => false,
                                                'options' => ['placeholder' => Yii::t('jobseeker', 'Month')],
                                                'pluginLoading' => false,
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                ]
                                            ])
                                        ?>
                                        </div>

                                        <div class="form-group field-edit-work-experience-form--<?= $JobseekerWorkExperienceItemTitle ?>-education-end-date-year col-6 pr-0 pl-0 float-left">
                                        <?= $editWorkExperienceForm->field($editWorkExperienceFormModel, 'work_end_date_year', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-work-experience-form-work-end-date-year--' . $JobseekerWorkExperienceItemTitle]])
                                            ->widget(Select2::classname(), 
                                            [
                                                'data' => $SettingsSelectData->years,
                                                'theme' => 'my-settings','pluginLoading' => false,
                                                'options' => ['placeholder' => Yii::t('jobseeker', 'Year')],
                                                'pluginLoading' => false,
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                ]
                                            ])
                                        ?>
                                        </div>


                                    </div>
                                    
                                    <?php $editWorkExperienceFormModel->description = $JobseekerWorkExperienceItem->description; ?>

                                    <div class="form-group field-add-additional-education-form-description col-12 px-0 float-left">
                                    <?= $editWorkExperienceForm
                                        ->field($editWorkExperienceFormModel, 'description', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-additional-education-form-description--' . $JobseekerWorkExperienceItemTitle] ])
                                        ->label(Yii::t('jobseeker', 'Descritpion') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                                        ->hint(Yii::t('jobseeker', 'Describe briefly and succinctly your work.') . '<ul class="pl-3 mb-0"><li>' . Yii::t('jobseeker', 'List the main things you learned') . '</li><li>' . Yii::t('jobseeker', 'Highlight the main subjects' . '</li><li>' . Yii::t('jobseeker', 'Keep it short and informative').'</li></ul>'), ['class' => 'hint-block text-secondary'])
                                        ->textarea([])?> 
                                    </div>
                                    
                                    <?=
                                        $editWorkExperienceForm->field($editWorkExperienceFormModel, 'work_experience_id')
                                            ->hiddenInput(['value'=> $JobseekerWorkExperienceItem->id])
                                            ->label(false) ?>


                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                                    <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Save') ?></button>
                                </div>
                                <?php 
                                    ActiveForm::end();
                                ?>
                            </div>
                        </div>
                    </div>
                    
            <?php

                endforeach;
             
            ?>
                    
                </div>
                
            <?php
            
                Pjax::end(); 
            
            else:
                Pjax::begin([ 'id' => 'JobseekerWorkExperience', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n4 pb-4']]); 
            ?>
                    
                    <div class="row mx-0" id="profile-work-experince-container">
                        <div class="container mb-2">
                            <h5 class="font-weight-normal">
                                <?= Yii::t('jobseeker', 'Employment history') ?>:
                                <span class="profile-control-buttons-container">
                                    <div class="profile-control-button-block" data-toggle="modal" data-target="#add-work-experience-modal">
                                        <button class="profile-control-text-button ml-2 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Add work experience') ?>">
                                            <i class="fas fa-plus"></i>
                                            &nbsp;<?= Yii::t('jobseeker', 'Add new') ?>
                                        </button>   
                                    </div> 
                                </span>
                            </h5>
                        </div>
                        <div class="container px-3 py-3 mx-3 bg-light">
                            <h6 class="text-secondary font-weight-normal mb-0">
                                <?= Yii::t('jobseeker', 'No work experience added yet') ?>
                            </h6>
                        </div>
                    </div>
            <?php

                Pjax::end(); 
            endif;

            ?>
                                    
                <hr class="bt-1 mx-n4">

            <?php
                
            if(!empty($JobseekerOtherExperience)):
                    
                Pjax::begin([ 'id' => 'JobseekerOtherExperience', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n4 pb-4']]); 
                
            ?>
                
                <div class="row mx-0" id="profile-other-experince-container">
                    <div class="container mb-2">
                        <h5 class="font-weight-normal">
                            <?= Yii::t('jobseeker', 'Other experience') ?>:
                            <span class="profile-control-buttons-container">
                                <div class="profile-control-button-block" data-toggle="modal" data-target="#add-other-experience-modal">
                                    <button class="profile-control-text-button ml-2 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Add other experience') ?>">
                                        <i class="fas fa-plus"></i>
                                        &nbsp;<?= Yii::t('jobseeker', 'Add new') ?>
                                    </button>   
                                </div> 
                            </span>
                        </h5>
                    </div>
                    
                <?php

                    foreach ($JobseekerOtherExperience as $JobseekerOtherExperienceItem):
                        $JobseekerOtherExperienceItemTitle = md5($JobseekerOtherExperienceItem->title);
                        
                ?>
                    
                    <div class="container mb-2 px-0 profile-other-experience">
                        <div class="col col-other-experience-content px-3 mb-2 py-2 pb-5">
                            <h5 class="mb-3 mt-2">
                                <?= $JobseekerOtherExperienceItem->title ?>
                            </h5>
                            <p class="profile-other-experience-info word-break text-pre-line" onclick="$(this).toggleClass('expanded');"><?= $JobseekerOtherExperienceItem->description ?></p>
                            <span class="profile-control-buttons-container mr-3 mt-3"  style="top: 0;right: 0;">
                                <div class="profile-control-button-block d-inline-block" data-toggle="modal" data-target="#edit-other-experience-modal--<?= $JobseekerOtherExperienceItemTitle ?>">
                                    <button class="profile-control-button" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Edit other experience') ?>">
                                        <i class="fas fa-pencil"></i>
                                    </button>                                     
                                </div>
                                <div class="profile-control-button-block d-inline-block" >
                                    <?= Html::button('<i class="fas fa-trash"></i>', ['class' => 'profile-control-button', 'style' => 'top:-3px;', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-original-title' => Yii::t('jobseeker', 'Remove other experience'), 'onclick' => 
                                       '$("#JobseekerOtherExperience").toggleClass("loading");
                                        $.ajax({
                                            type: "POST",
                                            url: "",
                                            data: {
                                                action: "JobseekerOtherExperienceRemove",
                                                other_experience_id: "'.$JobseekerOtherExperienceItem->id.'"
                                            }, success: function(response) {
                                                if(response) {
                                                    pjaxContainerReload("#JobseekerOtherExperience");
                                                }
                                            }
                                        });'
                                    ]) ?>
                                </div>
                            </span>
                        </div>
                    </div>
                    <div class="modal fade" id="edit-other-experience-modal--<?= $JobseekerOtherExperienceItemTitle ?>" tabindex="-1" aria-labelledby="edit-other-experience-modal-title--<?= $JobseekerOtherExperienceItemTitle ?>" aria-hidden="true" role="dialog">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content br-2 b-none">
                                <?php 
                                    $editOtherExperienceForm = ActiveForm::begin([
                                        'id' => 'edit-other-experience-form--' . $JobseekerOtherExperienceItemTitle,
                                        'options' => [
                                            'onsubmit' => 'return submitFormAjax(this, "#JobseekerOtherExperience");',
                                            'style' => 'height: 100%; overflow-y: auto;'
                                            ]
                                    ]);
                                ?>
                                <div class="modal-header">
                                    <h5 class="modal-title" id="edit-other-education-modal-title--<?= $JobseekerOtherExperienceItemTitle ?>"><?= Yii::t('jobseeker', 'Add other experience') ?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">

                                    <?php $editOtherExperienceFormModel->title = $JobseekerOtherExperienceItem->title; ?>
                                    
                                    <?= $editOtherExperienceForm
                                        ->field($editOtherExperienceFormModel, 'title', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-other-experience-form-title--' . $JobseekerOtherExperienceItemTitle, 'placeholder' => Yii::t('jobseeker', 'e.g. Example Gmbh')] ])
                                        ->label(Yii::t('jobseeker', 'Title') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label label-popover font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.']) ?>  

                                    <?php $editOtherExperienceFormModel->description = $JobseekerOtherExperienceItem->description; ?>
                                    
                                    <div class="form-group field-add-other-experience-form--<?= $JobseekerOtherExperienceItemTitle ?>-description col-12 px-0 float-left">
                                    <?= $editOtherExperienceForm
                                        ->field($editOtherExperienceFormModel, 'description', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-other-experience-form-description--' . $JobseekerOtherExperienceItemTitle] ])
                                        ->label(Yii::t('jobseeker', 'Descritpion') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                                        ->hint(Yii::t('jobseeker', 'Describe briefly and succinctly your experience.') . '<ul class="pl-3 mb-0"><li>' . Yii::t('jobseeker', 'List the main things you learned') . '</li><li>' . Yii::t('jobseeker', 'Highlight the main subjects' . '</li><li>' . Yii::t('jobseeker', 'Keep it short and informative').'</li></ul>'), ['class' => 'hint-block text-secondary'])
                                        ->textarea([])?> 
                                    </div>
                                    
                                    <?=
                                        $editOtherExperienceForm->field($editOtherExperienceFormModel, 'other_experience_id')
                                            ->hiddenInput(['value'=> $JobseekerOtherExperienceItem->id])
                                            ->label(false) ?>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                                    <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Add') ?></button>
                                </div>
                                <?php 
                                    ActiveForm::end();
                                ?>
                            </div>
                        </div>
                    </div>
                    
                <?php

                    endforeach;
                    
                ?>
                    
                </div>
                    
            <?php
            
                Pjax::end(); 
            
            else:
                Pjax::begin([ 'id' => 'JobseekerOtherExperience', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n4 pb-4']]); 
            
            ?>
                <div class="row mx-0" id="profile-other-experince-container">
                    <div class="container mb-2">
                        <h5 class="font-weight-normal">
                            <?= Yii::t('jobseeker', 'Other experience') ?>:
                            <span class="profile-control-buttons-container">
                                <div class="profile-control-button-block" data-toggle="modal" data-target="#add-other-experience-modal">
                                    <button class="profile-control-text-button ml-2 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= Yii::t('jobseeker', 'Add other experience') ?>">
                                        <i class="fas fa-plus"></i>
                                        &nbsp;<?= Yii::t('jobseeker', 'Add new') ?>
                                    </button>   
                                </div> 
                            </span>
                        </h5>
                    </div>
                    <div class="container px-3 py-3 mx-3 bg-light">
                        <h6 class="text-secondary font-weight-normal mb-0">
                            <?= Yii::t('jobseeker', 'No other experience added yet') ?>
                        </h6>
                    </div>
                </div>

            <?php
            
                Pjax::end(); 
            endif;
                
            ?>    
                
                <hr class="bt-1 mx-n4">

                <div class="row mx-0" id="profile-other-experince-container">
                    <div class="container mb-2">
                        <h5 class="font-weight-normal">
                            Testimonials:
                            <span class="profile-control-buttons-container">
                                <button class="profile-control-text-button ml-2 position-relative" style="top:-3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit employment type">
                                    <i class="fas fa-plus"></i>
                                    &nbsp;Add new
                                </button>  
                            </span>
                        </h5>
                    </div>

                    <div class="container mb-2 px-0 profile-other-experience-experience">
                        <div class="col px-3 mb-2 py-2 pb-5">
                            <h5 class="mb-3 mt-2">OSCHADBANK</h5>
                            <p class="profile-other-experience-info expanded" onclick="$(this).toggleClass('expanded');">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                            <span class="profile-control-buttons-container mr-3 mt-3" style="top: 0;right: 0;">
                                <button class="profile-control-button" style="height: 32px;width: 32px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit employment type">
                                    <i class="fas fa-pencil"></i>
                                </button> 

                                <button class="profile-control-button" style="height: 32px;width: 32px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit employment type">
                                    <i class="fas fa-trash"></i>
                                </button> 
                            </span>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
</div>



<?php

if(!empty($editVisibilityFormModel)):

?>

<div class="modal fade" id="edit-visibility-modal" tabindex="-1" aria-labelledby="edit-visibility-modal-title" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content br-2 b-none">
            <?php 
                $editVisibilityForm = ActiveForm::begin([
                    'id' => 'edit-visibility-form',
                    'options' => [
                        'onsubmit' => 'return submitFormAjax(this, "#Jobseeker");',
                        'style' => 'height: 100%; overflow-y: auto;'
                    ]
                ]);
            ?>
            <div class="modal-header">
                <h5 class="modal-title" id="edit-visibility-modal-title"><?= Yii::t('jobseeker', 'Edit profile visibility') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                
                <?php 
                
                    $editVisibilityFormModel->visibility = $Jobseeker->visibility;
                
                ?>
            
                <?= $editVisibilityForm->field($editVisibilityFormModel, 'visibility', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'id' => 'edit-visibility-form-select'] ])->label(Yii::t('jobseeker', 'Who can see your profile?'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                    [
                        'data' => $SettingsSelectData->profile_visibility,
                        'theme' => 'my-settings','pluginLoading' => false,
                        'pluginLoading' => false,
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ])
                ?>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Save') ?></button>
            </div>
            <?php 
                ActiveForm::end();
            ?>
        </div>
    </div>
</div>



<?php

endif;

if(!empty($editPhotoFormModel)):
    
?>

<div class="modal fade" id="edit-photo-modal" tabindex="-1" aria-labelledby="edit-photo-modal-title" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content br-2 b-none">
            <?php 
                Pjax::begin([ 'id' => 'EditPhotoForm', 'enablePushState' => false, 'options' => ['class' => 'mx-n4 px-4 mt-n3 pt-3 mb-n4 pb-4', 'style' => 'height: 100%; overflow-y: auto;']]); 
                
                if(!empty($Jobseeker->photo->path_cropped))
                {
                    $c = [
                        $Jobseeker->photo->crop_x1, $Jobseeker->photo->crop_y1, $Jobseeker->photo->crop_width, $Jobseeker->photo->crop_height 
                    ];
                }
                else
                {
                    $c = [0,0,200,200];
                }
                
                $editPhotoForm = ActiveForm::begin([
                    'id' => 'edit-photo-form',
                    'options' => [
                        'enctype' => 'multipart/form-data',
                        'onsubmit' => 'return updateProfilePhotoAjax(this, "#EditPhotoForm", "#Jobseeker");'
                    ]
                ]);
            ?>
            <div class="modal-header">
                <h5 class="modal-title" id="edit-photo-modal-title"><?= Yii::t('jobseeker', 'Edit profile photo') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card card-body mb-2 px-0 py-0 <?= (empty($Jobseeker->photo)) ? 'collapse hide' : 'collapse show' ?>" id="photo-crop-image-container" data-parent="#edit-photo-form" id="photo-crop-container">
                    <img src="<?= (empty($Jobseeker->photo)) ?: $Jobseeker->photo->path  ?>" id="photo-crop-image" class="col-12" onload="$(this).removeAttr('onload'); imageJcrop(this, c = [<?=$c[0]?>,<?=$c[1]?>,<?=$c[0]+$c[2]?>,<?=$c[1]+$c[3]?>], $(this).parent().actual('innerWidth'), [$(this).prop('naturalWidth'), $(this).prop('naturalHeight')]);"> 
                </div>
                
                <div class="card card-body mb-2 bg-light <?= (empty($Jobseeker->photo)) ? 'collapse show' : 'collapse hide' ?>" data-parent="#edit-photo-form">
                    <h6 class="text-secondary font-weight-normal mb-0">
                        <?= Yii::t('jobseeker', 'No photo added yet') ?>
                    </h6>
                </div>
                <div class="card card-body b-none justify-content-around" style="flex-direction: row;">
                    <?= $editPhotoForm->field($editPhotoFormModel, 'photo', ['options' => ['tag' => false]])->label('<i class="fas fa-upload"></i> ' . Yii::t('jobseeker', 'Upload new photo'), ['class' => 'btn btn-primary d-inline-block col-5'])->fileInput(['class' => 'd-none', 'onchange' => 'return updateProfilePhotoAjax($(this).parents("form")[0], "#EditPhotoForm", "#Jobseeker")']) ?>

                    <?= Html::button('<i class="fas fa-trash"></i> ' . Yii::t('jobseeker', 'Delete photo'), ['class' => 'btn btn-secondary d-inline-block text-main mb-2 col-5', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-original-title' => Yii::t('jobseeker', 'Remove other experience'), 'onclick' => 
                       '$("#JobseekerOtherExperience").toggleClass("loading");
                        $.ajax({
                            type: "POST",
                            url: "",
                            data: {
                                action: "JobseekerPhotoRemove",
                                jobseeker_id: "'.$Jobseeker->id.'"
                            }, success: function(response) {
                                if(response) {
                                    pjaxContainerReload("#Jobseeker");
                                }
                            }
                        });'
                    ]) ?>
                </div>
                <?= $editPhotoForm->field($editPhotoFormModel, 'crop_x1', [ 'options' => ['tag' => false]])->hiddenInput(['id' => 'edit-photo-form-crop-x1'])->label(false) ?>
                <?= $editPhotoForm->field($editPhotoFormModel, 'crop_y1', [ 'options' => ['tag' => false]])->hiddenInput(['id' => 'edit-photo-form-crop-y1'])->label(false) ?>
                <?= $editPhotoForm->field($editPhotoFormModel, 'crop_height', [ 'options' => ['tag' => false]])->hiddenInput(['id' => 'edit-photo-form-crop-width'])->label(false) ?>
                <?= $editPhotoForm->field($editPhotoFormModel, 'crop_width', [ 'options' => ['tag' => false]])->hiddenInput(['id' => 'edit-photo-form-crop-height'])->label(false) ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Save') ?></button>
            </div>
            <?php 
                
                ActiveForm::end();
                Pjax::end();
                
            ?>
        </div>
    </div>
</div>

<?php
    
endif;

if(!empty($addLanguageFormModel)):

?>

<div class="modal fade" id="add-language-modal" tabindex="-1" aria-labelledby="add-language-modal-title" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content br-2 b-none">
            <?php 
                $addLanguageForm = ActiveForm::begin([
                    'id' => 'add-language-form',
                    'options' => [
                        'onsubmit' => 'return submitFormAjax(this, "#JobseekerLanguages");',
                        'style' => 'height: 100%; overflow-y: auto;'
                        ]
                ]);
            ?>
            <div class="modal-header">
                <h5 class="modal-title" id="add-language-modal-title"><?= Yii::t('jobseeker', 'Add language') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            
                <?= $addLanguageForm->field($addLanguageFormModel, 'language', ['addAriaAttributes' => true, 'enableClientValidation' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'id' => 'add-language-form-select'] ])->label(Yii::t('jobseeker', 'Choose language'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                    [
                        'theme' => 'my-settings','pluginLoading' => false,
                        'options' => [
                            'placeholder' => Yii::t('jobseeker', 'Select language')
                        ],
                        'pluginLoading' => true,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 0,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return '" . Yii::t('jobseeker', 'Waiting for results...') . "'; }"),
                            ],
                            'ajax' => [
                                'url' =>  Yii::getAlias('@web') . '/api/v1/languages/search',
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {"filter[english_name][like]":params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(languages) { return languages.english_name; }'),
                            'templateSelection' => new JsExpression('function (languages) { return languages.english_name; }'),
                        ]
                    ])
                ?>
                
                <?= $addLanguageForm->field($addLanguageFormModel, 'proficiency', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'id' => 'add-language-form-proficiency-select'] ])->label(Yii::t('jobseeker', 'Proficiency'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                    [
                        'data' => $SettingsSelectData->language_proficiency,
                        'theme' => 'my-settings','pluginLoading' => false,
                        'options' => [
                            'placeholder' => Yii::t('jobseeker', 'Proficiency'),
                        ],                        
                        'pluginLoading' => false,
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ])
                ?>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Save') ?></button>
            </div>
            <?php 
                ActiveForm::end();
            ?>
        </div>
    </div>
</div>

<?php

endif;


if(!empty($addEducationFormModel)):

?>

<div class="modal fade" id="add-education-modal" tabindex="-1" aria-labelledby="add-education-modal-title" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content br-2 b-none">
            <?php 
                $addEducationForm = ActiveForm::begin([
                    'id' => 'add-education-form',
                    'options' => ['style' => 'height: 100%; overflow-y: auto;']
                ]);
            ?>
            <div class="modal-header">
                <h5 class="modal-title" id="add-education-modal-title"><?= Yii::t('jobseeker', 'Add education') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <?= $addEducationForm->field($addEducationFormModel, 'education_level', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'id' => 'add-education-form-education-level-select'] ])->label(Yii::t('jobseeker', 'What is your education level?'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                    [
                        'data' => $SettingsSelectData->education_levels,
                        'theme' => 'my-settings','pluginLoading' => false,
                        'options' => ['placeholder' => Yii::t('jobseeker', 'Select your education level')],
                        'pluginLoading' => false,
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ])
                ?>
                
                <?= $addEducationForm->field($addEducationFormModel, 'country', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'id' => 'add-education-form-country-select'] ])->label(Yii::t('jobseeker', 'Country'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                    [
                        'theme' => 'my-settings','pluginLoading' => false,
                        'options' => ['placeholder' => Yii::t('jobseeker', 'Country')],
                        'pluginLoading' => false,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 1,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return '" . Yii::t('jobseeker', 'Waiting for results...') . "'; }"),
                            ],
                            'ajax' => [
                                'url' => Yii::getAlias('@web') . '/api/v1/geodb/countries',
                                'dataType' => 'json',
                                'delay' => 250,
                                'data' => new JsExpression('function(params) { return {namePrefix:params.term}; }'),
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ],
                        'pluginEvents' => [
                            'select2:select' => new JsExpression($AddEducationCountryChange),
                            'select2:clear' => new JsExpression($AddEducationCountryChange)
                        ]
                    ])
                ?>  
                
                <?= $addEducationForm->field($addEducationFormModel, 'educational_institution', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'id' => 'add-education-form-educational-institution-select'] ])->label(Yii::t('jobseeker', 'Educational Institution'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                    [
                        'theme' => 'my-settings','pluginLoading' => false,
                        'options' => ['placeholder' => Yii::t('jobseeker', 'Educational institution')],
                        'pluginLoading' => false,
                        'disabled' => true,
                        'maintainOrder' => true,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 1,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return '" . Yii::t('jobseeker', 'Waiting for results...') . "'; }"),
                            ],
                            'ajax' => [
                                'url' => Yii::getAlias('@web') . '/api/v1/universities/country',
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {countryCode:$("#add-education-form-country-select").val().toLowerCase(), namePrefix:params.term}; }'),
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(university) { return university.text; }'),
                            'templateSelection' => new JsExpression('function (university) { return university.text; }'),
                        ]
                    ])
                ?>  
                
                <?= $addEducationForm
                    ->field($addEducationFormModel, 'specialization', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'inputOptions' => ['class' => 'form-control', 'id' => 'add-education-form-specialization', 'placeholder' => Yii::t('jobseeker', 'e.g. Finance')] ])
                    ->label(Yii::t('jobseeker', 'Specialization title') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label label-popover font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                    ->hint(Yii::t('jobseeker', 'Enter your specialization title'), ['class' => 'hint-block text-secondary']) ?>
                
                
                <div class="col-9 col-md-6 float-left pr-3 pl-0">
                    <label class="control-label label-popover font-weight-normal text-main-2 w-100"><?= Yii::t('jobseeker', 'Education start date') ?></label>
                    <div class="form-group field-addeducationform-education_start_date_month col-6 pr-1 pl-0 float-left"> 
                    <?= $addEducationForm->field($addEducationFormModel, 'education_start_date_month', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-education-form-education-start-date-month'] ])
                    ->widget(Select2::classname(), 
                        [
                            'data' => $SettingsSelectData->months,
                            'theme' => 'my-settings','pluginLoading' => false,
                            'options' => ['placeholder' => Yii::t('jobseeker', 'Month')],
                            'pluginLoading' => false,
                            'pluginOptions' => [
                                'allowClear' => true,
                            ]
                        ])
                    ?>
                    </div>
                    <div class="form-group field-addeducationform-education_start_date_year col-6 pr-1 pl-0 float-left">
                    <?= $addEducationForm->field($addEducationFormModel, 'education_start_date_year', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-education-form-education-start-date-year']])
                        ->widget(Select2::classname(), 
                        [
                            'data' => $SettingsSelectData->years,
                            'theme' => 'my-settings','pluginLoading' => false,
                            'options' => ['placeholder' => Yii::t('jobseeker', 'Year')],
                            'pluginLoading' => false,
                            'pluginOptions' => [
                                'allowClear' => true,
                            ]
                        ])
                    ?>
                    </div>
                </div>
                
                <div class="col-9 col-md-6 float-left pr-3 pl-0">
                    <label class="control-label label-popover font-weight-normal text-main-2 w-100"><?= Yii::t('jobseeker', 'Education end date') ?></label>
                    <div class="form-group field-addeducationform-education_start_date_month col-6 pr-1 pl-0 float-left"> 
                    <?= $addEducationForm->field($addEducationFormModel, 'education_end_date_month', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-education-form-education-end-date-month'] ])
                        ->widget(Select2::classname(), 
                        [
                            'data' => $SettingsSelectData->months,
                            'theme' => 'my-settings','pluginLoading' => false,
                            'options' => ['placeholder' => Yii::t('jobseeker', 'Month')],
                            'pluginLoading' => false,
                            'pluginOptions' => [
                                'allowClear' => true,
                            ]
                        ])
                    ?>
                    </div>
               
                    <div class="form-group field-addeducationform-education_start_date_year col-6 pr-0 pl-0 float-left">
                    <?= $addEducationForm->field($addEducationFormModel, 'education_end_date_year', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-education-form-education-end-date-year']])
                        ->widget(Select2::classname(), 
                        [
                            'data' => $SettingsSelectData->years,
                            'theme' => 'my-settings','pluginLoading' => false,
                            'options' => ['placeholder' => Yii::t('jobseeker', 'Year')],
                            'pluginLoading' => false,
                            'pluginOptions' => [
                                'allowClear' => true,
                            ]
                        ])
                    ?>
                    </div>
                </div>
                
                <div class="form-group field-add-education-form-description col-12 px-0 float-left">
                <?= $addEducationForm
                    ->field($addEducationFormModel, 'description', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-education-form-description'] ])
                    ->label(Yii::t('jobseeker', 'Pitch') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                    ->hint(Yii::t('jobseeker', 'Describe briefly and succinctly your education.') . '<ul class="pl-3 mb-0"><li>' . Yii::t('jobseeker', 'List the main things you learned') . '</li><li>' . Yii::t('jobseeker', 'Highlight the main subjects' . '</li><li>' . Yii::t('jobseeker', 'Keep it short and informative').'</li></ul>'), ['class' => 'hint-block text-secondary'])
                    ->textarea([])?> 
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Add') ?></button>
            </div>
            <?php 
                ActiveForm::end();
            ?>
        </div>
    </div>
</div>

<?php

endif;

if(!empty($addAdditionalEducationFormModel)):

?>

<div class="modal fade" id="add-additional-education-modal" tabindex="-1" aria-labelledby="add-additional-education-modal-title" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content br-2 b-none">
            <?php 
                $addAdditionalEducationForm = ActiveForm::begin([
                    'id' => 'add-additional-education-form',
                    'options' => ['style' => 'height: 100%; overflow-y: auto;']
                ]);
            ?>
            <div class="modal-header">
                <h5 class="modal-title" id="add-additional-education-modal-title"><?= Yii::t('jobseeker', 'Add additional education') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                 
                <?= $addAdditionalEducationForm
                    ->field($addAdditionalEducationFormModel, 'educational_institution', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'inputOptions' => ['class' => 'form-control', 'id' => 'add-education-form-educational-institution', 'placeholder' => Yii::t('jobseeker', 'e.g. Design Academy London')] ])
                    ->label(Yii::t('jobseeker', 'Educational Institution') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label label-popover font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                    ->hint(Yii::t('jobseeker', 'Enter the name of school, academy, college etc.'), ['class' => 'hint-block text-secondary']) ?>  
                
                <?= $addAdditionalEducationForm
                    ->field($addAdditionalEducationFormModel, 'specialization', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'inputOptions' => ['class' => 'form-control', 'id' => 'add-additional-education-form-specialization', 'placeholder' => Yii::t('jobseeker', 'e.g. Web-Design')] ])
                    ->label(Yii::t('jobseeker', 'Area of study') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label label-popover font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                    ->hint(Yii::t('jobseeker', 'Enter the title of the course you\'ve attended'), ['class' => 'hint-block text-secondary']) ?>
                
                <?= $addAdditionalEducationForm->field($addAdditionalEducationFormModel, 'country', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'id' => 'add-additional-education-form-country-select'] ])->label(Yii::t('jobseeker', 'Country'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                    [
                        'theme' => 'my-settings','pluginLoading' => false,
                        'options' => ['placeholder' => Yii::t('jobseeker', 'Country')],
                        'pluginLoading' => false,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 1,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return '" . Yii::t('jobseeker', 'Waiting for results...') . "'; }"),
                            ],
                            'ajax' => [
                                'url' => Yii::getAlias('@web') . '/api/v1/geodb/countries',
                                'dataType' => 'json',
                                'delay' => 250,
                                'data' => new JsExpression('function(params) { return {namePrefix:params.term}; }'),
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ]
                    ])
                ?>
                
                <div class="col-9 col-md-6 float-left pr-3 pl-0">
                    <label class="control-label label-popover font-weight-normal text-main-2 w-100"><?= Yii::t('jobseeker', 'Education start date') ?></label>
                    <div class="form-group field-addadditionaleducationform-education_start_date_month col-6 pr-1 pl-0 float-left"> 
                    <?= $addAdditionalEducationForm->field($addAdditionalEducationFormModel, 'education_start_date_month', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-additional-education-form-education-start-date-month'] ])
                    ->widget(Select2::classname(), 
                        [
                            'data' => $SettingsSelectData->months,
                            'theme' => 'my-settings','pluginLoading' => false,
                            'options' => ['placeholder' => Yii::t('jobseeker', 'Month')],
                            'pluginLoading' => false,
                            'pluginOptions' => [
                                'allowClear' => true,
                            ]
                        ])
                    ?>
                    </div>
               
                <?php
                
                $year = array();
                for($i = date('Y'); $i >= 1900; $i--)
                    $year[$i] = $i;
                
                ?>
                    <div class="form-group field-addadditionaleducationform-education_start_date_year col-6 pr-1 pl-0 float-left">
                    <?= $addAdditionalEducationForm->field($addAdditionalEducationFormModel, 'education_start_date_year', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'addadditional-education-form-education-start-date-year']])
                        ->widget(Select2::classname(), 
                        [
                            'data' => $SettingsSelectData->years,
                            'theme' => 'my-settings','pluginLoading' => false,
                            'options' => ['placeholder' => Yii::t('jobseeker', 'Year')],
                            'pluginLoading' => false,
                            'pluginOptions' => [
                                'allowClear' => true,
                            ]
                        ])
                    ?>
                    </div>
                </div>
                
                <div class="col-9 col-md-6 float-left pr-3 pl-0">
                    <label class="control-label label-popover font-weight-normal text-main-2 w-100"><?= Yii::t('jobseeker', 'Education end date') ?></label>
                    <div class="form-group field-addadditionaleducationform-education_start_date_month col-6 pr-1 pl-0 float-left"> 
                    <?= $addAdditionalEducationForm->field($addAdditionalEducationFormModel, 'education_end_date_month', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-additional-education-form-education-end-date-month'] ])
                        ->widget(Select2::classname(), 
                        [
                            'data' => $SettingsSelectData->months,
                            'theme' => 'my-settings','pluginLoading' => false,
                            'options' => ['placeholder' => Yii::t('jobseeker', 'Month')],
                            'pluginLoading' => false,
                            'pluginOptions' => [
                                'allowClear' => true,
                            ]
                        ])
                    ?>
                    </div>
                    <div class="form-group field-addadditionaleducationform-education_start_date_year col-6 pr-0 pl-0 float-left">
                    <?= $addAdditionalEducationForm->field($addAdditionalEducationFormModel, 'education_end_date_year', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-additional-education-form-education-end-date-year']])
                        ->widget(Select2::classname(), 
                        [
                            'data' => $SettingsSelectData->years,
                            'theme' => 'my-settings','pluginLoading' => false,
                            'options' => ['placeholder' => Yii::t('jobseeker', 'Year')],
                            'pluginLoading' => false,
                            'pluginOptions' => [
                                'allowClear' => true,
                            ]
                        ])
                    ?>
                    </div>
                </div>
                
                <div class="form-group field-add-additional-education-form-description col-12 px-0 float-left">
                <?= $addAdditionalEducationForm
                    ->field($addAdditionalEducationFormModel, 'description', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-additional-education-form-description'] ])
                    ->label(Yii::t('jobseeker', 'Pitch') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                    ->hint(Yii::t('jobseeker', 'Describe briefly and succinctly your education.') . '<ul class="pl-3 mb-0"><li>' . Yii::t('jobseeker', 'List the main things you learned') . '</li><li>' . Yii::t('jobseeker', 'Highlight the main subjects' . '</li><li>' . Yii::t('jobseeker', 'Keep it short and informative').'</li></ul>'), ['class' => 'hint-block text-secondary'])
                    ->textarea([])?> 
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Add') ?></button>
            </div>
            <?php 
                ActiveForm::end();
            ?>
        </div>
    </div>
</div>

<?php

endif;

if(!empty($addWorkExperienceFormModel)):

?>

<div class="modal fade" id="add-work-experience-modal" tabindex="-1" aria-labelledby="add-work-experience-modal-title" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content br-2 b-none">
            <?php 
                $addWorkExperienceForm = ActiveForm::begin([
                    'id' => 'add-work-experience-form',
                    'options' => ['style' => 'height: 100%; overflow-y: auto;']
                ]);
            ?>
            <div class="modal-header">
                <h5 class="modal-title" id="add-additional-education-modal-title"><?= Yii::t('jobseeker', 'Add work experience') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                 
                <?= $addWorkExperienceForm
                    ->field($addWorkExperienceFormModel, 'company_name', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'inputOptions' => ['class' => 'form-control', 'id' => 'add-work-experience-form-company-name', 'placeholder' => Yii::t('jobseeker', 'e.g. Example Gmbh')] ])
                    ->label(Yii::t('jobseeker', 'Company name') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label label-popover font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                    ->hint(Yii::t('jobseeker', 'Enter the name of the company you worked for.'), ['class' => 'hint-block text-secondary']) ?>  
                
                <?= $addWorkExperienceForm->field($addWorkExperienceFormModel, 'company_industry', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'id' => 'add-work-experience-form-company-industry-select'] ])->label(Yii::t('jobseeker', 'Comapany industry'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                    [
                        'data' => $SettingsSelectData->specialization,
                        'theme' => 'my-settings','pluginLoading' => false,
                        'options' => [
                            'placeholder' => Yii::t('jobseeker', 'Select company industry')
                        ],
                        'pluginLoading' => false,
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ])
                ?>
                    
                <?= $addWorkExperienceForm
                    ->field($addWorkExperienceFormModel, 'job_title', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'inputOptions' => ['class' => 'form-control', 'id' => 'add-work-experience-form-job-title', 'placeholder' => Yii::t('jobseeker', 'Senior Web Developer')] ])
                    ->label(Yii::t('jobseeker', 'Job title') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label label-popover font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.']) ?>  
                
                <?= $addWorkExperienceForm->field($addWorkExperienceFormModel, 'country', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'id' => 'add-work-experience-form-country-select'] ])->label(Yii::t('jobseeker', 'Country'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                    [
                        'theme' => 'my-settings','pluginLoading' => false,
                        'options' => ['placeholder' => Yii::t('jobseeker', 'Country')],
                        'pluginLoading' => false,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 1,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return '" . Yii::t('jobseeker', 'Waiting for results...') . "'; }"),
                            ],
                            'ajax' => [
                                'url' => Yii::getAlias('@web') . '/api/v1/geodb/countries',
                                'dataType' => 'json',
                                'delay' => 250,
                                'data' => new JsExpression('function(params) { return {namePrefix:params.term}; }'),
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ]
                    ])
                ?>
                
                <div class="col-9 col-md-6 float-left pr-3 pl-0">
                    <label class="control-label label-popover font-weight-normal text-main-2 w-100"><?= Yii::t('jobseeker', 'Work begin date') ?></label>
                    <div class="form-group field-addadditionaleducationform-education_start_date_month col-6 pr-1 pl-0 float-left"> 
                    <?= $addWorkExperienceForm->field($addWorkExperienceFormModel, 'work_start_date_month', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-work-experience-form-work-start-date-month'] ])
                    ->widget(Select2::classname(), 
                        [
                            'data' => $SettingsSelectData->months,
                            'theme' => 'my-settings','pluginLoading' => false,
                            'options' => ['placeholder' => Yii::t('jobseeker', 'Month')],
                            'pluginLoading' => false,
                            'pluginOptions' => [
                                'allowClear' => true,
                            ]
                        ])
                    ?>
                    </div>
                    <div class="form-group field-addadditionaleducationform-education_start_date_year col-6 pr-1 pl-0 float-left">
                    <?= $addWorkExperienceForm->field($addWorkExperienceFormModel, 'work_start_date_year', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-work-experience-form-work-start-date-year']])
                        ->widget(Select2::classname(), 
                        [
                            'data' => $SettingsSelectData->years,
                            'theme' => 'my-settings','pluginLoading' => false,
                            'options' => ['placeholder' => Yii::t('jobseeker', 'Year')],
                            'pluginLoading' => false,
                            'pluginOptions' => [
                                'allowClear' => true,
                            ]
                        ])
                    ?>
                    </div>
                </div>
                
                <div class="col-9 col-md-6 float-left pr-3 pl-0">
                    <label class="control-label label-popover font-weight-normal text-main-2 w-100"><?= Yii::t('jobseeker', 'Work end date') ?></label>
                    <div class="form-group field-addadditionaleducationform-education_start_date_month col-6 pr-1 pl-0 float-left"> 
                    <?= $addWorkExperienceForm->field($addWorkExperienceFormModel, 'work_end_date_month', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-work-experience-form-work-end-date-month'] ])
                        ->widget(Select2::classname(), 
                        [
                            'data' => $SettingsSelectData->months,
                            'theme' => 'my-settings','pluginLoading' => false,
                            'options' => ['placeholder' => Yii::t('jobseeker', 'Month')],
                            'pluginLoading' => false,
                            'pluginOptions' => [
                                'allowClear' => true,
                            ]
                        ])
                    ?>
                    </div>
                    <div class="form-group field-addadditionaleducationform-education_start_date_year col-6 pr-0 pl-0 float-left">
                    <?= $addWorkExperienceForm->field($addWorkExperienceFormModel, 'work_end_date_year', ['addAriaAttributes' => true, 'template' => "{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-work-experience-form-work-end-date-year']])
                        ->widget(Select2::classname(), 
                        [
                            'data' => $SettingsSelectData->years,
                            'theme' => 'my-settings','pluginLoading' => false,
                            'options' => ['placeholder' => Yii::t('jobseeker', 'Year')],
                            'pluginLoading' => false,
                            'pluginOptions' => [
                                'allowClear' => true,
                            ]
                        ])
                    ?>
                    </div>
                    
                    
                </div>
                
                <div class="form-group field-add-additional-education-form-description col-12 px-0 float-left">
                <?= $addWorkExperienceForm
                    ->field($addWorkExperienceFormModel, 'description', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-additional-education-form-description'] ])
                    ->label(Yii::t('jobseeker', 'Descritpion') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                    ->hint(Yii::t('jobseeker', 'Describe briefly and succinctly your work.') . '<ul class="pl-3 mb-0"><li>' . Yii::t('jobseeker', 'List the main things you learned') . '</li><li>' . Yii::t('jobseeker', 'Highlight the main subjects' . '</li><li>' . Yii::t('jobseeker', 'Keep it short and informative').'</li></ul>'), ['class' => 'hint-block text-secondary'])
                    ->textarea([])?> 
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Add') ?></button>
            </div>
            <?php 
                ActiveForm::end();
            ?>
        </div>
    </div>
</div>

<?php

endif;

if(!empty($addOtherExperienceFormModel)):

?>

<div class="modal fade" id="add-other-experience-modal" tabindex="-1" aria-labelledby="add-other-experience-modal-title" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content br-2 b-none">
            <?php 
                $addOtherExperienceForm = ActiveForm::begin([
                    'id' => 'add-other-experience-form',
                    'options' => ['style' => 'height: 100%; overflow-y: auto;']
                ]);
            ?>
            <div class="modal-header">
                <h5 class="modal-title" id="add-other-education-modal-title"><?= Yii::t('jobseeker', 'Add other experience') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                 
                <?= $addOtherExperienceForm
                    ->field($addOtherExperienceFormModel, 'title', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'inputOptions' => ['class' => 'form-control', 'id' => 'add-other-experience-form-title', 'placeholder' => Yii::t('jobseeker', 'e.g. Example Gmbh')] ])
                    ->label(Yii::t('jobseeker', 'Title') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label label-popover font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.']) ?>  

                <div class="form-group field-add-other-experience-form-description col-12 px-0 float-left">
                <?= $addOtherExperienceForm
                    ->field($addOtherExperienceFormModel, 'description', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-other-experience-form-description'] ])
                    ->label(Yii::t('jobseeker', 'Descritpion') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                    ->hint(Yii::t('jobseeker', 'Describe briefly and succinctly your experience.') . '<ul class="pl-3 mb-0"><li>' . Yii::t('jobseeker', 'List the main things you learned') . '</li><li>' . Yii::t('jobseeker', 'Highlight the main subjects' . '</li><li>' . Yii::t('jobseeker', 'Keep it short and informative').'</li></ul>'), ['class' => 'hint-block text-secondary'])
                    ->textarea([])?> 
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Add') ?></button>
            </div>
            <?php 
                ActiveForm::end();
            ?>
        </div>
    </div>
</div>

<?php

endif;

if(!empty($addProfileFormModel)):

?>

<div class="modal fade" id="add-profile-modal" tabindex="-1" aria-labelledby="add-profile-modal-title" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content br-2 b-none">
            <?php 
                $addProfileForm = ActiveForm::begin([
                    'id' => 'add-profile-form',
                    'options' => [
                        'style' => 'height: 100%; overflow-y: auto;'
                    ]
                ]);
            ?>
            <div class="modal-header">
                <h5 class="modal-title" id="add-profile-modal-title"><?= Yii::t('jobseeker', 'Add specialized profile') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="<?= Yii::t('jobseeker', 'Close') ?>">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            
                <?= $addProfileForm
                    ->field($addProfileFormModel, 'title', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'inputOptions' => ['class' => 'form-control', 'id' => 'add-profile-form-title', 'placeholder' => Yii::t('jobseeker', 'e.g. Senior Web Developer')] ])
                    ->label(Yii::t('jobseeker', 'Profile title') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label label-popover font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                    ->hint(Yii::t('jobseeker', 'Enter your specialization title'), ['class' => 'hint-block text-secondary']) ?>
                
                <?= $addProfileForm
                    ->field($addProfileFormModel, 'employment_type', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control', 'id' => 'add-profile-form-employment-type-select'] ])
                    ->label(Yii::t('jobseeker', 'Employment type'), ['class' => 'control-label font-weight-normal text-main-2'])
                    ->widget(Select2::classname(), 
                    [
                        'data' => $SettingsSelectData->employment_types,
                        'theme' => 'my-settings','pluginLoading' => false,
                        'options' => [
                            'multiple' => true,
                            'placeholder' => Yii::t('jobseeker', 'Select desired employment type(s)')
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'maximumInputLength' => 10
                        ],
                    ])
                ?>
                <div class="form-group field-add-profile-form-salary col-6 px-0 float-left">
                <?= $addProfileForm
                    ->field($addProfileFormModel, 'salary', ['addAriaAttributes' => true, 'enableClientValidation' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-profile-form-salary'] ])
                    ->label(Yii::t('jobseeker', 'Salary') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                    ->hint(Yii::t('jobseeker', 'Enter your desired salary'), ['class' => 'hint-block text-secondary']) ?>
                </div>
                
                <div class="form-group field-add-profile-form-salary-currency-select col-5 offset-1 px-0 float-left">
                <?= $addProfileForm->field($addProfileFormModel, 'salary_currency', ['addAriaAttributes' => true, 'enableClientValidation' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-profile-form-currency-select'] ])->label(Yii::t('jobseeker', 'Currency'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                    [
                        'data' => $SettingsSelectData->currencies,
                        'theme' => 'my-settings','pluginLoading' => false,
                        'options' => [
                            'placeholder' => Yii::t('jobseeker', 'Currency'),
                        ],                        
                        'pluginLoading' => false,
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ])
                ?>
                </div>
                
                <div class="form-group field-add-profile-form-specialization-select required col-12 px-0 float-left">
                <?= $addProfileForm->field($addProfileFormModel, 'specialization', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-profile-form-specialization-select'] ])->label(Yii::t('jobseeker', 'Specialization'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                    [
                        'data' => $SettingsSelectData->specialization,
                        'theme' => 'my-settings','pluginLoading' => false,
                        'options' => [
                            'placeholder' => Yii::t('jobseeker', 'Select specialization')
                        ],
//                        'pluginLoading' => false,
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'pluginEvents' => [
                            'select2:select' => new JsExpression($AddProfileFormSpecializationChange),
                            'select2:clear' => new JsExpression($AddProfileFormSpecializationChange)
                        ]
                    ])
                ?>
                </div>
                <div class="form-group field-add-profile-form-categories-select required col-12 px-0 float-left">
                <?= $addProfileForm->field($addProfileFormModel, 'categories', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{hint}\n{error}",  'inputOptions' => ['class' => 'form-control', 'id' => 'add-profile-form-categories-select'] ])->label(Yii::t('jobseeker', 'Categories'), ['class' => 'control-label font-weight-normal text-main-2'])->widget(Select2::classname(), 
                    [
                        'theme' => 'my-settings','pluginLoading' => false,
                        'options' => [
                            'multiple' => true,
                            'placeholder' => Yii::t('jobseeker', 'Select categories')
                        ],
                        'disabled' => true,
                        'hideSearch' => true,
                        'pluginLoading' => true,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 0,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return '" . Yii::t('jobseeker', 'Waiting for results...') . "'; }"),
                            ],
                            'ajax' => [
                                'url' => Yii::getAlias('@web') . '/api/v1/specializations/categories',
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {specialization_id:$("#add-profile-form-specialization-select").val(), title_prefix:params.term}; }'),
                            ],
                            'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(categories) { return categories.title; }'),
                            'templateSelection' => new JsExpression('function (categories) { return categories.title; }'),
                        ]
                    ])
                ?>
                </div>
                <div class="form-group field-add-profile-form-description col-12 px-0 float-left">
                <?= $addProfileForm
                    ->field($addProfileFormModel, 'description', ['addAriaAttributes' => true, 'template' => "{label}\n{input}\n{error}\n{hint}", 'options' => ['tag' => false], 'inputOptions' => ['class' => 'form-control', 'id' => 'add-profile-form-description'] ])
                    ->label(Yii::t('jobseeker', 'Pitch') . ' <i class="fal fa-question-circle "></i>', ['class' => 'control-label font-weight-normal text-main-2', 'data-toggle' => 'popover', 'data-trigger' => 'click hover', 'data-placement' => 'bottom', 'data-content'=>'Your email is required in order for you to sign in to your dashboard and to remind you of your password if you forget it. If you post jobs, you will receive applications in your email.'])
                    ->hint(Yii::t('jobseeker', 'Use this space to show employers you have the skills and experience they\'re looking for.') . '<ul class="pl-3 mb-0"><li>' . Yii::t('jobseeker', 'Describe your strengths and skills') . '</li><li>' . Yii::t('jobseeker', 'Highlight projects, accomplishments and education.' . '</li><li>' . Yii::t('jobseeker', 'Keep it short and make sure it\'s error-free').'</li></ul>'), ['class' => 'hint-block text-secondary'])
                    ->textarea([])?> 
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary text-main br-2" data-dismiss="modal"><?= Yii::t('jobseeker', 'Cancel') ?></button>
                <button type="submit" class="btn btn-primary br-2"><?= Yii::t('jobseeker', 'Add') ?></button>
            </div>
            <?php 
                ActiveForm::end();
            ?>
        </div>
    </div>
</div>

<?php

endif;

?>