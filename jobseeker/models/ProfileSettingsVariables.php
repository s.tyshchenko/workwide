<?php

namespace jobseeker\models;

use common\models\Specializations;                
use common\models\Currencies;
use common\models\EducationLevels;
use common\models\EmploymentTypes;
use common\models\ProfileVisibility;
use common\models\Countries;
use common\models\LanguageProficiency;
use common\models\SkillProficiency;
use common\models\SkillLastUsed;
use common\models\SoftSkills;

class ProfileSettingsVariables {
    
    public $specialization;
    public $currencies;
    public $education_levels;
    public $employment_types;
    public $profile_visibility;
    public $countries;
    public $language_proficiency;
    public $skill_proficiency;
    public $skill_last_used;
    public $soft_skills;
    public $years;
    public $months;
    
    function __construct() {
        $this->specialization          = Specializations::combineSpecializations();
        $this->currencies              = Currencies::combineCurrencies();
        $this->education_levels        = EducationLevels::combineEducationLevels();
        $this->employment_types        = EmploymentTypes::combineEmploymentTypes();
        $this->profile_visibility      = ProfileVisibility::combineProfileVisibility();
        $this->countries               = Countries::combineCountries();
        $this->language_proficiency    = LanguageProficiency::combineLanguageProficiency();
        $this->skill_proficiency       = SkillProficiency::combineSkillProficiency();
        $this->skill_last_used         = SkillLastUsed::combineSkillLastUsed();
        $this->soft_skills             = SoftSkills::combineSoftSkills();
        $this->years                   = array_combine(range(1900,date('Y', strtotime('+5 years'))),range(1900,date('Y', strtotime('+5 years'))));

        $this->months                  = array_combine(range(1,12),range(1,12));
        array_walk($this->months, function(&$item, $key){ $item = date('F', mktime(0, 0, 0, $key, 10)); });
    }
    
}