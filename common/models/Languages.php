<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Languages extends ActiveRecord {
    
    public static function tableName()
    {
        return '_app_languages';
    }
    
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['date_create', 'date_update'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['date_update'],
                ],
            ],
        ];
    }
    
    //Переменная, для хранения текущего объекта языка
    static $current = null;

    //Получение текущего объекта языка
    static function getCurrent()
    {
//    	if(Yii::$app->request->cookies->has('language')){
//            self::$current = self::getCookieLang();
//        }else
        if( self::$current === null ){
            self::$current = self::getDefaultLang();
        }
        return self::$current;
    }

    //Установка текущего объекта языка и локаль пользователя
    static function setCurrent($url = null)
    {
    	
        $language = self::getLangByUrl($url);
        
        self::$current = ($language === null) ? self::getDefaultLang() : $language;
        
        Yii::$app->language = self::$current->local;
    }
    
    //Получения объекта языка по умолчанию
    static function getDefaultLang()
    {   
        $defaultLang = self::find()->where('`url` = :iso', [':iso' => strtolower(Yii::$app->geoip->ip()->isoCode)])->one(); 
        if(!empty($defaultLang))
            return $defaultLang;
        else
            return self::findOne(['default' => 1]);
    }

    //Получения объекта языка по буквенному идентификатору
    static function getLangByUrl($url = null)
    {
        if (empty($url)) {
            return null;
        } else {
            $language = self::find()->where('url = :url', [':url' => $url])->one();
            if ( $language === null ) {
                return null;
            }else{
                return $language;                 
            }
        }
    }
    
}

?>