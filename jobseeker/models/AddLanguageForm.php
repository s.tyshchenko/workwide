<?php

namespace jobseeker\models;

use Yii;
use yii\base\Model;
use common\models\Jobseekers;
use common\models\JobseekerLanguages;
use jobseeker\components\validators\JobseekerLanguagesValidator;

class AddLanguageForm extends Model 
{
    public $language;
    public $proficiency;
        
    public function rules()
    {
        return [
            [['language', 'proficiency'], 'required'],
            ['language', JobseekerLanguagesValidator::className()]
        ];
    }
    
    public function addLanguage() {
        if ($validation = !$this->validate()) {
            return $validation;
//            return Yii::$app->response->redirect(Yii::$app->urlManager->createUrl('jobseeker/my/settings/profile'));
        }
        
        $jobseeker_language = new JobseekerLanguages();
        $jobseeker_language->language = $this->language;
        $jobseeker_language->jobseeker_id = Jobseekers::find(['id' => Yii::$app->user->id])->one()->id;
        $jobseeker_language->proficiency = $this->proficiency;
        
        return $jobseeker_language->save();
        
//        return Yii::$app->response->redirect(Yii::$app->urlManager->createUrl('jobseeker/my/settings/profile'));
        
    }
}