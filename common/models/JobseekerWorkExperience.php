<?php

namespace common\models;

use Yii;
use common\models\Jobseekers;
use yii\db\ActiveRecord;

class JobseekerWorkExperience extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jobseeker_work_experience';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['jobseeker_id', 'required']
        ];
    }
    
    public function getJobseeker()
    {
        return $this->hasOne(Jobseekers::className(), ['id' => 'jobseeker_id']);
    }
    
}