<?php

namespace jobseeker\models;

use Yii;
use yii\base\Model;
use common\models\Jobseekers;
use api\modules\v1\models\GeoDB;

class EditLocationForm extends Model 
{
    public $country; 
//    public $region;
    public $city; 
        
    public function rules()
    {
        return [
            ['country', 'required'],
            ['city', 'required']
        ];
    }
    
    public function editLocation() {
        if(!$this->validate()) {
            return false;
        }
        
        $jobseeker = Jobseekers::findOne(['user_id' => Yii::$app->user->getId()]);
        $jobseeker->country = $this->country;
        $jobseeker->city = $this->city;
        $jobseeker->timezone = json_decode(file_get_contents("http://34.123.108.88/api/v1/geodb/city-details?cityId=" . $this->city))->result->timezone;
        
        return $jobseeker->save();
        
    }
}