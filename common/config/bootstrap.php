<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@web', 'http://34.70.72.34');
Yii::setAlias('@api', dirname(dirname(__DIR__)) . '/api');
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@jobseeker', dirname(dirname(__DIR__)) . '/jobseeker');
Yii::setAlias('@employer', dirname(dirname(__DIR__)) . '/employer');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
