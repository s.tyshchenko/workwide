<?php

namespace common\widgets;

use common\models\Languages;

class WLanguages extends \yii\bootstrap4\Widget
{
    public function init(){}

    public function run() {
        return $this->render('languages/view', [
            'current' => Languages::getCurrent(),
            'langs' => Languages::find()->where('id != :current_id', [':current_id' => Languages::getCurrent()->id])->all(),
        ]);
    }
}