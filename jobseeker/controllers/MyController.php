<?php

namespace jobseeker\controllers;

use Yii;
use yii\web\Controller;
use jobseeker\models\SearchForm;
use jobseeker\models\LoginForm;
use jobseeker\models\SignupForm;
use common\models\Users;
use common\models\Jobseekers;
use yii\widgets\ActiveForm;
use yii\web\Response;

class MyController extends Controller
{

    public function actionIndex()
    {
        if(Yii::$app->user->isGuest)
        {
            Yii::$app->response->redirect(Yii::$app->urlManager->createUrl('jobseeker/my/login'));
            return;
        }

        $User = Users::findOne(Yii::$app->user->id)->jobseeker->toArray();
        
        $this->layout = 'account';
        
        return $this->render('index', ['User' => $User, 'searchFormModel' => new SearchForm()]);
    }
    
    public function actionLogout()
    {
        Yii::$app->user->logout();
        Yii::$app->response->redirect(Yii::$app->request->referrer);
    }
    
    public function actionSignup()
    {
        if(!Yii::$app->user->isGuest)
        {
            Yii::$app->response->redirect(Yii::$app->urlManager->createUrl('jobseeker/my'));
        }

        $this->layout = 'signup';
        
        $signupFormModel = new SignupForm(['scenario' => 'phone_verify']);
        
        if(Yii::$app->request->isAjax)
        {
            switch(Yii::$app->request->post('action'))
            {
                case 'send':
                    $phone_verification_code = rand(100000, 999999);
                    
                    Yii::$app->session->set('phone_verification_code', ['code' => $phone_verification_code, 'timestamp' => time()]);
                    
                    return $this->phoneVerificationCodeSend(Yii::$app->request->post('phone_number'));     
                case 'validate':
                    $signupFormModel->load(Yii::$app->request->post(), 'SignupForm');
                    	
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    
                    return json_encode(ActiveForm::validate($signupFormModel, ['phone_verification_code']));
            }
        }
        
        if($signupFormModel->load(Yii::$app->request->post()))
        {            
            if($signupFormModel->signup())
            {
                Yii::$app->session->setFlash('success', Yii::t('jobseeker', 'You\'re successfully signed up!'));
                $this->layout = 'account';
                return $this->render('index');
            }
            else 
            {
                Yii::$app->session->setFlash('error', Yii::t('jobseeker', 'Sign up failed.'));
            }
        }
        
        return $this->render('signup', compact('signupFormModel'));
    }
    
    public function actionLogin()
    {
        if(!Yii::$app->user->isGuest)
        {
            Yii::$app->response->redirect(Yii::$app->urlManager->createUrl('jobseeker/my'));
        }

        $this->layout = 'login';
        
        $loginFormModel = new LoginForm();
        
        if($loginFormModel->load(Yii::$app->request->post()))
        {
            $user = $loginFormModel->login();
            if($user) {
                Yii::$app->session->setFlash('success', 'Validation success');
                Yii::$app->response->redirect(Yii::$app->urlManager->createUrl('jobseeker/my'));
            }
            else
            {
                Yii::$app->session->setFlash('error', 'Validation error');
            }
        }
        
        return $this->render('login', compact('loginFormModel'));
    }

    protected function phoneVerificationCodeSend($phone_number) {
        $session = Yii::$app->session;
        
        if(time() > $session['phone_verification_code']['timestamp'] + 60)
        {
            return file_get_contents('https://api.turbosms.ua/message/send.json?recipients[0]=' . urlencode($phone_number) . '&sms[sender]=Meduza&sms[text]=' . urlencode(Yii::t('jobseeker', '{code} - Work Wide phone confirmation code. Do not share it with anybody.', ['code' => $session['phone_verification_code']['code']])) . '&token=' . 'faade7455231d6b95f12cb74a62b965ffabbefab');
        }
    }
    
}