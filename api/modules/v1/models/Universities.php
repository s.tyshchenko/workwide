<?php

namespace api\modules\v1\models;

class Universities {
    
    function __construct() {
        $this->languageCode = Yii::$app->language;
    }

    public function getUniversitiesByCountry($countryCode, $namePrefix) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $curl = curl_init();

        curl_setopt_array($curl, [
                CURLOPT_URL => "https://university-college-list-and-rankings.p.rapidapi.com/api/universities?countryCode=" . $countryCode,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => [
                        "x-rapidapi-host: university-college-list-and-rankings.p.rapidapi.com",
                        "x-rapidapi-key: d53204ea6cmshec6074d3e1ed68bp1eee4bjsn9bfcdf28a6c2"
                ],
        ]);


        $response = json_decode(curl_exec($curl), true);
        $err = curl_error($curl);

        curl_close($curl);

        $response = preg_grep('~' . preg_quote($namePrefix, '~') . '|' . preg_quote(ucfirst($namePrefix), '~') . '~', $response);
              
        foreach ($response as $key => $value)
        {
            $results_array['results'][] = ['id' => $value, 'text' => $value];
        }        
        
        if($namePrefix)
        {
            $results_array ['results'][] = [['id' => ucwords($namePrefix), 'text' => ucwords($namePrefix)]];
        }
        
        if($err)
        {
            return "cURL Error #:" . $err;
        }
        else 
        {
            return $results_array;
        }
    }
}