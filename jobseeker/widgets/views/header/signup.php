<?php

use common\widgets\WLanguages;
use yii\helpers\Html;
use yii\helpers\Url;

$WidgetLanguages = WLanguages::widget();


?>

    <section class="container">
        <div class="row justify-content-between">
            <div class="col-4 col-md-3 col-lg-3">
                <a href="<?= Url::home() ?>">
                    <div id="header-logo-image">
                    	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Слой_1" x="0px" y="0px" viewBox="0 0 600 200" style="enable-background:new 0 0 600 200;" xml:space="preserve" height="44">
                        <style type="text/css">.st0{display:none;fill:#fff;}
                        	.st1{fill:#fff;}</style>
                        <rect class="st0" width="600" height="200"></rect>
                        <g>
                        	<path class="st1" d="M0,53.8h17.8L31.7,106l2,9.6h1l2.6-9.6l12.9-40.2h15.7l13,40.2l2.5,9.5h1l1.9-9.5l13-52.2h17.8l-23.7,89.5   H75.1l-14-43.1l-2.6-9.9h-1l-2.6,9.9l-14,43.1H24.7L0,53.8z"></path>
                        	<path class="st1" d="M146.2,80c4.7,0,9.1,0.8,13,2.4c4,1.6,7.4,3.9,10.2,6.7c2.9,2.9,5.1,6.3,6.7,10.3c1.6,4,2.4,8.4,2.4,13.1   s-0.8,9.1-2.4,13.1c-1.6,4-3.9,7.4-6.7,10.3c-2.9,2.9-6.3,5.1-10.2,6.7c-4,1.6-8.3,2.4-13,2.4s-9.1-0.8-13-2.4   c-4-1.6-7.4-3.9-10.2-6.7c-2.9-2.9-5.1-6.3-6.7-10.3s-2.4-8.4-2.4-13.1s0.8-9.1,2.4-13.1s3.9-7.4,6.7-10.3   c2.9-2.9,6.3-5.1,10.2-6.7C137.1,80.8,141.4,80,146.2,80z M146.2,130.1c2.1,0,4.1-0.4,6.1-1.2s3.7-1.9,5.2-3.4s2.7-3.3,3.6-5.5   c0.9-2.2,1.3-4.6,1.3-7.4s-0.4-5.2-1.3-7.4c-0.9-2.2-2.1-4-3.6-5.5s-3.2-2.6-5.2-3.4s-4-1.2-6.1-1.2c-2.2,0-4.2,0.4-6.1,1.2   c-1.9,0.8-3.6,1.9-5.1,3.4s-2.7,3.3-3.6,5.5c-0.9,2.2-1.3,4.6-1.3,7.4s0.4,5.2,1.3,7.4c0.9,2.2,2.1,4,3.6,5.5s3.2,2.6,5.1,3.4   C142,129.7,144,130.1,146.2,130.1z"></path>
                        	<path class="st1" d="M187.9,82h15.4v8.5h1c0.7-1.5,1.7-2.9,3-4.1c1.2-1.2,2.6-2.3,4.2-3.3c1.5-0.9,3.2-1.7,5-2.2   c1.8-0.6,3.6-0.9,5.3-0.9c2.2,0,4,0.2,5.6,0.7c1.5,0.5,2.8,1.1,3.9,1.8l-4.4,14.6c-1-0.5-2.1-0.9-3.3-1.2c-1.2-0.3-2.7-0.4-4.4-0.4   c-2.2,0-4.3,0.4-6.1,1.3s-3.4,2.1-4.7,3.7s-2.3,3.5-3,5.6s-1.1,4.5-1.1,7v30.1h-16.4V82z"></path>
                        	<path class="st1" d="M239.3,53.8h16.4v48.7h0.7L276.8,82H297v1l-22.7,22.1l24.6,37.1v1h-19.3l-16.7-26.9l-7.2,7.1v19.7h-16.4V53.8z   "></path>
                        	<path class="st1" d="M427,53.8h12l-23.8,89.5h-11.7l-19.2-59h-0.5l-19.2,59h-11.7l-24.8-89.5h12l18.8,70.3h0.5l19.3-58.4h10.7   l19.3,58.4h0.5L427,53.8z"></path>
                        	<path class="st1" d="M460.3,60.9c0,2.3-0.8,4.2-2.4,5.7c-1.6,1.6-3.5,2.4-5.7,2.4s-4.2-0.8-5.7-2.4c-1.6-1.6-2.4-3.5-2.4-5.7   s0.8-4.2,2.4-5.7c1.6-1.6,3.5-2.4,5.7-2.4s4.2,0.8,5.7,2.4C459.5,56.7,460.3,58.6,460.3,60.9z M458,82v61.2h-11.5V82H458z"></path>
                        	<path class="st1" d="M497.9,145.2c-8,0-14.9-3.2-20.6-9.5c-5.7-6.4-8.5-14.1-8.5-23.1s2.8-16.7,8.5-23.1c5.7-6.3,12.6-9.5,20.6-9.5   c4.5,0,8.6,1,12.3,2.9s6.6,4.5,8.5,7.6h0.5l-0.5-8.5V53.8h11.5v89.5h-11v-8.5h-0.5c-2,3.2-4.8,5.7-8.5,7.6   C506.5,144.3,502.4,145.2,497.9,145.2z M499.8,134.7c5.7,0,10.3-2,13.9-6.1c3.7-4.1,5.6-9.4,5.6-16c0-6.4-1.9-11.7-5.6-15.9   c-3.7-4.2-8.3-6.2-13.9-6.2c-5.5,0-10.1,2.1-13.9,6.2c-3.7,4.2-5.6,9.5-5.6,15.9c0,6.5,1.9,11.8,5.6,15.9   C489.7,132.7,494.3,134.7,499.8,134.7z"></path>
                        	<path class="st1" d="M571,145.2c-9,0-16.4-3.1-22.2-9.2c-5.8-6.2-8.7-14-8.7-23.4c0-9.3,2.8-17.1,8.5-23.3s12.9-9.3,21.7-9.3   c9.1,0,16.3,2.9,21.7,8.8s8.1,14.1,8.1,24.7l-0.1,1.2h-48.1c0.2,6,2.2,10.8,6,14.5s8.4,5.5,13.7,5.5c7.3,0,13.1-3.7,17.2-11l10.2,5   c-2.7,5.2-6.6,9.2-11.4,12.1C582.7,143.8,577.2,145.2,571,145.2z M552.7,105.2h35.1c-0.3-4.2-2.1-7.8-5.2-10.6   c-3.1-2.8-7.3-4.2-12.5-4.2c-4.3,0-8.1,1.3-11.2,4C555.8,97.2,553.7,100.7,552.7,105.2z"></path>
                        </g>
                        </svg>
                    </div>
                </a>
            </div>
            <div class="d-none d-md-block col-md-9 col-lg-9 py-1">
                <nav class="navbar navbar-expand float-right">
                    <ul class="navbar-nav float-right">
                        <li class="nav-item mx-1">
                            <?= Html::a(Yii::t('jobseeker', 'Employer'), Yii::$app->urlManager->createUrl('employer'), ['class' => 'nav-link--header nav-link--underline']) ?>
                        </li>
                        <li class="nav-item dropdown mx-1">
                            <?= $WidgetLanguages ?>
                        </li>   
                        <li class="nav-item dropdown unselectable br-2 bg-white mx-1">
                            <a id="navbarProfileDropdown" class="nav-link text-main" role="button" data-offset="10,0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="far fa-bars px-1"></i>
                              <i class="far fa-user px-1"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarProfileDropdown">
                                <?= Html::a('<i class="far fa-user-lock mr-2"></i> ' . Yii::t('jobseeker', 'Log in'), Yii::$app->urlManager->createUrl('jobseeker/my/login'), ['class' => 'dropdown-item']) ?>
                                <?= Html::a('<i class="far fa-user-plus mr-2"></i> ' . Yii::t('jobseeker', 'Sign Up'), Yii::$app->urlManager->createUrl('jobseeker/my/signup'), ['class' => 'dropdown-item']) ?>
                            </div>
                       </li>
                    </ul>
                </nav>
                <vr class="float-right"></vr>
                <nav class="navbar navbar-expand float-right">
                    <ul class="navbar-nav float-right">
                        <li class="nav-item">
                            <?= Html::a(Yii::t('jobseeker', 'Already registered?'), Yii::$app->urlManager->createUrl('jobseeker/my/login'), ['class' => 'nav-link--header nav-link--underline']) ?>
                        </li>
                        <li class="nav-item">
                            <?= Html::a('<i class="far fa-sign-in-alt"></i> ' . Yii::t('jobseeker', 'Log in'), Yii::$app->urlManager->createUrl('jobseeker/my/login'), ['class' => 'nav-link--header nav-link--underline']) ?>
                        </li>
                    </ul>
                </nav>
                
            </div>
            <div class=" d-block d-md-none d-lg-none py-1">
            	<nav class="d-none d-sm-block navbar navbar-expand float-left">
                    <ul class="navbar-nav">
                        <li class="nav-item mx-1">
                            <?= Html::a(Yii::t('jobseeker', 'Employer'), Yii::$app->urlManager->createUrl('employer'), ['class' => 'nav-link']) ?>
                        </li>
                    </ul>
                </nav>
                <vr class="float-left d-none d-sm-block"></vr>
                <nav class="navbar navbar-expand float-left py-2 pr-0">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown mx-1">
                            <?= $WidgetLanguages ?>
                        </li>
                    </ul>
                </nav>
                <nav class="navbar navbar-expand float-left py-2">
                    <ul class="navbar-nav">
                        <li class="nav-item mx-1">
                            <a class="nav-link--header" data-toggle="header-navbar-menu"><i class="far fa-bars fa-lg"></i></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </section> 
