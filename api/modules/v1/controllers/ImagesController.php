<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use common\models\Images;
/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class ImagesController extends ActiveController
{
    public $modelClass = 'common\models\Images'; 
    
    public function actionUpdateImages() {
        return Images::updateImages();
    }
}


