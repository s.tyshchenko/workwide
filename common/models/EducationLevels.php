<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class EducationLevels extends ActiveRecord {

    public static function tableName()
    {
        return '_education_levels';
    }
    
    public function fields()
    {
        return ['name', 'title'];
    }
    
    public function combineEducationLevels() {
        $return_array = [];
        foreach (self::find()->orderBy(['index_number' => SORT_ASC])->all() as $EducationLevel)
        {
            $return_array[$EducationLevel->name] = Yii::t('jobseeker', $EducationLevel->title); 
        }
        
        return $return_array;
    }
    
}