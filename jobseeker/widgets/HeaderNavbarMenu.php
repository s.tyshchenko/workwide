<?php

namespace jobseeker\widgets;

class HeaderNavbarMenu extends \yii\bootstrap4\Widget
{
    public function init(){}

    public function run() {
        return $this->render('header-navbar-menu/view');
    }
}