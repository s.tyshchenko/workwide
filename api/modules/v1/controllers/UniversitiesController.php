<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use api\modules\v1\models\Universities;

class UniversitiesController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\GeoDB'; 
    
    public function actions() {
        $actions = parent::actions();

        unset(
            $actions[ 'index' ],
            $actions[ 'view' ],
            $actions[ 'create' ],
            $actions[ 'update' ],
            $actions[ 'delete' ],
            $actions[ 'options' ]
        );


        return $actions;
    }

    public function actionCountry() {
        return Universities::getUniversitiesByCountry($_GET['countryCode'], $_GET['namePrefix']);
    }
    
    
}


