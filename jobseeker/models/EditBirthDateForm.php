<?php

namespace jobseeker\models;

use Yii;
use yii\base\Model;
use common\models\Jobseekers;

class EditBirthDateForm extends Model 
{
    public $birth_date; 
        
    public function rules()
    {
        return [
            ['birth_date', 'required'],
            ['birth_date', 'date', 'format' => 'php:d.m.Y']
        ];
    }
    
    public function editBirthDate() {
        if(!$this->validate()) {
            return false;
        }
        
        $jobseeker = Jobseekers::findOne(['user_id' => Yii::$app->user->getId()]);
        $jobseeker->birth_date = date('Y-m-d', strtotime($this->birth_date));
                
        return $jobseeker->save();
              
    }
}