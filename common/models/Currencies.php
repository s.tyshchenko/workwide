<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Currencies extends ActiveRecord {

    public static function tableName()
    {
        return '_currencies';
    }
    
    public function fields()
    {
        return ['code', 'title'];
    }
    
    public function combineCurrencies() {
        $return_array = [];
        foreach (self::find()->all() as $Currency)
        {
            $return_array[$Currency->code] = Yii::t('jobseeker', $Currency->title); 
        }
        
        return $return_array;
    }
}