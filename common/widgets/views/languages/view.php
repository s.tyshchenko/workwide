<?php
use yii\helpers\Html;
?>

<a class="nav-link--header nav-link--underline" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="far fa-globe"></i>
</a>
<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
    <?php foreach ($langs as $lang):?>
        <?= Html::a($lang->name, '/'.$lang->url.Yii::$app->getRequest()->getLangUrl(), ['class' => 'dropdown-item']) ?>
    <?php endforeach;?>
</div>
