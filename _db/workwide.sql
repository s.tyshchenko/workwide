-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Апр 19 2021 г., 08:07
-- Версия сервера: 5.7.33
-- Версия PHP: 7.3.27-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `workwide`
--

-- --------------------------------------------------------

--
-- Структура таблицы `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alt` text COLLATE utf8_unicode_ci,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `date_update` int(11) NOT NULL,
  `date_create` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `images`
--

INSERT INTO `images` (`id`, `title`, `alt`, `path`, `extension`, `date_update`, `date_create`) VALUES
(1, 'main-header-background', NULL, 'images/3b/39/8c641c0bb3d7fab6ad9e1466a2a3e2d4.png', 'png', 1607191972, 1607191972),
(2, 'main-header-background', NULL, 'images/d7/3b/8c641c0bb3d7fab6ad9e1466a2a3e2d4.webp', 'webp', 1607191973, 1607191973),
(3, 'profile-photo-empty', NULL, 'images/e6/da/3cca0513481175e40185647bdf6ff648.png', 'png', 1607210060, 1607210060),
(4, 'profile-photo-empty', NULL, 'images/e1/7f/3cca0513481175e40185647bdf6ff648.webp', 'webp', 1607210060, 1607210060);

-- --------------------------------------------------------

--
-- Структура таблицы `jobseekers`
--

CREATE TABLE `jobseekers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth_date` date DEFAULT NULL,
  `visibility` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'all',
  `timezone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `jobseekers`
--

INSERT INTO `jobseekers` (`id`, `user_id`, `full_name`, `birth_date`, `visibility`, `timezone`, `country`, `city`) VALUES
(1, 1, 'Stanislav Tyshchenko', '2000-05-17', 'users', 'Europe__Moscow', 'RU', '102740');

-- --------------------------------------------------------

--
-- Структура таблицы `jobseeker_additional_education`
--

CREATE TABLE `jobseeker_additional_education` (
  `id` int(11) NOT NULL,
  `jobseeker_id` int(11) NOT NULL,
  `educational_institution` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `specialization` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `education_start_date` date NOT NULL,
  `education_end_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `jobseeker_additional_education`
--

INSERT INTO `jobseeker_additional_education` (`id`, `jobseeker_id`, `educational_institution`, `specialization`, `country`, `description`, `education_start_date`, `education_end_date`) VALUES
(1, 1, 'Computer Academy ITStep', 'Web-Design', 'UA', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \r\n\r\nSuspendisse dolor lectus, vestibulum id tincidunt at, imperdiet ut augue. \r\n\r\nNullam mi tellus, lacinia vitae est a, dapibus pellentesque mi.', '2017-02-01', '2018-01-01');

-- --------------------------------------------------------

--
-- Структура таблицы `jobseeker_education`
--

CREATE TABLE `jobseeker_education` (
  `id` int(11) NOT NULL,
  `jobseeker_id` int(11) NOT NULL,
  `education_level` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `educational_institution` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `specialization` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `education_start_date` date NOT NULL,
  `education_end_date` date NOT NULL,
  `diploma_photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `jobseeker_education`
--

INSERT INTO `jobseeker_education` (`id`, `jobseeker_id`, `education_level`, `educational_institution`, `specialization`, `country`, `description`, `education_start_date`, `education_end_date`, `diploma_photo`) VALUES
(2, 1, 'associate', 'Universität Stuttgart', 'Finance, Banking and Insurance', 'DE', '', '2016-09-01', '2020-08-01', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `jobseeker_languages`
--

CREATE TABLE `jobseeker_languages` (
  `id` int(11) NOT NULL,
  `jobseeker_id` int(11) NOT NULL,
  `language` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `proficiency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `certificate_photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `jobseeker_languages`
--

INSERT INTO `jobseeker_languages` (`id`, `jobseeker_id`, `language`, `proficiency`, `certificate_photo`) VALUES
(2, 1, 'ukr', 'native', NULL),
(7, 1, 'rus', 'native', NULL),
(9, 1, 'eng', 'full_professional', NULL),
(12, 1, 'ger', 'full_professional', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `jobseeker_other_experience`
--

CREATE TABLE `jobseeker_other_experience` (
  `id` int(11) NOT NULL,
  `jobseeker_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `jobseeker_other_experience`
--

INSERT INTO `jobseeker_other_experience` (`id`, `jobseeker_id`, `title`, `description`) VALUES
(2, 1, 'UpWork Freelance Experience', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \r\n\r\nSuspendisse dolor lectus, vestibulum id tincidunt at, imperdiet ut augue. Nullam mi tellus, lacinia vitae est a, dapibus pellentesque mi. Nunc fringilla sit amet purus non euismod. Proin id sem facilisis turpis volutpat luctus.\r\n\r\nDonec fermentum nisi metus, id semper arcu egestas quis. Sed maximus, lectus ac feugiat ullamcorper, eros enim hendrerit magna, at vehicula sem augue non lorem. \r\nFusce a rhoncus tortor.');

-- --------------------------------------------------------

--
-- Структура таблицы `jobseeker_photos`
--

CREATE TABLE `jobseeker_photos` (
  `id` int(11) NOT NULL,
  `jobseeker_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path_webp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path_cropped` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `crop_x1` int(11) DEFAULT NULL,
  `crop_y1` int(11) DEFAULT NULL,
  `crop_width` int(11) DEFAULT NULL,
  `crop_height` int(11) DEFAULT NULL,
  `date_create` int(11) NOT NULL,
  `date_update` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `jobseeker_photos`
--

INSERT INTO `jobseeker_photos` (`id`, `jobseeker_id`, `path`, `extension`, `path_webp`, `path_cropped`, `crop_x1`, `crop_y1`, `crop_width`, `crop_height`, `date_create`, `date_update`) VALUES
(1, 1, '/images/d8/e1/522884c5446b0a6865c040e88251d590.jpg', 'jpg', '/images/c2/4f/522884c5446b0a6865c040e88251d590.webp', '/images/13/e3/39292558e014aef91771e32a048030b1.jpg', 843, 687, 798, 798, 1607207616, 1611171331);

-- --------------------------------------------------------

--
-- Структура таблицы `jobseeker_profiles`
--

CREATE TABLE `jobseeker_profiles` (
  `id` int(11) NOT NULL,
  `jobseeker_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `employment_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary` int(11) DEFAULT NULL,
  `salary_currency` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'usd',
  `specialization` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `soft_skills` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visibility` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'all',
  `is_general` tinyint(1) NOT NULL DEFAULT '0',
  `date_create` int(11) NOT NULL,
  `date_update` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `jobseeker_profiles`
--

INSERT INTO `jobseeker_profiles` (`id`, `jobseeker_id`, `title`, `description`, `employment_type`, `salary`, `salary_currency`, `specialization`, `categories`, `soft_skills`, `visibility`, `is_general`, `date_create`, `date_update`) VALUES
(1, 1, 'Accountant', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras facilisis pretium convallis. Aenean quis purus id felis laoreet tincidunt. Praesent neque erat, venenatis vitae ultrices non, faucibus et urna.', 'full-time, part-time, remote', 2360, 'EUR', '1', 'Fiduciary accounting, Auditing, Cost accounting, Public accounting', 'Responsibility, Creativity, Verbal communication', 'all', 1, 1606781194, 1607691043),
(3, 1, 'Financial Consultant ', '', 'full-time, part-time, remote', 2900, 'USD', '1', 'Financial accounting, Cost accounting, Auditing', '', 'all', 0, 1608252406, 1617628629);

-- --------------------------------------------------------

--
-- Структура таблицы `jobseeker_references`
--

CREATE TABLE `jobseeker_references` (
  `id` int(11) NOT NULL,
  `jobseeker_id` int(11) NOT NULL,
  `referrer_full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referrer_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referrer_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `jobseeker_work_experience`
--

CREATE TABLE `jobseeker_work_experience` (
  `id` int(11) NOT NULL,
  `jobseeker_id` int(11) NOT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_industry` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_start_date` int(11) NOT NULL,
  `work_end_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `jobseeker_work_experience`
--

INSERT INTO `jobseeker_work_experience` (`id`, `jobseeker_id`, `company_name`, `country`, `job_title`, `company_industry`, `description`, `work_start_date`, `work_end_date`) VALUES
(1, 1, 'OSCHADBANK', 'UA', 'Senior Financier of the Business Sector', '9', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.', 2020, 2020);

-- --------------------------------------------------------

--
-- Структура таблицы `profile_employment_types`
--

CREATE TABLE `profile_employment_types` (
  `id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `full_time` int(1) DEFAULT NULL,
  `part_time` int(1) DEFAULT NULL,
  `remote` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `profile_professional_skills`
--

CREATE TABLE `profile_professional_skills` (
  `id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `skill` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `proficiency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `years_of_experience` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `profile_professional_skills`
--

INSERT INTO `profile_professional_skills` (`id`, `profile_id`, `skill`, `proficiency`, `last_used`, `years_of_experience`) VALUES
(8, 3, '1C', 'Expert (recognized authority)', 'Currently using', 10),
(11, 3, 'Xero', 'Advanced (applied theory)', '< 1 Year ago', 4),
(13, 3, 'International Financial Reporting Standards', 'Expert (recognized authority)', '> 3 Years ago', 5),
(14, 3, 'FINSYNC', '', '1 Year ago', 4),
(15, 3, 'QuickBooks Online', '', '', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(2) NOT NULL,
  `role` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'jobseeker',
  `date_update` int(11) NOT NULL,
  `date_create` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `auth_key`, `password_hash`, `password_reset_token`, `verification_token`, `email`, `phone`, `status`, `role`, `date_update`, `date_create`) VALUES
(1, 'JP7IP-UVwqi3ULWaZHeq4IJMIZHwrwA0', '$2y$13$ExmGmwpVE4tdD6SkX9F/I.AYBAbfRM0XQWzW9LEYssXPbKvJOkrWW', NULL, 'G9V5Nl_lw6TIb7QjhQCprS__xehwMuuV_1604611113', 'tishenko17@icloud.com', '+380952206941', 10, 'jobseeker', 1606086012, 1604611113);

-- --------------------------------------------------------

--
-- Структура таблицы `_app_languages`
--

CREATE TABLE `_app_languages` (
  `id` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `local` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default` smallint(6) NOT NULL DEFAULT '0',
  `date_update` int(11) NOT NULL,
  `date_create` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `_app_languages`
--

INSERT INTO `_app_languages` (`id`, `url`, `local`, `name`, `default`, `date_update`, `date_create`) VALUES
(1, 'en', 'en-EN', 'English', 1, 1603211129, 1603211129),
(2, 'ru', 'ru-RU', 'Русский', 0, 1603211129, 1603211129),
(3, 'ua', 'ua-UA', 'Українська', 0, 1603211344, 1603211344);

-- --------------------------------------------------------

--
-- Структура таблицы `_countries`
--

CREATE TABLE `_countries` (
  `code` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `_countries`
--

INSERT INTO `_countries` (`code`, `title`) VALUES
('DE', 'Germany'),
('RU', 'Russia'),
('UA', 'Ukraine');

-- --------------------------------------------------------

--
-- Структура таблицы `_currencies`
--

CREATE TABLE `_currencies` (
  `code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `_currencies`
--

INSERT INTO `_currencies` (`code`, `title`) VALUES
('EUR', 'EUR (Euro)'),
('USD', 'USD (United States Dollars)');

-- --------------------------------------------------------

--
-- Структура таблицы `_education_levels`
--

CREATE TABLE `_education_levels` (
  `index_number` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `_education_levels`
--

INSERT INTO `_education_levels` (`index_number`, `name`, `title`) VALUES
(1, 'associate', 'Associate\'s Degree'),
(2, 'bachelor', 'Bachelor\'s Degree'),
(4, 'doctoral', 'Doctoral Degree'),
(3, 'master', 'Master\'s Degree');

-- --------------------------------------------------------

--
-- Структура таблицы `_employment_types`
--

CREATE TABLE `_employment_types` (
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `_employment_types`
--

INSERT INTO `_employment_types` (`name`, `title`) VALUES
('full-time', 'Full-time'),
('part-time', 'Part-time'),
('remote', 'Remote');

-- --------------------------------------------------------

--
-- Структура таблицы `_i18n_languages`
--

CREATE TABLE `_i18n_languages` (
  `id` char(3) NOT NULL COMMENT 'ISO 639-2 Code',
  `iso639_1` varchar(2) DEFAULT NULL COMMENT 'ISO 639-1 Code',
  `english_name` varchar(255) DEFAULT NULL,
  `french_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `_i18n_languages`
--

INSERT INTO `_i18n_languages` (`id`, `iso639_1`, `english_name`, `french_name`) VALUES
('aar', 'aa', 'Afar', 'afar'),
('abk', 'ab', 'Abkhazian', 'abkhaze'),
('afr', 'af', 'Afrikaans', 'afrikaans'),
('aka', 'ak', 'Akan', 'akan'),
('alb', 'sq', 'Albanian', 'albanais'),
('amh', 'am', 'Amharic', 'amharique'),
('ara', 'ar', 'Arabic', 'arabe'),
('arg', 'an', 'Aragonese', 'aragonais'),
('arm', 'hy', 'Armenian', 'arménien'),
('asm', 'as', 'Assamese', 'assamais'),
('ava', 'av', 'Avaric', 'avar'),
('ave', 'ae', 'Avestan', 'avestique'),
('aym', 'ay', 'Aymara', 'aymara'),
('aze', 'az', 'Azerbaijani', 'azéri'),
('bak', 'ba', 'Bashkir', 'bachkir'),
('bam', 'bm', 'Bambara', 'bambara'),
('baq', 'eu', 'Basque', 'basque'),
('bel', 'be', 'Belarusian', 'biélorusse'),
('ben', 'bn', 'Bengali', 'bengali'),
('bih', 'bh', 'Bihari', 'bihari'),
('bis', 'bi', 'Bislama', 'bichlamar'),
('bos', 'bs', 'Bosnian', 'bosniaque'),
('bre', 'br', 'Breton', 'breton'),
('bul', 'bg', 'Bulgarian', 'bulgare'),
('bur', 'my', 'Burmese', 'birman'),
('cat', 'ca', 'Catalan; Valencian', 'catalan; valencien'),
('cha', 'ch', 'Chamorro', 'chamorro'),
('che', 'ce', 'Chechen', 'tchétchène'),
('chi', 'zh', 'Chinese', 'chinois'),
('chu', 'cu', 'Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic', 'slavon d\'église; vieux slave; slavon liturgique; vieux bulgare'),
('chv', 'cv', 'Chuvash', 'tchouvache'),
('cor', 'kw', 'Cornish', 'cornique'),
('cos', 'co', 'Corsican', 'corse'),
('cre', 'cr', 'Cree', 'cree'),
('cze', 'cs', 'Czech', 'tchèque'),
('dan', 'da', 'Danish', 'danois'),
('div', 'dv', 'Divehi; Dhivehi; Maldivian', 'maldivien'),
('dut', 'nl', 'Dutch; Flemish', 'néerlandais; flamand'),
('dzo', 'dz', 'Dzongkha', 'dzongkha'),
('eng', 'en', 'English', 'anglais'),
('epo', 'eo', 'Esperanto', 'espéranto'),
('est', 'et', 'Estonian', 'estonien'),
('ewe', 'ee', 'Ewe', 'éwé'),
('fao', 'fo', 'Faroese', 'féroïen'),
('fij', 'fj', 'Fijian', 'fidjien'),
('fin', 'fi', 'Finnish', 'finnois'),
('fre', 'fr', 'French', 'français'),
('fry', 'fy', 'Western Frisian', 'frison occidental'),
('ful', 'ff', 'Fulah', 'peul'),
('geo', 'ka', 'Georgian', 'géorgien'),
('ger', 'de', 'German', 'allemand'),
('gla', 'gd', 'Gaelic; Scottish Gaelic', 'gaélique; gaélique écossais'),
('gle', 'ga', 'Irish', 'irlandais'),
('glg', 'gl', 'Galician', 'galicien'),
('glv', 'gv', 'Manx', 'manx; mannois'),
('gre', 'el', 'Greek, Modern (1453-)', 'grec moderne (après 1453)'),
('grn', 'gn', 'Guarani', 'guarani'),
('guj', 'gu', 'Gujarati', 'goudjrati'),
('hat', 'ht', 'Haitian; Haitian Creole', 'haïtien; créole haïtien'),
('hau', 'ha', 'Hausa', 'haoussa'),
('heb', 'he', 'Hebrew', 'hébreu'),
('her', 'hz', 'Herero', 'herero'),
('hin', 'hi', 'Hindi', 'hindi'),
('hmo', 'ho', 'Hiri Motu', 'hiri motu'),
('hrv', 'hr', 'Croatian', 'croate'),
('hun', 'hu', 'Hungarian', 'hongrois'),
('ibo', 'ig', 'Igbo', 'igbo'),
('ice', 'is', 'Icelandic', 'islandais'),
('ido', 'io', 'Ido', 'ido'),
('iii', 'ii', 'Sichuan Yi; Nuosu', 'yi de Sichuan'),
('iku', 'iu', 'Inuktitut', 'inuktitut'),
('ile', 'ie', 'Interlingue; Occidental', 'interlingue'),
('ina', 'ia', 'Interlingua (International Auxiliary Language Association)', 'interlingua (langue auxiliaire internationale)'),
('ind', 'id', 'Indonesian', 'indonésien'),
('ipk', 'ik', 'Inupiaq', 'inupiaq'),
('ita', 'it', 'Italian', 'italien'),
('jav', 'jv', 'Javanese', 'javanais'),
('jpn', 'ja', 'Japanese', 'japonais'),
('kal', 'kl', 'Kalaallisut; Greenlandic', 'groenlandais'),
('kan', 'kn', 'Kannada', 'kannada'),
('kas', 'ks', 'Kashmiri', 'kashmiri'),
('kau', 'kr', 'Kanuri', 'kanouri'),
('kaz', 'kk', 'Kazakh', 'kazakh'),
('khm', 'km', 'Central Khmer', 'khmer central'),
('kik', 'ki', 'Kikuyu; Gikuyu', 'kikuyu'),
('kin', 'rw', 'Kinyarwanda', 'rwanda'),
('kir', 'ky', 'Kirghiz; Kyrgyz', 'kirghiz'),
('kom', 'kv', 'Komi', 'kom'),
('kon', 'kg', 'Kongo', 'kongo'),
('kor', 'ko', 'Korean', 'coréen'),
('kua', 'kj', 'Kuanyama; Kwanyama', 'kuanyama; kwanyama'),
('kur', 'ku', 'Kurdish', 'kurde'),
('lao', 'lo', 'Lao', 'lao'),
('lat', 'la', 'Latin', 'latin'),
('lav', 'lv', 'Latvian', 'letton'),
('lim', 'li', 'Limburgan; Limburger; Limburgish', 'limbourgeois'),
('lin', 'ln', 'Lingala', 'lingala'),
('lit', 'lt', 'Lithuanian', 'lituanien'),
('ltz', 'lb', 'Luxembourgish; Letzeburgesch', 'luxembourgeois'),
('lub', 'lu', 'Luba-Katanga', 'luba-katanga'),
('lug', 'lg', 'Ganda', 'ganda'),
('mac', 'mk', 'Macedonian', 'macédonien'),
('mah', 'mh', 'Marshallese', 'marshall'),
('mal', 'ml', 'Malayalam', 'malayalam'),
('mao', 'mi', 'Maori', 'maori'),
('mar', 'mr', 'Marathi', 'marathe'),
('may', 'ms', 'Malay', 'malais'),
('mlg', 'mg', 'Malagasy', 'malgache'),
('mlt', 'mt', 'Maltese', 'maltais'),
('mon', 'mn', 'Mongolian', 'mongol'),
('nau', 'na', 'Nauru', 'nauruan'),
('nav', 'nv', 'Navajo; Navaho', 'navaho'),
('nbl', 'nr', 'Ndebele, South; South Ndebele', 'ndébélé du Sud'),
('nde', 'nd', 'Ndebele, North; North Ndebele', 'ndébélé du Nord'),
('ndo', 'ng', 'Ndonga', 'ndonga'),
('nep', 'ne', 'Nepali', 'népalais'),
('nno', 'nn', 'Norwegian Nynorsk; Nynorsk, Norwegian', 'norvégien nynorsk; nynorsk, norvégien'),
('nob', 'nb', 'Bokmål, Norwegian; Norwegian Bokmål', 'norvégien bokmål'),
('nor', 'no', 'Norwegian', 'norvégien'),
('nya', 'ny', 'Chichewa; Chewa; Nyanja', 'chichewa; chewa; nyanja'),
('oci', 'oc', 'Occitan (post 1500)', 'occitan (après 1500)'),
('oji', 'oj', 'Ojibwa', 'ojibwa'),
('ori', 'or', 'Oriya', 'oriya'),
('orm', 'om', 'Oromo', 'galla'),
('oss', 'os', 'Ossetian; Ossetic', 'ossète'),
('pan', 'pa', 'Panjabi; Punjabi', 'pendjabi'),
('per', 'fa', 'Persian', 'persan'),
('pli', 'pi', 'Pali', 'pali'),
('pol', 'pl', 'Polish', 'polonais'),
('por', 'pt', 'Portuguese', 'portugais'),
('pus', 'ps', 'Pushto; Pashto', 'pachto'),
('que', 'qu', 'Quechua', 'quechua'),
('roh', 'rm', 'Romansh', 'romanche'),
('rum', 'ro', 'Romanian; Moldavian; Moldovan', 'roumain; moldave'),
('run', 'rn', 'Rundi', 'rundi'),
('rus', 'ru', 'Russian', 'russe'),
('sag', 'sg', 'Sango', 'sango'),
('san', 'sa', 'Sanskrit', 'sanskrit'),
('sin', 'si', 'Sinhala; Sinhalese', 'singhalais'),
('slo', 'sk', 'Slovak', 'slovaque'),
('slv', 'sl', 'Slovenian', 'slovène'),
('sme', 'se', 'Northern Sami', 'sami du Nord'),
('smo', 'sm', 'Samoan', 'samoan'),
('sna', 'sn', 'Shona', 'shona'),
('snd', 'sd', 'Sindhi', 'sindhi'),
('som', 'so', 'Somali', 'somali'),
('sot', 'st', 'Sotho, Southern', 'sotho du Sud'),
('spa', 'es', 'Spanish; Castilian', 'espagnol; castillan'),
('srd', 'sc', 'Sardinian', 'sarde'),
('srp', 'sr', 'Serbian', 'serbe'),
('ssw', 'ss', 'Swati', 'swati'),
('sun', 'su', 'Sundanese', 'soundanais'),
('swa', 'sw', 'Swahili', 'swahili'),
('swe', 'sv', 'Swedish', 'suédois'),
('tah', 'ty', 'Tahitian', 'tahitien'),
('tam', 'ta', 'Tamil', 'tamoul'),
('tat', 'tt', 'Tatar', 'tatar'),
('tel', 'te', 'Telugu', 'télougou'),
('tgk', 'tg', 'Tajik', 'tadjik'),
('tgl', 'tl', 'Tagalog', 'tagalog'),
('tha', 'th', 'Thai', 'thaï'),
('tib', 'bo', 'Tibetan', 'tibétain'),
('tir', 'ti', 'Tigrinya', 'tigrigna'),
('ton', 'to', 'Tonga (Tonga Islands)', 'tongan (Îles Tonga)'),
('tsn', 'tn', 'Tswana', 'tswana'),
('tso', 'ts', 'Tsonga', 'tsonga'),
('tuk', 'tk', 'Turkmen', 'turkmène'),
('tur', 'tr', 'Turkish', 'turc'),
('twi', 'tw', 'Twi', 'twi'),
('uig', 'ug', 'Uighur; Uyghur', 'ouïgour'),
('ukr', 'uk', 'Ukrainian', 'ukrainien'),
('urd', 'ur', 'Urdu', 'ourdou'),
('uzb', 'uz', 'Uzbek', 'ouszbek'),
('ven', 've', 'Venda', 'venda'),
('vie', 'vi', 'Vietnamese', 'vietnamien'),
('vol', 'vo', 'Volapük', 'volapük'),
('wel', 'cy', 'Welsh', 'gallois'),
('wln', 'wa', 'Walloon', 'wallon'),
('wol', 'wo', 'Wolof', 'wolof'),
('xho', 'xh', 'Xhosa', 'xhosa'),
('yid', 'yi', 'Yiddish', 'yiddish'),
('yor', 'yo', 'Yoruba', 'yoruba'),
('zha', 'za', 'Zhuang; Chuang', 'zhuang; chuang'),
('zul', 'zu', 'Zulu', 'zoulou');

-- --------------------------------------------------------

--
-- Структура таблицы `_language_proficiency`
--

CREATE TABLE `_language_proficiency` (
  `index_number` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `_language_proficiency`
--

INSERT INTO `_language_proficiency` (`index_number`, `name`, `title`) VALUES
(1, 'elementary', 'Elementary'),
(4, 'full_professional', 'Full Professional'),
(2, 'limited_working', 'Limited Working'),
(5, 'native', 'Native / Bilingual'),
(3, 'professional_working', 'Professional Working');

-- --------------------------------------------------------

--
-- Структура таблицы `_profile_visibility`
--

CREATE TABLE `_profile_visibility` (
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `_profile_visibility`
--

INSERT INTO `_profile_visibility` (`name`, `title`) VALUES
('all', 'Everyone'),
('none', 'Nobody\r\n'),
('users', 'Only WorkWide users');

-- --------------------------------------------------------

--
-- Структура таблицы `_skill_last_used`
--

CREATE TABLE `_skill_last_used` (
  `index_number` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `_skill_last_used`
--

INSERT INTO `_skill_last_used` (`index_number`, `title`) VALUES
(1, 'Currently using'),
(2, '< 1 Year ago'),
(3, '1 Year ago'),
(4, '> 1 Year ago'),
(5, '2 Years ago'),
(6, '3 Years ago'),
(7, '> 3 Years ago');

-- --------------------------------------------------------

--
-- Структура таблицы `_skill_proficiency`
--

CREATE TABLE `_skill_proficiency` (
  `index_number` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `_skill_proficiency`
--

INSERT INTO `_skill_proficiency` (`index_number`, `title`, `description`) VALUES
(4, 'Advanced (applied theory)', 'A person perform the actions associated with this skill without assistance. '),
(5, 'Expert (recognized authority)', 'The person is known as a specialist in this field. He / she can provide guidance, troubleshoot, and answer questions related to that area of expertise and the area in which the skill is used.'),
(1, 'Fundamental Awareness (basic knowledge)', 'The person has a general knowledge or understanding of basic techniques and concepts.'),
(3, 'Intermediate (practical application)', 'A person can successfully complete tasks in this competency upon request.'),
(2, 'Novice (limited experience)', 'The individual has experience gained in class and/or in experimental scenarios, or as a trainee in the workplace.');

-- --------------------------------------------------------

--
-- Структура таблицы `_soft_skills`
--

CREATE TABLE `_soft_skills` (
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `_soft_skills`
--

INSERT INTO `_soft_skills` (`title`, `description`) VALUES
('Active listening', 'Active listening is the ability to focus completely on a speaker, understand their message, comprehend the information and respond thoughtfully.'),
('Acuity', NULL),
('Agility', NULL),
('Analysis', 'Analytical skills are the traits and abilities that allow you to observe, research and interpret a subject in order to develop complex ideas and solutions.'),
('Attention to detail', 'Being detail-oriented means paying close attention to all of the small particulars when working on a task or project. Detail-oriented employees complete each assignment as flawlessly as possible before moving to the next task.'),
('Authenticity', NULL),
('Brainstorming', NULL),
('Calmness', NULL),
('Clarity', NULL),
('Collaboration', NULL),
('Commitment', NULL),
('Confidence', 'Confidence is your belief or trust in something. In the workplace, it can refer to the belief you have in yourself to carry out your job and the belief you have in your own abilities.'),
('Conflict management', NULL),
('Conflict resolution', 'Conflict resolution is the art of addressing those differences and finding common ground that enables everyone to work together peacefully.'),
('Consistency', NULL),
('Constructive feedback', NULL),
('Cooperation', NULL),
('Coordination', NULL),
('Coping', NULL),
('Creativity', 'Creativity is the ability to think about a task or a problem in a new or different way, or the ability to use the imagination to generate new ideas. Creativity enables you to solve complex problems or find interesting ways to approach tasks. '),
('Critical observation', NULL),
('Critical thinking', 'Critical thinking is the act of analyzing facts to understand a problem or topic thoroughly. The critical thinking process typically includes steps like collecting information and data, asking thoughtful questions and analyzing possible solutions. '),
('Cultural intelligence', NULL),
('Curiosity', NULL),
('Decision making', NULL),
('Decision-making', 'Decision-making skills show your proficiency in choosing between two or more alternatives.'),
('Delegation', NULL),
('Dependability', NULL),
('Design', NULL),
('Diplomacy', NULL),
('Discipline', NULL),
('Divergent thinking', NULL),
('Empathy', 'Empathy is one’s ability to understand someone else’s emotions and state of mind.'),
('Enthusiasm', NULL),
('Experimentation', NULL),
('Experimenting', NULL),
('Flexibility', 'The word “flexibility” technically means the ability to bend without breaking. Being flexible in life means that you can change your plans and adapt to new situations easily.'),
('Focus', NULL),
('Friendliness', NULL),
('Generosity', NULL),
('Goal setting', NULL),
('Growth mindset', NULL),
('Humility', NULL),
('Humor', NULL),
('Idea exchange', NULL),
('Imagination', NULL),
('Initiative', NULL),
('Innovation', NULL),
('Insight', NULL),
('Inspiration', NULL),
('Integrity', 'Workplace integrity is a set of core values and attributes that guide you to be honest, trustworthy, dependable and use good judgment in your work.'),
('Lateral thinking', NULL),
('Learning from others', NULL),
('Listening', NULL),
('Logical reasoning', NULL),
('Mediation', NULL),
('Memory', NULL),
('Mentoring', NULL),
('Mind mapping', NULL),
('Motivation', NULL),
('Negotiating', NULL),
('Negotiation', 'Negotiation skills are qualities that allow two or more parties to reach a compromise. These are often soft skills and include abilities such as communication, persuasion, planning, strategizing and cooperating.'),
('Networking', NULL),
('Non-verbal communication', 'Nonverbal communication is the transfer of information through the use of body language including eye contact, facial expressions, gestures and more. For example, smiling when you meet someone conveys friendliness, acceptance and openness.'),
('Observation', NULL),
('Open-mindedness', NULL),
('Optimism', NULL),
('Organization', 'When an employee displays strong organizational skills in the workplace, it typically means they also have a strong aptitude for time management, goal setting and understanding how to meet their objectives.'),
('Patience', NULL),
('Perseverance', NULL),
('Persistence', NULL),
('Persuasion', NULL),
('Planning', NULL),
('Positive reinforcement', NULL),
('Prioritizing', NULL),
('Professionalism', NULL),
('Project management', NULL),
('Public speaking', NULL),
('Questioning', NULL),
('Recall', NULL),
('Reframing', NULL),
('Research', 'Research skills are our ability to find an answer to a question or a solution to a problem. Research skills include the ability to gather information about your topic, review that information and analyze it in a manner that brings us to a solution.'),
('Resourcefulness', NULL),
('Respect', NULL),
('Responsibility', NULL),
('Results-oriented', NULL),
('Risk management', 'Risk management is the process in which a business, financial manager or individual identifies, evaluates and prioritizes risks and then formulates a plan to minimize the impact of those risks.'),
('Self-confidence', NULL),
('Self-management', NULL),
('Self-motivation', NULL),
('Self-starter', NULL),
('Selflessness', NULL),
('Sensitivity', NULL),
('Stress management', NULL),
('Taking calculated risks', NULL),
('Teamwork', 'Teamwork skills are the qualities and abilities that allow you to work well with others during conversations, projects, meetings or other collaborations. '),
('Time-management', NULL),
('Tolerance', NULL),
('Troubleshooting', NULL),
('Trust', NULL),
('Verbal communication', NULL),
('Versatility', NULL),
('Writing', 'Writing skills include all the knowledge and abilities related to expressing ideas through the written word.'),
('Written communication', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `_specializations`
--

CREATE TABLE `_specializations` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `_specializations`
--

INSERT INTO `_specializations` (`id`, `title`) VALUES
(1, 'Accounting & auditing'),
(2, 'Administration & middle management'),
(3, 'Agriculture & agribusiness'),
(4, 'Beauty, fitness & sports'),
(5, 'Construction & architecture'),
(6, 'Culture, music & entertainment'),
(7, 'Design & creativity'),
(8, 'Education & science'),
(9, 'Finance & banking'),
(10, 'Hotels, restaurants & tourism'),
(11, 'HR & personnel management'),
(12, 'Insurance'),
(13, 'IT, computers & Internet'),
(14, 'Journalism, publishing & printing'),
(15, 'Law'),
(16, 'Logistics, warehouse & international commerce'),
(17, 'Marketing, advertising & PR'),
(18, 'Medicine & pharmaceuticals '),
(19, 'Real estate'),
(20, 'Retail'),
(21, 'Sales & procurement'),
(22, 'Secretarial, clerical & administrative assistants'),
(23, 'Security & guarding'),
(24, 'Service sector'),
(25, 'Skilled trades & manufacturing'),
(26, 'Telecommunications'),
(27, 'Transportation & auto industry'),
(28, 'Upper & senior management');

-- --------------------------------------------------------

--
-- Структура таблицы `_specialization_categories`
--

CREATE TABLE `_specialization_categories` (
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `specialization_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `_specialization_categories`
--

INSERT INTO `_specialization_categories` (`title`, `specialization_id`) VALUES
('Accounting information systems', 1),
('Auditing', 1),
('Cost accounting', 1),
('Fiduciary accounting', 1),
('Financial accounting', 1),
('Forensic accounting', 1),
('Government accounting', 1),
('Managerial accounting', 1),
('Public accounting', 1),
('Tax accounting', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `_specialization_skills`
--

CREATE TABLE `_specialization_skills` (
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `specialization_id` int(11) DEFAULT NULL,
  `group_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `_specialization_skills`
--

INSERT INTO `_specialization_skills` (`title`, `specialization_id`, `group_title`, `description`) VALUES
('GAAP', 1, 'Standards of accounting', NULL),
('International Financial Reporting Standards', 1, 'Standards of accounting', NULL),
('Pro Forma', 1, 'Standards of accounting', NULL),
('Xero', 1, 'Software skills', NULL),
('FINSYNC', 1, 'Software skills', NULL),
('QuickBooks Online', 1, 'Software skills', NULL),
('Honorable Mention', 1, 'Software skills', NULL),
('1C', 1, 'Software skills', NULL),
('Google Docs', 0, 'Computer skills', NULL),
('Google Sheets', 0, 'Computer skills', NULL),
('Google Slides', 0, 'Computer skills', NULL),
('IMB DB2', 0, 'Computer skills', NULL),
('MS Access', 0, 'Computer skills', NULL),
('MS Excel', 0, 'Computer skills', NULL),
('MS OneNote', 0, 'Computer skills', NULL),
('MS Outlook', 0, 'Computer skills', NULL),
('MS Powerpoint', 0, 'Computer skills', NULL),
('MS Word', 0, 'Computer skills', NULL),
('MySQL', 0, 'Computer skills', NULL),
('OpenOffice Calc', 0, 'Computer skills', NULL),
('OpenOffice Impress', 0, 'Computer skills', NULL),
('Oracle', 0, 'Computer skills', NULL),
('SQL', 0, 'Computer skills', NULL),
('Tableu', 0, 'Computer skills', NULL),
('Very small (1-9 employees)', 0, 'Business Size Experience', NULL),
('Small (10-99 employees)', 0, 'Business Size Experience', NULL),
('Mid (100-999 employees)', 0, 'Business Size Experience', NULL),
('Large (1000+)', 0, 'Business Size Experience', NULL),
('Startup', 0, 'Business Size Experience', NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `jobseekers`
--
ALTER TABLE `jobseekers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `jobseeker_additional_education`
--
ALTER TABLE `jobseeker_additional_education`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `jobseeker_education`
--
ALTER TABLE `jobseeker_education`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `jobseeker_languages`
--
ALTER TABLE `jobseeker_languages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `jobseeker_other_experience`
--
ALTER TABLE `jobseeker_other_experience`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `jobseeker_photos`
--
ALTER TABLE `jobseeker_photos`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `jobseeker_profiles`
--
ALTER TABLE `jobseeker_profiles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `jobseeker_references`
--
ALTER TABLE `jobseeker_references`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `jobseeker_work_experience`
--
ALTER TABLE `jobseeker_work_experience`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `profile_employment_types`
--
ALTER TABLE `profile_employment_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `profile_professional_skills`
--
ALTER TABLE `profile_professional_skills`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `_app_languages`
--
ALTER TABLE `_app_languages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url` (`url`);

--
-- Индексы таблицы `_countries`
--
ALTER TABLE `_countries`
  ADD PRIMARY KEY (`code`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Индексы таблицы `_currencies`
--
ALTER TABLE `_currencies`
  ADD PRIMARY KEY (`code`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Индексы таблицы `_education_levels`
--
ALTER TABLE `_education_levels`
  ADD PRIMARY KEY (`name`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `_employment_types`
--
ALTER TABLE `_employment_types`
  ADD PRIMARY KEY (`name`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `_i18n_languages`
--
ALTER TABLE `_i18n_languages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `2letter` (`iso639_1`);

--
-- Индексы таблицы `_language_proficiency`
--
ALTER TABLE `_language_proficiency`
  ADD PRIMARY KEY (`name`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `_profile_visibility`
--
ALTER TABLE `_profile_visibility`
  ADD PRIMARY KEY (`name`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `_skill_last_used`
--
ALTER TABLE `_skill_last_used`
  ADD PRIMARY KEY (`title`),
  ADD UNIQUE KEY `index_number` (`index_number`);

--
-- Индексы таблицы `_skill_proficiency`
--
ALTER TABLE `_skill_proficiency`
  ADD PRIMARY KEY (`title`),
  ADD UNIQUE KEY `title` (`title`);

--
-- Индексы таблицы `_soft_skills`
--
ALTER TABLE `_soft_skills`
  ADD PRIMARY KEY (`title`),
  ADD UNIQUE KEY `title` (`title`);

--
-- Индексы таблицы `_specializations`
--
ALTER TABLE `_specializations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `_specialization_categories`
--
ALTER TABLE `_specialization_categories`
  ADD PRIMARY KEY (`title`),
  ADD UNIQUE KEY `title` (`title`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `jobseekers`
--
ALTER TABLE `jobseekers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `jobseeker_additional_education`
--
ALTER TABLE `jobseeker_additional_education`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `jobseeker_education`
--
ALTER TABLE `jobseeker_education`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `jobseeker_languages`
--
ALTER TABLE `jobseeker_languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `jobseeker_other_experience`
--
ALTER TABLE `jobseeker_other_experience`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `jobseeker_photos`
--
ALTER TABLE `jobseeker_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `jobseeker_profiles`
--
ALTER TABLE `jobseeker_profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `jobseeker_references`
--
ALTER TABLE `jobseeker_references`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `jobseeker_work_experience`
--
ALTER TABLE `jobseeker_work_experience`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `profile_employment_types`
--
ALTER TABLE `profile_employment_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `profile_professional_skills`
--
ALTER TABLE `profile_professional_skills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `_app_languages`
--
ALTER TABLE `_app_languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `_specializations`
--
ALTER TABLE `_specializations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
