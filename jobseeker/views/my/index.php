<?php

use yii\widgets\ActiveForm;

?>

<section id="jobseeker-signup">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <h3><?= Yii::t('jobseeker', 'Find Work') ?></h3>
            </div>
            <div class="col-md-8">
                <?php 
                    $searchForm = ActiveForm::begin([
                        'id' => 'jobseeker-search-form',
                        'options' => ['id' => 'search_block', 'class' => 'col-md-8 col-left'],
                    ]);
                ?>
                <?= $searchForm->field($searchFormModel, 'keywords', ['addAriaAttributes' => true, 'template' => "{input}", 'inputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('jobseeker', 'Search for jobs')] ]) ?>
                <?php 
                    ActiveForm::end();
                ?>
            </div>
        </div>
    </div>
</section>