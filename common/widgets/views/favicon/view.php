<?php
use yii\helpers\Url;
?>
<link rel="apple-touch-icon" sizes="180x180" href="<?= Url::to('/images/icons/apple-touch-icon.png', true) ?>">
<link rel="icon" type="image/png" sizes="32x32" href="<?= Url::to('images/icons/favicon-32x32.png', true) ?>">
<link rel="icon" type="image/png" sizes="16x16" href="<?= Url::to('/images/icons/favicon-16x16.png', true) ?>">
<link rel="manifest" href="<?= Url::to('/images/icons/site.webmanifest', true) ?>">
<!--<link rel="mask-icon" href="<? //Url::to('/images/icons/safari-pinned-tab.svg', true) ?>" color="#1c1c1e">-->
<link rel="shortcut icon" href="<?= Url::to('/images/icons/favicon.ico', true) ?>">
<!--<meta name="msapplication-TileColor" content="#1c1c1e">-->
<meta name="msapplication-config" content="<?= Url::to('/images/icons/browserconfig.xml', true) ?>">
<meta name="theme-color" content="#ffffff">

