<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class EmploymentTypes extends ActiveRecord {

    public static function tableName()
    {
        return '_employment_types';
    }
    
    public function fields()
    {
        return ['name', 'title'];
    }
    
    public function combineEmploymentTypes() {
        $return_array = [];
        foreach (self::find()->all() as $EmploymentType)
        {
            $return_array[$EmploymentType->name] = Yii::t('jobseeker', $EmploymentType->title); 
        }
        
        return $return_array;
    }
}