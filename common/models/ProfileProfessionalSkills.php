<?php

namespace common\models;

use Yii;
use common\models\JobseekerProfiles;
use yii\db\ActiveRecord;

class ProfileProfessionalSkills extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile_professional_skills';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['profile_id','skill'], 'required'],
            ['skill', 'string'],
            ['proficiency', 'string'],
            ['last_used', 'string'],
            ['years_of_experience', 'integer'],
        ];
    }
    
    public function getJobseekerProfile()
    {
        return $this->hasOne(JobseekerProfiles::className(), ['id' => 'profile_id']);
    }
    
}