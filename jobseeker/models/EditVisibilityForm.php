<?php

namespace jobseeker\models;

use Yii;
use yii\base\Model;
use common\models\Jobseekers;

class EditVisibilityForm extends Model 
{
    public $visibility; 
        
    public function rules()
    {
        return [
            ['visibility', 'required'],
        ];
    }
    
    public function editVisibility() {
        if(!$this->validate()) {
            return false;
        }
        
        $jobseeker = Jobseekers::findOne(['user_id' => Yii::$app->user->getId()]);
        $jobseeker->visibility = $this->visibility;
        
        return $jobseeker->save();
    }
}