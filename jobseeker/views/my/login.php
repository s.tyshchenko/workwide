<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use borales\extensions\phoneInput\PhoneInput;

$this->title = Yii::t('jobseeker', 'Sign Up as Jobseeker');

?>

<section id="jobseeker-signup">
    <div class="container py-5">
        <h1><?= Yii::t('jobseeker', 'Authorization') ?></h1>
        <p class="text-secondary"><?= Yii::t('jobseeker', "Authorize and get access to the vacancies worldwide") ?></p>
        
        <div class="row">
                <?php 
                    
                    $loginForm = ActiveForm::begin([
                        'id' => 'user-login-form',
                        'options' => ['id' => 'signup_block', 'class' => 'col-md-8 col-left px-4 py-4'],
                    ]);
                ?>
                    
                <h2 class="mb-4">
                    <?= Yii::t('jobseeker', 'Your login details') ?>
                </h2>
    
                <div class="">
                <?= $loginForm->field($loginFormModel, 'login', ['template' => "{label}\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'form-control placeholder-animated'] ])->label(Yii::t('jobseeker', 'Email or Phone Number'), ['class' => 'form-control-label h5 text-main-2']) ?>
                </div>
        
                <div class="">
                <?= $loginForm->field($loginFormModel, 'password', ['template' => "{label}\n{input}\n{error}", 'inputOptions' => ['class' => 'form-control placeholder-animated', 'type' => 'password'] ])->label(Yii::t('jobseeker', 'Password'), ['class' => 'form-control-label h5 text-main-2']) ?>
                </div>
            
    
                <div class="col-xs-12 col-sm-8 col-md-2 col-lg-2 px-0">
                     <?= Html::submitButton(Yii::t('jobseeker', 'Log in'), ['name' => 'login-button']) ?>
                </div>
                <?php 
                    ActiveForm::end();
                ?>
            <div class="col-md-4 col-right">
                <h5 class="text-main pl-4">
                    <?= Yii::t('jobseeker','Forgot your password?') ?>
                </h5>
                <ul class="text-blue pl-4 list-unstyled">
                    <li>
                        <i class="fad fa-circle mr-2"></i>
                        <?= Html::a(Yii::t('jobseeker','Restore password'), Yii::$app->urlManager->createUrl('jobseeker/my/login'))  ?></a>
                    </li>
                </ul>
                
                <h5 class="text-main pl-4">
                   <?= Yii::t('jobseeker','Still not a member?') ?>
                </h5>
                <ul class="text-blue pl-4 list-unstyled">
                    <li>
                        <i class="fad fa-circle mr-2"></i>
                        <?= Html::a(Yii::t('jobseeker','Register'), Yii::$app->urlManager->createUrl('jobseeker/my/signup')) ?>
                    </li>
                </ul>
                
            </div>
        </div>
    </div>
</section>