<?php

namespace jobseeker\widgets;

class MySettingsNav extends \yii\bootstrap4\Widget
{
    
    public function init(){}

    public function run() {
        
        
        return $this->render('my-settings-nav/view');
    }
}