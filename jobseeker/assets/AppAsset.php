<?php

namespace jobseeker\assets;

use yii\web\AssetBundle;
/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $css = [
        'css/_main.less',
    ];
    public $cssOptions = [
        'rel' => 'stylesheet/less',
        'type' => 'text/css',
    ];
    
    public $js = [
        'js/common.js',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END,
    ];
    
    public $depends = [
        'common\assets\CommonAsset',
    ];
}
