<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use common\models\SoftSkills;

class SoftSkillsController extends ActiveController
{
    public $modelClass = 'common\models\SoftSkills';  
    
    public function actionGetSkills() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $query = new \yii\db\Query;
        $query->select('title AS id, title, description')
            ->from('_soft_skills')
            ->where(['like', 'title',  (isset($_GET['title_prefix']) ? $_GET['title_prefix'] : '')]);
        $command = $query->createCommand();
        $data = $command->queryAll();
        
        $return_data['results'] = array_values($data);
        
        return $return_data;
    }
}


