<?php

namespace jobseeker\models;

use Yii;
use yii\base\Model;
use common\models\Jobseekers;
use common\models\JobseekerProfiles;

class EditProfileForm extends Model 
{
    public $profile_id;
    public $title;
    public $description;
    public $visibility; 
    public $employment_type;
    public $salary;
    public $salary_currency;
    public $soft_skills;
    public $specialization;
    public $categories;
        
    public function rules()
    {
        return [
            [['title', 'employment_type', 'specialization', 'categories', 'profile_id'], 'required'],
            ['profile_id', 'integer'],
            ['description', 'string', 'min' => 100, 'max' => 1000],
            ['employment_type', 'each', 'rule' => ['string']],
            ['salary', 'integer'],
            ['salary_currency', 'string'],
            ['soft_skills', 'each', 'rule' => ['string']],
            ['specialization', 'string'],
            ['categories', 'each', 'rule' => ['string']],
            
            [['salary_currency'], 'required', 'when' => function($model){
                return $model->salary != null;
            }, 'whenClient' => "function (attribute, value) {
                return $('#add-profile-form-salary').val() != null;
            }"]
        ];
    }
    
    public function editProfile() {
        if(!$this->validate()) {
            return false;
        }
        
        $jobseeker_profile = JobseekerProfiles::findOne(['jobseeker_id' => Jobseekers::find(['id' => Yii::$app->user->id])->one()->id, 'id' => $this->profile_id]);
        $jobseeker_profile->title = $this->title;
        $jobseeker_profile->description = $this->description;
        $jobseeker_profile->employment_type = implode(', ', $this->employment_type);
        $jobseeker_profile->salary = $this->salary;
        $jobseeker_profile->salary_currency = $this->salary_currency;
        $jobseeker_profile->soft_skills = (empty($this->soft_skills)) ? null : implode(', ', $this->soft_skills);
        $jobseeker_profile->specialization = $this->specialization;
        $jobseeker_profile->categories = implode(', ', $this->categories);
        
        return $jobseeker_profile->save();
        
    }
}