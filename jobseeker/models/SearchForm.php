<?php

namespace jobseeker\models;

use yii\base\model;

class SearchForm extends Model 
{
    public $keywords; 
    public $specialism; 
    public $country;
}

