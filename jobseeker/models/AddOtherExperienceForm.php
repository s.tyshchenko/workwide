<?php

namespace jobseeker\models;

use Yii;
use yii\base\Model;
use common\models\Jobseekers;
use common\models\JobseekerOtherExperience;

class AddOtherExperienceForm extends Model 
{
    public $title;
    public $description;
        
    public function __construct() {
        parent::__construct();
    }
    
    public function rules()
    {
        return [
            [['title', 'description'], 'required'],
            ['description', 'string']
        ];
    }
    
    public function addOtherExperience() {
        if (!$this->validate()) {
            return Yii::$app->response->redirect(Yii::$app->urlManager->createUrl('jobseeker/my/settings/profile'));
        }
        
        $jobseeker_other_experience = new JobseekerOtherExperience();
        $jobseeker_other_experience->title = $this->title;
        $jobseeker_other_experience->jobseeker_id = Jobseekers::find(['id' => Yii::$app->user->id])->one()->id;
        $jobseeker_other_experience->description = $this->description;
        
        $jobseeker_other_experience->save();
        
        return Yii::$app->response->redirect(Yii::$app->urlManager->createUrl('jobseeker/my/settings/profile'));
        
    }
}