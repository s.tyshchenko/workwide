<?php

namespace jobseeker\models;

use Yii;
use yii\base\Model;
use common\models\Users;
use borales\extensions\phoneInput\PhoneInputValidator;
use jobseeker\components\validators\PhoneVerificationCodeValidator;

class EditPhoneForm extends Model 
{
    public $phone; 
    public $phone_verification_code;
        
    public function rules()
    {
        return [
            ['phone', 'required'],
            ['phone', 'unique', 'targetClass' => '\common\models\Users', 'message' => Yii::t('jobseeker', 'This phone is already registered.')],
            ['phone', PhoneInputValidator::className(), 'message' => Yii::t('jobseeker', 'Invalid phone number')],
            
            ['phone_verification_code', 'required'],
            ['phone_verification_code', PhoneVerificationCodeValidator::className(), 'message' => Yii::t('jobseeker', 'Invalid code')],
        ];
    }
    
    
    public function editPhone() {
        if(!$this->validate()) {
            return false;
        }
        
        $user = Users::findOne(['id' => Yii::$app->user->id]);
        $user->phone = $this->phone;
                        
        return $user->save();
               
    }
}