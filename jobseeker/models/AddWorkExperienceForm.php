<?php

namespace jobseeker\models;

use Yii;
use yii\base\Model;
use common\models\Jobseekers;
use common\models\JobseekerWorkExperience;

use common\models\Countries;

class AddWorkExperienceForm extends Model 
{
    public $company_name;
    public $country;
    public $job_title;
    public $company_industry;
    public $work_start_date_month;
    public $work_start_date_year;
    public $work_end_date_month;
    public $work_end_date_year;
    public $description;

    public function rules()
    {
        return [
            [['company_name',  'job_title', 'company_industry', 'country', 'work_start_date_year', 'work_end_date_year', 'work_start_date_month', 'work_end_date_month'], 'required'],
            ['company_name', 'string'],
            ['company_industry', 'string'],
            ['job_title', 'string'],
            ['country', 'string'],
            ['description', 'string', 'length' => 1000],
            [['work_start_date_month', 'work_end_date_month'], 'integer', 'min' => 1, 'max' => 12],
            [['work_start_date_year', 'work_end_date_year'], 'integer', 'min' => 1900],
            ['work_end_date_year', 'compare', 'operator' => '>=', 'compareAttribute' => 'work_start_date_year']
        ];
    }
    
    public function addWorkExperience() {
        if (!$this->validate()) {
            return Yii::$app->response->redirect(Yii::$app->urlManager->createUrl('jobseeker/my/settings/profile'));
        }
        
        if(!Countries::findOne(['code' => $this->country]))
        {
            
            /*
             * Remove with non-Basic GeoDB Plan
             */
            sleep(2);
            
            $country_data = json_decode(file_get_contents("http://34.123.108.88/api/v1/geodb/country-details?countryId=" . $this->country))->result;
            
            $Country = new Countries();
            
            $Country->code = $this->country;
            $Country->title = $country_data->name;
            
            $Country->save();
        }
        
        $jobseeker_work_experience = new JobseekerWorkExperience();
        $jobseeker_work_experience->company_name         = $this->company_name;
        $jobseeker_work_experience->company_industry     = $this->company_industry;
        $jobseeker_work_experience->job_title            = $this->job_title;
        $jobseeker_work_experience->country              = $this->country;
        $jobseeker_work_experience->description          = $this->description;
        $jobseeker_work_experience->work_start_date      = date('Y-m-d', strtotime('01.'.$this->work_start_date_month.'.'.$this->work_start_date_year));
        $jobseeker_work_experience->work_end_date        = date('Y-m-d', strtotime('01.'.$this->work_end_date_month.'.'.$this->work_end_date_year));
        
        $jobseeker_work_experience->jobseeker_id = Jobseekers::find(['id' => Yii::$app->user->id])->one()->id;
        
        $jobseeker_work_experience->save();
        
        return Yii::$app->response->redirect(Yii::$app->urlManager->createUrl('jobseeker/my/settings/profile'));
        
    }
}