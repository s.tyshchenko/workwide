<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\Jobseekers;

class JobseekerPhotos extends ActiveRecord {

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['date_create', 'date_update'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['date_update'],
                ],
            ],
        ];
    }
    
    public static function tableName()
    {
        return 'jobseeker_photos';
    }
    
    public function getJobseeker()
    {
        return $this->hasOne(Jobseekers::className(), ['id' => 'jobseeker_id']);
    }
    
    
    public static function getPhotoPath($JobseekerId)
    {
        $photo = self::find()->where(['jobseeker_id' => $JobseekerId])->one();
        
        if($photo->exists())
        {            
            if(strpos($_SERVER['HTTP_ACCEPT'], 'image/webp') !== false) 
            {
                return ((isset($_SERVER['HTTPS'])) ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . '/' . $photo->path_webp;
            }
            else
            {
                return ((isset($_SERVER['HTTPS'])) ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . '/' . $photo->path;
            }
        }
        else
        {
            return null;
        }
    }
    
    
    
//    public function updateImages() 
//    {
// 
//        $tmp_dir = Yii::getAlias('@common') . '/web/images/tmp';
//
//        $images = scandir($tmp_dir);
//        
//        foreach($images as $image)
//        {
//            if($image != '..' && $image != '.')
//            {  
//                $img_title_arr = explode('.', $image);
//                array_pop($img_title_arr);
//                $image_title = implode('.', $img_title_arr);
//                
//                if(Images::find()->where(['title' => $image_title])->exists())
//                {
//                    Images::removeOldImage($image_title);
//                }
//                
//                $info = getimagesize($tmp_dir . '/' . $image);
//                if ($info['mime'] == 'image/jpeg') 
//                {       
//                    
//                    $img = imagecreatefromjpeg($tmp_dir . '/' . $image);
//                    imagepalettetotruecolor($img);
//                    imagealphablending($img, true);
//                    imagesavealpha($img, true);
//
//                    $jpeg_dir = Images::generateImageDir($image, 'jpeg');
//                    $webp_dir = Images::generateImageDir($image, 'webp');
//                    
//                    imagejpeg($img, $jpeg_dir, 1);
//                    $model = Images::find()->where(['title' => $image_title])->andWhere(['!=', 'extension', 'webp'])->one();
//                    
//                    if($model == null)
//                        $model = new Images();                        
//                    
//                    $model->title = $image_title;
//                    $model->path = substr($jpeg_dir, 7);
//                    $model->extension = 'jpeg';
//                    $model->save(false);
//                    
//                    imagewebp($img, $webp_dir, 1);
//                    $model2 = Images::find()->where(['title' => $image_title])->andWhere(['extension' => 'webp'])->one();
//                    
//                    if($model2 == null)
//                        $model2 = new Images();
//                    
//                    $model2->title = $image_title;
//                    $model2->path = substr($webp_dir, 7);
//                    $model2->extension = 'webp';
//                    $model2->save(false);
//
//                    imagedestroy($img);
//                }
//                elseif ($info['mime'] == 'image/png')
//                {                
//                    $img = imagecreatefrompng($tmp_dir . '/' . $image);
//                    imagepalettetotruecolor($img);
//                    imagealphablending($img, true);
//                    imagesavealpha($img, true);
//                    
//                    $png_dir = Images::generateImageDir($image, 'png');
//                    $webp_dir = Images::generateImageDir($image, 'webp');
//
//                    imagepng($img, $png_dir, 1);
//                    $model = Images::find()->where(['title' => $image_title])->andWhere(['!=', 'extension', 'webp'])->one();
//                    
//                    if($model == null)
//                        $model = new Images();
//                    
//
//                    $model->title = $image_title;
//                    $model->path = substr($png_dir, strlen(Yii::getAlias('@common'))+5);
//                    $model->extension = 'png';
//                    $model->save(false);
//                    
//                    imagewebp($img, $webp_dir, 1);
//                    $model2 = Images::find()->where(['title' => $image_title])->andWhere(['extension' => 'webp'])->one();
//                    
//                    if($model2 == null)
//                        $model2 = new Images();
//                    
//
//                    $model2->title = substr($image, 0, -4);
//                    $model2->path = substr($webp_dir, strlen(Yii::getAlias('@common'))+5);
//                    $model2->extension = 'webp';
//                    $model2->save(false);
//                    
//                    imagedestroy($img);
//                }
//                
//                unlink($tmp_dir . '/' . $image);
//            }
//        }
//    }
//    
//    function removeOldImage($image)
//    {
//        $images = Images::find(['title' => $image])->all();
//        foreach ($images as $image) 
//            unlink(Yii::getAlias('@common') . '/web/'. $image->path);
//    }
//    
//    function generateImageDir($image, $mime, $folder = '/web/images')
//    {
//        $folder = Yii::getAlias('@common') . '/web/images';
//        $name = md5($image) . '.' . $mime;
//        $folder_1 = substr(md5(microtime()), mt_rand(0, 30), 2);
//        $folder_2 = substr(md5(microtime()), mt_rand(0, 30), 2);
////        $path = $dir . '/' . $name;
//
//        if(!file_exists($folder . '/' . $folder_1))
//            mkdir($folder . '/' . $folder_1);
//        
//        if(!file_exists($folder . '/' . $folder_1 . '/' . $folder_2))
//            mkdir($folder . '/' . $folder_1 . '/' . $folder_2);
//        
//        fopen(($folder . '/' . $folder_1 . '/' . $folder_2 . '/' . $name), 'w+');
//
//        return $folder . '/' . $folder_1 . '/' . $folder_2 . '/' . $name;
//    }
}