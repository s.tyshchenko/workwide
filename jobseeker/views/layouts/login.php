<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use jobseeker\assets\AppAsset;
use common\widgets\WLanguages;
use common\widgets\Favicon;
use common\widgets\Alert;
use jobseeker\widgets\Header;
use jobseeker\widgets\HeaderNavbarMenu;

AppAsset::register($this);

$this->registerJsFile(
    '@web/js/less.js',
    [
        'data-env' => 'development',
        'position' => \yii\web\View::POS_HEAD,
        'depends' => 'yii\bootstrap4\BootstrapPluginAsset'
    ]
);

\yii\web\View::registerJsFile(
    '@web/js/placeholder-animation.js',
    [
        'position' => \yii\web\View::POS_END,
        'depends' => 'yii\bootstrap4\BootstrapPluginAsset'
    ]
);


\yii\web\View::registerJs(
    "animated_placeholder({
	el: document.getElementsByClassName('placeholder-animated')[0],
	sentences: ['example@email.com', '+12015550123'],
        options: {
            letterDelay: 50,
            loop: true,
            startOnFocus: false
        }
    })",
    \yii\web\View::POS_LOAD
);

\yii\web\View::registerJs(
    "animated_placeholder({
	el: document.getElementsByClassName('placeholder-animated')[1],
	sentences: ['*****************', '************'],
        options: {
            letterDelay: 50,
            loop: true,
            startOnFocus: false
        }
    })",
    \yii\web\View::POS_LOAD
);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <?= Favicon::widget() ?>
    
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    
</head>
<body id="<?= 'p-' . Yii::$app->controller->id . '-' . Yii::$app->controller->action->id ?>">
<?php $this->beginBody() ?>

    <?= HeaderNavbarMenu::widget() ?>
    <header id="header">
        <?= Header::widget(['action' => Yii::$app->controller->action->id]) ?>
    </header>

    
<main id="main" class="wrap">     
    <?= Alert::widget() ?>
    <?= $content ?>
</main>

<footer class="footer">
    <div class="container">

    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
