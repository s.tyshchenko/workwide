<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class LanguageProficiency extends ActiveRecord {

    public static function tableName()
    {
        return '_language_proficiency';
    }
    
    public function fields()
    {
        return ['name', 'title'];
    }
    
    public function combineLanguageProficiency() {
        $return_array = [];
        foreach (self::find()->orderBy(['index_number' => SORT_ASC])->all() as $LanguageProficiency)
        {
            $return_array[$LanguageProficiency->name] = Yii::t('jobseeker', $LanguageProficiency->title); 
        }
        
        return $return_array;
    }
}