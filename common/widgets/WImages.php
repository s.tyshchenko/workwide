<?php

namespace common\widgets;

use Yii;
use common\models\Images;

class WImages extends \yii\bootstrap4\Widget
{
    public $title;


    public function run()
    {
        
        if($path = Images::getImagePath($this->title))
        {
            return $path;
        }
        else
        {
            
            if(glob(Yii::getAlias('@common') . '/web/images/tmp/' . $this->title . '.*'))
            {
                Images::updateImages();
                return Images::getImagePath($this->title);
            }
            else 
            {
                return null;
            }
        }
    }
    
}