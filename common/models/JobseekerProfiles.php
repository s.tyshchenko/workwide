<?php

namespace common\models;

use Yii;
use common\models\Jobseekers;
use common\models\ProfileProfessionalSkills;
use common\models\SpecializationSkills;
use yii\db\ActiveRecord;

class JobseekerProfiles extends ActiveRecord
{
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['date_create', 'date_update'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['date_update'],
                ],
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jobseeker_profiles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['jobseeker_id', 'required']
        ];
    }
    
    public function getJobseeker()
    {
        return $this->hasOne(Jobseekers::className(), ['id' => 'jobseeker_id']);
    }
    
    public function getProfileProfessionalSkills()
    {
        return $this->hasMany(ProfileProfessionalSkills::className(), ['profile_id' => 'id']);
    }
    
    public function getProfileSpecializationSkills()
    {
//        $query = SpecializationSkills::find();
//        $query->link = ['specialization_id' => 'specialization'];
//        $query->orWhere('specialization_id IS NULL');
//        $query->multiple = true;
//        return $query;
////        
        return $this->hasMany(SpecializationSkills::className(), ['specialization_id' => 'specialization'])->union(SpecializationSkills::find()->where(['specialization_id' => '0']));
    }
}