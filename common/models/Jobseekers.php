<?php

namespace common\models;

use Yii;
use common\models\Users;
use yii\db\ActiveRecord;

class Jobseekers extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jobseekers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['user_id', 'unique'],
        ];
    }
    
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'id']);
    }
    
    public function getPhoto()
    {
        return $this->hasOne(JobseekerPhotos::className(), ['jobseeker_id' => 'id']);
    }
    
    public function getEducation()
    {
        return $this->hasMany(JobseekerEducation::className(), ['jobseeker_id' => 'id']);
    }

    public function getLanguages()
    {
        return $this->hasMany(JobseekerLanguages::className(), ['jobseeker_id' => 'id']);
    }
    
    public function findJobseekerByUserId($user_id)
    {
        $jobseeker = self::findOne(['user_id' => $user_id]);
        
        //Remove with non-Basic GeoDB Plan
//        sleep(1);
        
//        $location_data = json_decode(file_get_contents("http://34.123.108.88/api/v1/geodb/city-details?cityId=" . $jobseeker->city))->result;
        
        $jobseeker->birth_date = date('d.m.Y', strtotime($jobseeker->birth_date));
        $jobseeker->country = 'Ukraine';
        $jobseeker->city = 'Dnipro';
        $jobseeker->timezone = str_replace('__', ' / ', $jobseeker->timezone);
        
        return $jobseeker;
    }
}